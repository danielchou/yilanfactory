﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMWebApp.Common
{
    public class EZShip
    {

        public string orderStatusDisplay(string order_status)
        {
            string oss = "";
            switch (order_status)
            {
                case "S01": oss = "訂單新增成功"; break;
                case "E00": oss = "參數傳遞內容有誤或欄位短缺"; break;
                case "E01": oss = "<su_id> 帳號不存在"; break;
                case "E02": oss = "<su_id> 帳號無建立取貨付款權限 或 無網站串接權限 或 無ezShip宅配權限"; break;
                case "E03": oss = "<su_id> 帳號無可用之 輕鬆袋 或 迷你袋"; break;
                case "E04": oss = "<st_code> 取件門市有誤"; break;
                case "E05": oss = "<order_amount> 金額有誤"; break;
                case "E06": oss = "<rv_email> 格式有誤"; break;
                case "E07": oss = "<rv_mobile> 格式有誤"; break;
                case "E08": oss = "<order_status> 內容有誤 或 為空值"; break;
                case "E09": oss = "<order_type> 內容有誤 或 為空值"; break;
                case "E10": oss = "<rv_name> 內容有誤 或 為空值"; break;
                case "E11": oss = "<rv_addr> 內容有誤 或 為空值"; break;
                case "E98": oss = " 系統發生錯誤無法載入"; break;
                case "E99": oss = " 系統錯誤"; break;
            }
            return oss;
        }
    }


}