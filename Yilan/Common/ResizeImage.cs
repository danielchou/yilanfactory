﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Drawing.Imaging;
using NLog;


namespace My.Common
{

    

    public class ImageProcess
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="size"></param>
        /// <param name="preserveAspectRatio"></param>
        /// <returns></returns>
        public static Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }

            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        /// <summary>
        /// 產生對應小圖檔，位於尺寸決定得路徑下。
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        /// <param name="image"></param>
        /// <param name="size"></param>
        public static void SaveResizeImage(string path, string filename, Size size)
        {
            string OriginalPath = Path.Combine(path, filename); //只以高作決定
            string ResizedPath = path + @"\" + size.Height; //只以高作決定
            string NewPath = Path.Combine(ResizedPath, filename);

            try
            {
                Image original = Image.FromFile(OriginalPath);
                Image resized = ImageProcess.ResizeImage(original, size);

                if (!Directory.Exists(ResizedPath))
                {
                    Directory.CreateDirectory(ResizedPath);
                }
                resized.Save(NewPath, ImageFormat.Jpeg);
                original.Dispose();
                resized.Dispose();
            }
            catch (Exception ex) {
                logger.Debug(ex.ToString());
            }            
        }
    }
}
