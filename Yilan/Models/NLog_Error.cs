//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMWebApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class NLog_Error
    {
        public int LogId { get; set; }
        public int ThreadId { get; set; }
        public string MachineName { get; set; }
        public string LogName { get; set; }
        public string LogLevel { get; set; }
        public string LogMessage { get; set; }
        public string CallSite { get; set; }
        public string Exception { get; set; }
        public string Stacktrace { get; set; }
        public System.DateTime CreateDateTime { get; set; }
    }
}
