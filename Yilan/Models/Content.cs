//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CMWebApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Content
    {
        public int SN { get; set; }
        public Nullable<int> ParentSN { get; set; }
        public string Guid { get; set; }
        public string RouteName { get; set; }
        public string BigTitle { get; set; }
        public string SubTitle { get; set; }
        public string Head { get; set; }
        public string Body { get; set; }
        public string Note { get; set; }
        public string Dscr { get; set; }
        public string Creator { get; set; }
        public Nullable<System.DateTime> StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public string OnTheTop { get; set; }
        public string IsEnable { get; set; }
        public string IsRemove { get; set; }
        public Nullable<System.DateTime> InsertDate { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public Nullable<int> SortNo { get; set; }
    }
}
