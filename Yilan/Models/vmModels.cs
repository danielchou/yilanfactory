﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMWebApp.Models;

namespace CMWebApp.Models
{
    public class vmImage
    {
        public string Guid { get; set; }
        public string Title { get; set; }
        public string Dscr { get; set; }
        public string FileNameNew { get; set; }
    }
    public class vmItems : Item
    {
        public vmImage mainImage { get; set; }
        public IEnumerable<AttachFile> imgs { get; set; }
        public vmStore1 store { get; set; }

        public string storeCodeName { get; set; }
        public string storeName { get; set; }
        public string storeColor { get; set; }
        public int ParentSN { get; set; }

        public string compoWording { get; set; }
    }

    public class vmStore1 {
        public string name { get; set; }
        public string color { get; set; }
        public string codeName { get; set; }
    }
    
    public class vmMarker
    {
        public int sn { get; set; }
        public int storeID { get; set; }
        public string Name { get; set; }
        public string Typ { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string isVisible { get; set; }
        public int? sortNo { get; set; }
    }
}