﻿--查詢 欄位的Collation Name(核對、歸類、對照) =============================================
select object_name(object_id) as tablename, name as columnname,collation_name
from sys.columns
where collation_name is not null and object_name(object_id) in ('orderM', 'cart')
order by object_name(object_id),column_id

select name,collation_name from sys.databases where name='DB_9AB840_Shizi';

ALTER DATABASE [DB_9AB840_Shizi] SET SINGLE_USER WITH ROLLBACK IMMEDIATE 
ALTER DATABASE [DB_9AB840_Shizi] COLLATE Chinese_Taiwan_Stroke_CI_AS  
--SQL_Latin1_General_CP1_CI_AS  Chinese_Taiwan_Stroke_CI_AS
ALTER DATABASE [DB_9AB840_Shizi] SET MULTI_USER WITH ROLLBACK IMMEDIATE

		-----可以針對Table內的某個欄位Collation
		ALTER TABLE dbo.Cart ALTER COLUMN orderID  
			  varchar(50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL;  
		GO  

-- end Collation =======


--==帳戶管理====================
select * from [dbo].[AspNetUsers]
delete from [AspNetUsers] where email='daniel0422@gmail.com'

--DB_9AB840_Shizi_admin <==
--下載影音檔設定=========================================================================
delete from downloaditem
select * from [dbo].[Sheet1$]
drop table sheet1$
select * from downloaditem
update downloaditem set imgurl='' where imgurl='???101???102????'

-- 下載影音結束 =======
select Sn,Guid,HookID,Typ,sort,filename,filenameNew 
	from [dbo].[AttachFile];

delete from [AttachFile]

select * from Content
select * from Content where guid='d7f87e10-fc2e-4f3b-a730-ad3f81fc7605'
select * from content where routename='weeklyMain'
select * from item
select * from [AttachFile]
	

update [AttachFile] set isRemove=null;
delete from AttachFile where hookid=2

select newid()
insert into Content ([guid],parentGuid,title,subtitle,insertdate)
			values ( newid(),1, '華視媒體營影國','CCCamp',getdate());

--==網站內容的樹狀結構====================================================================
select * from vContentTreeNode where level =1 and sn not in (6,7) order by sort
select * from vContentTreeNode order by sort
select * from vContentTreeNode where parentSN =10 order by sort
select * from item where ContentSN in (select sn from vContentTreeNode where parentSN =10);
select * from Content where sn in (
	select sn from vContentTreeNode where level =1 and sn not in (6,7)
)

/* 修正是否有重複的RouteName? ========================================  */
select RouteName,count(*) from content group by RouteName order by 2 desc;
select * from content where sn=18

--end ====


select * from content where guid='18d1ac01-32bc-449d-ad43-2d0a44fc522c'
select * from content where RouteName='campresult'
select * from item order by insertdate desc
select 
(select bigTitle from content where guid=hookid) as contentBigTitle,*
 from AttachFile where typ='Content'

select substring(replace(newid(),'-',''),1,10)
/*====================================================================================*/


select id,email,emailconfirmed,phonenumber,username,AccessFailedCount 
	select * from [dbo].[AspNetUsers]
select * from AspNetUserLogins  --找出provider是誰?
select * from AspNetUserRoles  --腳色是誰? 目前沒設定
 

select rtrim('asdfasdf    ')
select typ,typ2,count(*) from [dbo].[downloaditem] group by typ,typ2 order by 1,2,3
select * from [dbo].[downloaditem] where typ2 like '套書 10萬個為什麼%'

select * from downloaditem where note='v'
update downloaditem set note ='v' where substring(dwurl,len(dwurl)-2,3)='jpg'
update downloaditem set dwurl=audiourl where note='v';

select * from Product
update content set parentSN=1,SortNo=1 where sn=95
select * from content where sn=73


--==產品管理 
select * from item where ContentSN=73 and isRemove is null
select BigTitle from Content where ParentSN=16 order by sortNo
select * from AspNetUsers
select id,username,email,EmailConfirmed from AspNetUsers

select * from item order by InsertDate desc
update item set contentSN=91 where sn in (219,218,217)


select * from AspNetUsers order by email --chalingwu123@gmail.com

select * from attachFile
select * from item where ContentSN=73
--點讀筆===========================================
select * from downloaditem where dwurl is not null and imgurl is not null
select * into downloaditem_old from downloaditem
select * from downloaditem_old
delete from downloaditem
DBCC CHECKIDENT ("downloaditem", RESEED, 1);

--項目=============================================================
select * from item where contentSN=73 and IsRemove='Y'
delete from item where contentSN=73 and IsRemove='Y'
insert into item (guid,ContentSN,sn,title,subtitle,sort,insertdate,cost,typ)
	select convert(char(20),replace(convert(varchar(50),newid()),'-',''))
		,92,sn+10000,title,subtitle,sort,insertdate,cost,typ from Item where contentSN=73

	select convert(char(20),replace(convert(varchar(50),newid()),'-',''))

select * from OrderM
select * from item where ContentSN=73  and sn in (162,163)
select * from cart where custID='daniel0422@gmail.com' and typ='cart' and isRemove=0
select * from vCart where custID='daniel0422@gmail.com' and typ='cart' and isRemove=0
update cart set isDiscount=0 where sn=2

select * from downloaditem where audioUrl is not null
update downloaditem set isRemove=0

select * from AspNetUsers where email='daniel0422@gmail.com'
delete from AspNetUsers where email='daniel0422@gmail.com'

--==訂單明細操作=============================================
	 --訂單主檔 
select * from orderM
select * from Cart where custid='daniel0422@gmail.com' and typ='cart'
select * from Cart where custid='daniel0422@gmail.com' and typ='order'
select * from Cart --購物車C & 我的最愛F
select * from item where cost is not null
select * from vCart --typ: favor|cart|order
select * from OrderM 
--===各店家=================================================
select * from content order by sn desc
select * from content where RouteName = 'home'
select sn,ParentSN,BigTitle,SortNo from content order by sn desc;
select * from store;
select * from item order by sn desc;

update content set ParentSN=1 where sn =110

select * from content order by InsertDate desc


delete from store where id in (1,2,3)
update store set codeName ='rabbit1' where id=5
update store set codeName ='lucky-art' where id=6
update store set codeName ='rden' where id=7
update store set codeName ='agrioz' where id=8
update store set codeName ='siho' where id=9
update store set codeName ='dr-duck' where id=10
update store set codeName ='junbaby' where id=11
update store set codeName ='kavalanwhisky' where id=12
update store set codeName ='cjwine' where id=13
update store set codeName ='dr-foot' where id=14
update store set codeName ='icake' where id=15
update store set codeName ='yide-cake' where id=16
update store set codeName ='how-mama' where id=17
update store set codeName ='sabelina' where id=18
update store set codeName ='kilibay' where id=19
update store set codeName ='wjfarm' where id=20
update store set codeName ='lotungfa' where id=21
update store set codeName ='geginseng' where id=22
update store set codeName ='onemit' where id=23
update store set codeName ='anyomuseum' where id=24

--==每家商店在地圖上的位置?======================================================
select * from store
--這是起點
update store set X=585,Y=61.5 where id=4; --虎牌米粉產業文化館
update store set X=468,Y=54 where id=5; --玉兔鉛筆學校
update store set X=581,Y=82 where id=6; --蜡藝蠟筆城堡
update store set X=433,Y=40 where id=7;  --亞典蛋糕
update store set X=476,Y=37.5 where id=8; --橘之鄉蜜餞觀光工廠
update store set X=592,Y=67 where id=9; --溪和三代目魚寮文化館
update store set X=550,Y=60 where id=10; --博士鴨觀光工廠
update store set X=438,Y=47 where id=11; --菌寶貝博物館
update store set X=278,Y=49 where id=12; --金車噶瑪蘭威士忌酒廠
update store set X=516,Y=13 where id=13; --藏酒酒莊
update store set X=512,Y=52.5 where id=14; --台灣足鞋健康知識館
update store set X=610,Y=89 where id=15; --宜蘭餅發明館 
update store set X=362,Y=61 where id=16; --益得畑の菓
update store set X=512,Y=59.5 where id=17; --東和食品工業股份有限公司
update store set X=474,Y=43.5 where id=18; --莎貝莉娜精靈印畫學院
update store set X=600,Y=73 where id=19; --奇麗灣珍奶文化館
update store set X=570,Y=52 where id=20; --夢田觀光米廠
update store set X=475,Y=60 where id=21; --羅東鎮農會養生休閒驛站
update store set X=453,Y=33.5 where id=22; --花旗蔘館
update store set X=605,Y=79 where id=23; --一米特創藝美食館
update store set X=544,Y=85 where id=24; --安永心食館
update store set X=429,Y=26 where id=25;--潭酵天地觀光工廠

--google 管理者============================================================
select id,name,GoogleAccount from store

update store set googleaccount ='tiger.lize9@gmail.com' where name=N'虎牌米粉產業文化館';
update store set googleaccount ='pencilschool@gmail.com' where name=N'玉兔鉛筆學校';
update store set googleaccount =' mygod3011@gmail.com' where name=N'蜡藝蠟筆城堡';
update store set googleaccount ='fhshterry@gmail.com' where name=N'亞典蛋糕密碼館';
update store set googleaccount ='agrioz888@gmail.com' where name=N'橘之鄉蜜餞觀光工廠';
update store set googleaccount ='service@siho.com.tw' where name=N'溪和三代目魚寮文化館';
update store set googleaccount ='drduck.office@gmail.com' where name=N'博士鴨觀光工廠';
update store set googleaccount ='bio.nin2016@gmail.com' where name=N'菌寶貝博物館';
update store set googleaccount ='lynn0707@gmail.com' where name=N'金車噶瑪蘭威士忌酒廠';
update store set googleaccount ='service@cjwine.com' where name=N'藏酒酒莊';
update store set googleaccount =' drfoot.cp@gmail.com' where name=N'台灣足鞋健康知識館';
update store set googleaccount ='icake549881@gmail.com' where name=N'宜蘭餅發明館 ';
update store set googleaccount ='h9898467@gmail.com' where name=N'益得畑の菓';
update store set googleaccount ='' where name=N'東和食品工業股份有限公司';
update store set googleaccount ='sabelina123@gmail.com' where name=N'莎貝莉娜精靈印畫學院';
update store set googleaccount ='' where name=N'奇麗灣珍奶文化館';
update store set googleaccount ='' where name=N'夢田觀光米廠';
update store set googleaccount ='linjerod@gmail.com' where name=N'羅東鎮農會養生休閒驛站';
update store set googleaccount ='geginseng@gmail.com' where name=N'花旗參館';
update store set googleaccount ='onemit2015@gmail.com' where name=N'一米特創藝美食館';
update store set googleaccount ='anyomuseum@gmail.com' where name=N'安永心食館';
update store set googleaccount ='hsuslegend@gmail.com' where name=N'潭酵天地觀光工廠';

select * from marker
Select * from TripName order by createDate DESC
Select * from TripRouting order by createDate DESC
select * from TripRouting where userId = 'weiwei.chou99@gmail.com' and tripid =1  and storeID =0

update TripRouting set sortNo=3 where sn=277

select * from TripRouting as a 
	join vmarker as b on a.storeID = b.id
	where a.userId='weiwei.chou99@gmail.com' and a.tripID = 1

delete from TripRouting where sn in (280,281)

sp_helptext vmarker

alter view vMarker as  
--select 0 as id,'startLocation' as name, '' as typ union
select id, name,'favor' as typ from store   
union   
select id, name,typ from marker  
--end



delete from TripName where id = 1 and userid='weiwei.chou99@gmail.com'
delete from TripRouting where userid ='weiwei.chou99@gmail.com'

select * from content order by insertDate desc 
update content set isRemove ='Y' where sn between 3 and 93;

update store set typ='B' where id=4
select * from storeDetail
--這邊手動加入
insert into StoreDetail (storeid,typ,dscr,createDate) 
	values (4,'service','aaaaaaaaaaaaaaasdf asdf asdfasdfasdasdfasdfasdfaswwwwwwwwwwwww',getdate());

select * from Item order by InsertDate desc
select * from AttachFile where HookID ='079a0a61153b45ac9109'
select * from vContentTreeNode 

Select * from store 
select * from FavorStore
alter table favorstore add updateTime smalldatetime;
update FavorStore set sortNo =2 where sn =2

delete from FavorStore
--alter table favorStore drop column isVisible;
alter table store add PlaceID varchar(200) null;

Select * from store where placeid is null;
update store set placeid = 'ChIJxwxiEU77ZzQRZ3a_KNM6LXg' where id=7
update store set placeid = 'ChIJk5XFe9H3ZzQRIfI-A3wker8' where id =13;
update store set placeid = 'ChIJZ89b0YjoZzQRbzXlPTCEoSU' where id=6;
update store set placeid = 'ChIJn4YOfd7lZzQRaj_IrGY12Ks' where id =10
select * from vContentTreeNode order by sort;
--工廠資料維護
select * from vContentTreeNode where sort like '1-2%' order by sort 
--行銷優惠
select * from vContentTreeNode where sort like '1-3%' order by sort 
Select * from item where ContentSN =100


select * from AspNetUsers

select * from store where codeName = 'tigerfood' --tiger.lize9@gmail.com
update store set GoogleAccount='tiger.lize9@gmail.com' where codename ='tigerfood'
update store set GoogleAccount='weiwei.chou99@gmail.com' where codename ='tigerfood'
--最大的管理者
select * from vContentTreeNode order by sort
select * from store where GoogleAccount='weiwei.chou99@gmail.com'

select * from vContentTreeNode where parentSN in (1,95) and sn =103 order by sort

declare @u as varchar(200); set @u='daniel0422@gmail.com';
select * from TripRouting where userid=@u and tripid=0

delete from TripRouting where sn =283

select * from TripName where userid =@u;
	delete from TripName where userid=@u;



delete from TripRouting where userid=@u and tripid <>0