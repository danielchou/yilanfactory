﻿Vue.component("member-form", {
    template: "#member-form-template",
    //replace: true, // cache view...
    props: {
        memberId: { type: String },
        isDetail: { type: Boolean, default: false },
        editMode: { type: Boolean, default: false },
        test: { type: String, default: "12344555MyTest!" }
    },
    data: function () {
        return {
            Member: null,
        }
    },
    watch: {
        "memberId": {
            handler: function (val, oldVal) {
                //console.log('new: %s, old: %s', val, oldVal);
                this.fetchData();
            },
            //deep:true
        }
    },
    created: function () {
        //console.log("created member-form", this.memberId, this.isDetail);
        this.fetchData();
    },
    methods: {
        fetchData: function () {
            var self = this;
            $.post(host("Member/GetOneMember"), { gid: this.memberId }, function (rs) {
                self.Member = rs;

                var f = self.Member;
                f.WeddingDate = ToJavaScriptDate(rs.WeddingDate);
                f.EngageDate = ToJavaScriptDate(rs.EngageDate);
                f.Birthday = ToJavaScriptDate(rs.Birthday);
                f.Birthday2 = ToJavaScriptDate(rs.Birthday2);
                f.InsertDate = rs.InsertDate;
                f.UpdateTime = rs.UpdateTime;
                self.$emit('form-loaded'); //確保資料進來後才會display view.
            });
        },
        hideDetail: function () {
            main.isDetail = false;
        },
        AddNewMember: function () {
            //console.log('addnew');
            this.editMode = true;
            this.Member = NewMember;
        },
        clickEdit: function () {
            this.editMode = true;
        },
        clickSave: function (e) {
            e.preventDefault();
            var self = this;
            $.post(host("Member/SaveMember"), this.Member, function (rs) {
                self.fetchData();
                self.editMode = false;
                self.$parent.$.refMemberlist.fetchData();
            });
        },
        clickCancel: function () {
            this.editMode = false;
        }
    }

});