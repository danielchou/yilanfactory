﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "sortable": ["Sortable/Sortable.min"]
    },
    shim: {
        "datepicker": ["jquery", "core"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "jquery","sortable", "datepicker"], function (Vue,$, Sortable) {
    $(document).bind("ajaxSend", function () {
        $("#loading").show();
    }).bind("ajaxComplete", function () {
        $("#loading").hide();
    });

    Vue.debug=true;
    //Vue.use(validator);
    Vue.component("codetable-list", {
        template: "#CodeTable-List-Template",
        paramAttributes: ["tb", "typ"],
        data: function () {
            return {
                TBs: null,
                Typs: null,
                CodeTables: null,
                newCodeTable: { Name:"", Dscr:"", Typ:"", TB:""},
                currTB: "Dress",
                currTyp: "Color",
                currGID: "",
                currEditGid:"" //編輯用的GID
            }
        },
        compiled: function () {
            this.GetTBList();
            //console.log("from outsite paras:", this.tb, this.typ);
            this.currTB = this.tb;
            this.currTyp = this.typ;
            this.$watch("currTB + currTyp", function () {
                this.GetCodeTableList();
            });
            

        },
        methods:{
            GetTBList: function () {
                var self = this;
                $.post("/CodeTable/GetTBList", {}, function (rs) {
                    self.TBs = rs;
                    $.post("/CodeTable/GetTyp", { TB: self.currTB }, function (rs) {
                        self.Typs = rs;
                        //self.currTyp = self.Typs[0].Typ;
                        self.GetCodeTableList();
                    });
                });
            },
            GetCodeTableList: function () {
                var self = this;
                $.post("/CodeTable/GetCodeTableList", { TB: self.currTB, Typ: self.currTyp }, function (rs) {
                    self.CodeTables = rs;
                });
            },

            GetTB: function (e) {
                this.currTB = e.targetVM._raw.Name;
                var self = this;
                $.post("/CodeTable/GetTyp", { TB: this.currTB }, function (rs) {
                    self.Typs = rs;
                    self.currTyp=self.Typs[0].Typ;
                });
            },
            GetTyp: function (e) {
                this.currTyp = e.targetVM._raw.Typ;
                this.currEditGid = "";
            },
            GetDetails: function (e) {
                this.currGID = e.targetVM._raw.GID;
                $.post("/CodeTable/GetDetails", { Guid: this.currGID }, function (rs) {
                    self.CodeTables = rs;
                });
            },
            onEdit: function (e) {
                this.currEditGid = e.targetVM._raw.GID;
                //console.log(this.currEditGid);
            },
            onCancel: function () {
                this.currEditGid = "";
            },
            onSave: function (e) {
                var CodeTable = e.$data,
                    self = this;
                $.post("/CodeTable/SaveDetails", { CodeTable: CodeTable }, function (rs) {
                    //console.log(rs);
                    console.log(self.newCodeTable);
                    self.GetCodeTableList();
                    self.currEditGid = "";
                });
            },
            onAddNew: function (e) {
                var CodeTable = e.$data, self = this;

                self.newCodeTable.TB = this.currTB;
                self.newCodeTable.Typ = this.currTyp;
                console.log(self.newCodeTable, CodeTable);
                $.post("/CodeTable/SaveDetails", { CodeTable: self.newCodeTable }, function (rs) {
                    self.GetCodeTableList();
                    self.currEditGid = "";
                    self.newCodeTable.Name = "";
                    self.newCodeTable.Dscr = "";
                });
            },
            onDelete: function (e) {
                var self = this;
                //Note:最好不要刪除，怕有問題，還要多檢查很麻煩!
                $.post("/CodeTable/DeleteCodeTable", { Guid: this.currGID }, function (rs) {
                    if (rs.code == "CodeTable Used") {
                        alert("該項目已有"+rs.cc+"個項目在使用，不能夠刪除。");
                    } else {
                        self.GetCodeTableList();
                        self.currEditGid = "";
                    }
                });
            }
        }
    });

    var main = new Vue({
        el: "#main",
        data: {},
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); }
        }
    });

    var el = document.getElementById("sortable");
    var sortable = new Sortable(el, {
        animation: 150,
        handle: ".my-handle",
        // dragging ended
        onEnd: function (/**Event*/e) {
            e.oldIndex;  // element's old index within parent
            e.newIndex;  // element's new index within parent
            var ct = main.$.refCodeTable;
            //console.log(e.oldIndex, e.newIndex, ct.currTB, ct.currTyp);
            $.post("/CodeTable/SaveSort", { oldIndex: e.oldIndex, newIndex: e.newIndex, TB: ct.currTB, Typ: ct.currTyp }, function () {
                ct.GetCodeTableList();
            });
        },
        // Called by any change to the list (add / update / remove)
        onSort: function (/**Event*/evt) {
            // same properties as onUpdate
            var itemEl = evt.item;
            console.log(itemEl);
        },
    });

    
});

