﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug = true;
    var main = new Vue({
        el: "#main",
        data: {
            dresses: {}
        },
        created: function () {
            $(".ui.blue.adminMenu,#headerBar").hide();
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post("/showroom/GetDressForExcelCopyList", {store: $(".table").attr("data-store") }, function (rs) {
                    self.dresses = rs;
                    console.log(rs);
                });
            }
        }
      
    });

});
