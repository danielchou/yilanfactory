﻿requirejs.config({
    baseUrl: "/Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.js/accounting.min"],
        "fullcalendar": ["fullcalendar/dist/fullcalendar.min"],
        "moment": ["moment/min/moment.min"]
    },
    shim: {
        "semantic": { deps: ["jquery"], exports: "semantic" },
        "datepicker": ["jquery"]
    }
});

define(["vue", "jquery", "accounting", "moment", "semantic"], function (Vue, $, accounting) {

    $(document).bind("ajaxSend", function () {
        $("#loading").show();
    }).bind("ajaxComplete", function () {
        $("#loading").hide();
    });



    var main = new Vue({
        el: "body",
        data: {
            tbTitle: ["#", "ISSTAR", "ORANAME", "LAST_ANALYZED", "OLDCC","NEWCC", "TBNAME", "TBCNAME","LASTEDITTIME", "NUM_ROWS"],
            sortColName: "NUM_ROWS",
            tableLists: {},
            tbtyps: [],
            isDesc: -1,
            currTBSN:0,
            currTBName: "",      //原本Oracle資料表
            currNewTBName: "",   //新MSSQL資料表名稱 
            currNewCTBName: "",   //資料表中文名稱
            TableTyps: ['未分類', '基礎設置', '客戶', '藥品(含管制)', '報價', '採購', '收貨', '入庫', '庫存', '訂單', '庫取', '出庫', '會計'], //請依順序排列
            currTableTyp: "",
            currTBColumns: {},
            currFilterTableTyp: "a0t",

            TBColTitles: ["#", "COLUMN_NAME", "原本說明","預設值", "IsNull","不同筆數", "MSSQL新欄位設定"],
            currOldColName: "",      //舊的欄位名(KEY)
            newTBColumns: {},        //準備POST出去的欄位定義資料
            newTBClm: {
                OWNER: "", TBNAME: "", COLUMN_ID: "", DATA_TYPE: "", DATA_LENGTH: "", COMMENTS: "", SN: 0, TBSN: 0, ORANAME: "", ENAME: "", NEWDATATYPE: "", CREATEDT: "", WHERESTR: "", ANDORSTR: "", ISLASTUPDATETIME: "", ISSTAR: "", SQLCOMMENTS: ""
                
            },
            isShowEnabled: false,
            NumDistinctDetails:{},
            TBNames: [],
            EnabledCC: 0,
            TotalCC: 0,
            IndexColumnCC: 0,
            IndexColsTitle: ["INDEX_NAME", "INDEX_TYPE", "UNIQUENESS", "COLUMN_POSITION", "COLUMN_NAME", "DESCEND"],
            IndexCols: {}
        },
        created: function () {
            var tb = $("#table-list").attr("data-currTableName");  //console.log(tb);
            if (tb !== "") {
                $('.ui.sidebar').sidebar('pull page');
                this.GetOraSingleTable(tb);
                this.fetchData();
            } else {
                this.fetchData();
                $('.ui.sidebar').sidebar('toggle');
            }
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            accounting: function (v) { return accounting.formatMoney(v); },
            accountingGer: function (v) { return accounting.formatMoney(v, "", 0); },
            toBoolean: function (v) { return (v === "Y"); },
            toTableTypName: function (v) { return this.TableTyps[v]; }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("/DBTable/GetOracleAllTables"), { sort: "desc", col: 2 }, function (rs) {
                    self.tableLists = rs.tbs;
                    //self.tbtyps =JSON.parse(rs.tbtyps);
                });
            },
            getSortCol: function (t) {
                var self = this;
                self.isDesc = self.isDesc * -1;
                self.sortColName = t;
                console.log(this.isDesc, this.sortColName);
            },
            GetOraSingleTable: function (tbName) {
                var self = this;
                self.currTBName = tbName;
                $.post(host("/DBTable/GetOraSingleTable/"), { tb: tbName }, function (rs) {
                    self.currTBColumns = rs.colRows;
                    var tb = rs.tb;
                    self.currTBSN = tb.SN;
                    self.currNewTBName = tb.TBNAME;
                    self.currNewCTBName = tb.TBCNAME;
                    $('.ui.sidebar').sidebar('hide')

                    self.EnabledCC = self.currTBColumns.filter(function (n) { return (n.ISENABLE == "Y"); }).length
                    self.TotalCC = self.currTBColumns.length
                    self.IndexColumnCC = rs.indexCols.length
                    self.IndexCols = rs.indexCols;
                });
            },
            toggleSideBar: function () {
                //$('.ui.sidebar').sidebar('toggle');
                location.href = "/DBtable";
            },
            setIsStar: function (tb) {
                var self = this;
                $.post(host("/DBTable/setIsStar/"), { tb: tb }, function (rs) {
                    self.fetchData();
                });
            },
            openModalTBName: function (e) {
                var self = this;
                self.currTBName = e.ORANAME;
                self.currNewTBName = e.TBNAME;
                self.currNewCTBName = e.TBCNAME;
                self.currTableTyp = e.TYP;
                $(".ui.modal.tbname").modal("show");
            },
            showTBRows: function (tb) {
                var self = this;
                self.currTBName = tb;
                $.post(host("/DBTable/ShowTBTop100Rows/"), {tb: tb}, function (rs) {
                    console.log(rs);
                });
            },
            SetNewTBName: function () {
                var self = this;
                var OraTableSchema = {
                    ORANAME: self.currTBName,
                    TBNAME: self.currNewTBName,
                    TBCNAME: self.currNewCTBName,
                    TYP: self.currTableTyp
                };
                $.post(host("/DBTable/SetNewTBName/"), OraTableSchema , function (rs) {
                    self.fetchData();
                });
            },
            openModalTBColumn: function (e) {
                this.currOldColName = e.ORANAME;   //比對若成功就會打開編輯Panel.
                this.newTBClm = e;
                var tr_top = document.getElementById("TR_" + this.currOldColName).offsetTop - 80; //捲到最上方
                console.log(tr_top, this.newTBClm);
                $('html, body').stop().animate({
                    'scrollTop': tr_top
                }, 500, 'swing', function () {
                    //window.location.hash = target;
                });
            },
            SaveOraTableSingleSchema: function () {
                var self = this;
                this.newTBClm.TBSN = this.currTBSN;
                console.log(this.newTBClm);
                $.post(host("/DBTable/SaveOraTableSingleSchema/"), this.newTBClm, function (rs) {
                    self.currTBColumns = rs.colRows;
                    self.currOldColName = "";
                });
            },
            closeModalTBCol: function () {
                this.currOldColName = "";
            },
            IsColumnEnableTransfer: function (e) {
                var self = this;
                e.TBSN = this.currTBSN;
                $.post(host("/DBTable/IsColumnEnableTransfer/"), e , function (rs) {
                    self.currTBColumns = rs.colRows;
                    self.currOldColName = "";
                });
            },
            openModalNumDistinct: function (col, comment) {
                var self = this;
                this.currNumDistinctFieldOld = col;
                this.currNumDistinctFieldComment = comment;
                $(".ui.modal.num_distinct").modal("show");
                self.NumDistinctDetails = null;
                $.post(host("/DBTable/GetNumDistinctDetails/"), { tb: this.currTBName, col: col }, function (rs) {
                    self.NumDistinctDetails = rs;
                });
                
            },
            isStarColumn: function (tbc) {
                var self = this;
                tbc.TBSN = this.currTBSN;
                $.post(host("/DBTable/IsStarColumn/"), tbc , function (rs) {
                    self.currTBColumns = rs.colRows;
                });
            },
            OpenModalColIndex: function () {
                $(".ui.modal.colsIndex").modal("show");
            }
        }
    });

});
