﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        'vue': ['validator/vendor/vue-0.11.1'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    $(document).bind("ajaxSend", function () {
        $("#loading").show();
             }).bind("ajaxComplete", function () {
        $("#loading").hide();
    });

    Vue.debug = true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy/mm/dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    Vue.component("dress-list", {
        template: "#dresslist-template",
        paramAttributes:["gid"],
        data: function () {
            return {
                dresses: null,
                cmp_searchText: "",
                icountDress: 0,
            }
        },
        compiled: function () {
            this.fetchData();
            this.$watch('cmp_searchText', function () {
                this.icountDress= this._children.length;
            });
        },
        computed:{
            isSelected: function () {
                console.log(this.model);
                return this.dresses.gid==this.gid;
            },
            isTest: function () {
                return ture;
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                var papa = self.$parent;
                //console.log("test:", papa , papa.funcStatus, papa.searchUseDate);
                $.post(host("admin/GetDressList"), {
                    type: papa.funcStatus, SearchDate: papa.searchUseDate
                }, function (rs) {
                    //console.log("rs:", rs);
                    self.dresses = rs;
                    self.icountDress = self.dresses.length;
                    self.$emit("dress-loaded");
                });
            },
            getOneDress: function (e) {

                
                var self = this;
                var fm = self.$parent.$.dressfm; // = false;
                //console.log("list compo:", main.isAddNew, fm);
                if (fm) {
                    //console.log("list:", fm.editMode);
                    fm.isAddNew = false;
                    fm.editMode = false;
                }

                $("div.dress").removeClass("selected");
                $(e.target.parentNode).addClass("selected");
                $("#dressForm").find("input,select").attr("disabled", "disabled");
                var raw=e.targetVM._raw;
                main.gid = raw.gid;         //Pass to parent...
                main.masterImg = raw.img2;  //Pass to parent...
            }
        }
    });

    Vue.component("dress-form",{
        template: "#dress-form-template",
        validator: {},
        data: function(){
            return {
                editMode: false, /* view|addnew|update */
                dress: {
                    GUID: "",ID: 0, Color: 1, Typ: 19, Name: "", Code: "", Hanger: 7, VenderID: 57, PerchaseDate: "", Slogan: "", OpenDate: "", OffDate: "", Cost: 0, IsEnabled:"N",InsertDate: null, UpdateTime: null, MasterImage:""
                },
                options: {},
                gid: "",
                isAddNew: false
            };
        },
        created: function () {
            this.editMode = false;
            this.$watch("gid+isAddNew+editMode", function () {
                this.checkDressMode();
            });
        },
        methods: {
            checkDressMode: function () {
                var self = this;
                if (self.editMode) {
                    $("#dressForm").find("input,select").removeAttr("disabled");
                } else {
                    $("#dressForm").find("input,select").attr("disabled", "disabled");
                }
                if (this.isAddNew) {
                    this.AddNewDress();
                } else {
                    this.getOneDressDetail();
                }
            },
            clickSaveDress: function (e) {
                e.preventDefault();
                var self = this;
                this.editMode = false;
                if (self.$valid) {
                    $.post(host("admin/SaveDress"), self.dress, function (rs) {
                        self.$parent.$.refDressList.fetchData();
                        main.gid;
                        self.getOneDressDetail();
                    });
                }
            },
            clickDeleteDress: function () {
                this.editMode = false;
                var self = this;
                if (confirm("確認要刪除? 刪除後不可再復原")) {
                    $.post(host("admin/DeleteDress"), { gid: main.gid }, function (rs) {
                        self.$parent.$.refDressList.fetchData();
                    });
                }
            },
            clickEditDress: function () {
                this.editMode = true;
            },
            clickCancelEdit:function(e){
                this.editMode = false;
            },
            getOneDressDetail: function () {
                var self = this;
                $.post(host("admin/GetDressDetails"), { gid: main.gid, store: main.store }, function (rs) {
                    //var f = self.dress;
                    self.dress = rs;
                    /*f.GUID = rs.GUID;
                    f.ID = rs.ID;
                    f.Name = rs.Name;
                    f.Slogan = rs.Slogan;
                    f.Cost = rs.Cost;
                    f.Code = rs.Code;
                    f.OffDate = rs.OffDate;
                    f.OpenDate = rs.OpenDate;
                    f.PerchaseDate = rs.PerchaseDate;
                    f.InsertDate = rs.InsertDate;
                    f.UpdateTime = rs.UpdateTime;
                    f.VenderID = rs.VenderID;
                    f.IsEnabled = rs.IsEnabled;
                    f.Color = rs.Color;
                    f.Hanger = rs.Hanger;
                    f.Typ = rs.Typ;
                    f.MasterImage = rs.MasterImage;*/
                });
            },
            AddNewDress: function (e) {
                $("#dressForm").find("input,select").removeAttr("disabled");
                var self = this;
                $.post("/Admin/GetStoreMaxCode", {store: main.store}, function (rs) {
                    self.dress = {
                        GUID: "", ID: 0, Color: 1, Typ: 19, Name: "", Code: "", Hanger: 7, VenderID: 57, PerchaseDate: "", Slogan: "", OpenDate: "", OffDate: "", Cost: 0, IsEnabled: "N", InsertDate: null, UpdateTime: null
                    };
                    self.dress.Code = rs;
                    self.editMode = true;
                });
            }
        }
    });


    Vue.component("dress-img", {
        template: "#dress-img-template",
        validator: {},
        data: function () {
            return {
                editMode: false, /* view|addnew|update */
                gid: main.gid,
                imgs: null,
                masterImg: main.masterImg,
                isFrei: false,
                isReadyForUpload: false,
                isDeleting:false,
                imgInforBeforeUpload: "",
                isWarning:false
            };
        },
        created: function () {
            this.fetchData();
            this.$watch("gid", function () {
                this.fetchData();
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                //console.log("Showonedressimages", self.gid);
                if (self.gid != null) {
                    $.post("/Admin/ShowOneDressImages", { DressGuid: self.gid }, function (rs) {
                        self.imgs = rs;
                        self.isDeleting = false;
                    });
                }
            },
            saveFile: function () {
                var fd = new FormData(),
                    token = document.getElementsByName("__RequestVerificationToken")[0].value,
                    dress_gid = document.getElementById("DressGid").value,
                    blob = document.getElementById("files").files,
                    len = blob.length,
                    self = this;

                //console.log("blob", blob, len);
                //fd.append("files", document.getElementById("files").files[0]);
                for (var i = 0; i < len; i++) {
                    fd.append("files", document.getElementById("files").files[i]);
                }
                fd.append("__RequestVerificationToken", token);
                fd.append("dress_gid", dress_gid);
                fd.append("UploadTyp", "DressPhoto");
                $.ajax({
                    url: "/Admin/UploadFile",
                    type: "POST",
                    data: fd,
                    beforeSend: function () {  },
                    success: function () {
                        self.fetchData();
                        self.imgInforBeforeUpload = "";
                        self.isReadyForUpload = false;
                        document.getElementById("files").value = "";
                    },
                    error: function () { console.log("Err"); },
                    processData: false,  // tell jQuery not to procefss the data
                    contentType: false   // tell jQuery not to set contentType
                });
            },
            deleteImg: function (e) {
                var raw = e.targetVM._raw,
                    imgGuid = raw.Guid,
                    self = this;
                if (confirm("確認要刪除" + raw.FileNameNew + "?")) {
                    this.isDeleting = true;
                    $.post("/Admin/RemoveFile", { imgGuid: imgGuid }, function (rs) {
                        if (rs == "RemoveFile Success.") {
                            self.fetchData();
                        }
                    });
                }
            },
            editImg: function (e) {
                var raw = e.targetVM._raw,
                    imgGuid = raw.Guid,
                    Note = raw.Note,
                    self = this;
                $.post("/Admin/EditAttachFileNote", { imgGuid: imgGuid, note:Note }, function (rs) {
                    self.fetchData();
                });
            },
            lockImg: function (e) {
                console.log(e.Guid);
                //$.post("/Admin/LockImg", { imgGuid: imgGuid, note:Note }, function (rs) { self.fetchData(); });
            },
            onMasterImg: function (e) {
                var raw = e.targetVM._raw,
                    imgGID = raw.Guid,
                    selectedImg = raw.FileNameNew,
                    dressGID = this.gid,
                    self = this;
                    
                $.post("/Admin/SetMasterImage", { dressGID: dressGID, imgGID: imgGID, store: main.store }, function (rs) {
                    //console.log(self.$parent.$);
                    var llist = self.$parent.$.refDressList;
                    llist.fetchData();
                    //llist.cmp_searchText=dressGID;
                    main.masterImg = selectedImg;
                });
            },
            FileSelected: function (e) {
                e.preventDefault();
                var files = this.$$.el_uploads.files;
                var fname, c, k=0, ks=0, d, i=0, ss = "";
                fname = files[0].name;
                //console.log("get file:", files, files.length);
                for (d in files) {
                    c = files[d].name;
                    if (c === undefined || c === "item") { } else {
                        ks += files[d].size;
                        ss += c + "<br />";
                    }
                }
                console.log(ks);

                this.imgInforBeforeUpload = ss;
                this.isReadyForUpload = (fname.length > 0);
                if (ks > 10480000) {
                    this.imgInforBeforeUpload = "檔案超過10MB，請減少張數或單張圖片縮小至2MB以內。";
                    this.isReadyForUpload = false;
                    this.isWarning = true;
                } else {
                    this.isWarning = false;
                }
                /*console.log("get file:",files, ss, fname, this.isReadyForUpload);*/
            }

        }
    });

    Vue.component("dress-usage", {
        template: "#dress-usage-template",
        data: function () {
            return {
                editMode: false, /* view|addnew|update */
                gid: main.gid,
                masterImg: main.masterImg,
                usages: null
            };
        },
        created: function () {
            this.fetchData();
            this.$watch("gid", function () {
                this.fetchData();
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                if (this.gid != null) {
                    $.post("/Admin/GetDressUsageList", { store:main.store, DressGuid: this.gid }, function (rs) {
                        self.usages = rs;
                        console.log(rs);
                        //self.isDeleting = false;
                    });
                }
            },
        }
    });

    var main = new Vue({
        el: "#main",
        data: {
            tabs: {
                detail: { n: "禮服明細", b: "active" },
                pics: { n: "圖片管理", b: "" },
                usage: { n: "客戶使用紀錄", b: "" },
                infor: { n: "禮服保養記錄", b: "" }
            },
            currTab: "",
            dresses: null,
            colorOptions: null,
            field: "name",
            funcStatus: "queryAll",//[queryAll|searchUseDate]
            searchUseDate:"",
            options:{
                colorOptions: null, 
                typOptions: null,
                hangerOptions: null,
                venderOptions: null
            },
            gid: "",
            isDressForm: true,
            isDressImg: false,
            isDressUsage: false,
            isAddNew:false, //@@@
            masterImg: "",
            isFrei: false,
            store: $(".container.body-content").attr("data-store")
        },
        created: function () {
            this.isFrei = ($(".brand-name").html() == "frei") ? true : false;
            //console.log(this.store);

            this.GetDressOptions();
            var self = this;
            this.$watch("currTab", function () {
                this.clickTabEE();
            });
            
        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        },
        methods: {
            GetDressOptions: function () {
                var op = this.options;
                var self = this;
                $.post(host("admin/GetDressOptions"), {store: self.store}, function (rs) {
                    op.colorOptions = rs.colorOptions;
                    op.typOptions = rs.typOptions;
                    op.hangerOptions = rs.hangerOptions;
                    op.venderOptions = rs.venderOptions;
                });
            },
            clickTab: function (e) {
                var raw = e.targetVM._raw;
                for (var c in this.tabs) { this.tabs[c].b = ""; }
                raw.b = "active";
                this.currTab = e.targetVM.$key;
                this.clickTabEE();
                this.editMode = false;
                //console.log(raw, this.currTab, e.targetVM.$key);
            },
            clickTabEE: function () {
                this.isDressForm = (this.currTab == "detail");
                this.isDressImg = (this.currTab == "pics");
                this.isDressUsage = (this.currTab == "usage")
            },
            clickAddNewDress: function () {
                this.gid = "  ";
                this.isDressImg = false; //hierachy比較高!
                this.isDressForm = true;
                this.editMode = false;
                this.isAddNew = true;
                //console.log("parent:", this.isAddNew, this.$, main.$.dressfm); //這一段始終有問題!

                //this.$.dressfm.isAddNew = true;  //必要!
                for (var c in this.tabs) { this.tabs[c].b = ""; }
                this.tabs.detail.b = "active";
                this.currTab == "detail";
            },
            Sort: function () {
                console.log("Start sorting...");
            },
            onClickSearchDate: function () {
                this.funcStatus = "searchUseDate";
                var self = this;
                if (this.searchUseDate == "") {
                    alert("請輸入查詢日期。");
                } else {
                    self.$.refDressList.fetchData();
                }
            },
            onClickSearchDateCancel: function () {
                this.funcStatus = "queryAll";
                this.searchUseDate = "";
                this.$.refDressList.fetchData();
            }
        }
    });

});
