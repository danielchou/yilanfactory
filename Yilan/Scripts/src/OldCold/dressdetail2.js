﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug = true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        twoWay: true,
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    /* Dress list component. */
    Vue.component("member-list", {
        template: "#memberlist-template",
        data: function () {
            return {
                members: null,
                searchText: "",
                icountMembers: 0,
                selectedMemberID: "",
            }
        },
        watch: {
            "searchText": function (val, oldVal) {
                this.icountMembers = this.$children.length;
            }
        },
        created: function () {
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Member/GetList"), {}, function (rs) {
                    self.members = rs.members;
                    self.icountMembers = self.members.length;
                    self.$emit("list-loaded");
                });
            },
            clickAddNewDress: function () {
                main.editMode = true;
                main.isDetail = true;
                this.$parent.$.refMemberForm.AddNewMember();
            },
            getOneMember: function (member) {
                main.editMode = false;
                var gid = member.GID;         //Pass to parent...
                this.selectedMemberID = gid;
                main.MemberID = gid;
                main.isDetail = true;
                //this.$parent.$.refMemberForm.memberId = gid;  //透過parent傳遞比較沒有接空參數的問題....
                //console.log(this.$parent.$.refMemberForm.memberId);
            }
        }
    });

    var NewMember = { GID: "", CallName: "", Mate: "", Email: "", Email2: "", TEL: "", Mobile: "", Mobile2: "", Address: "", Address2: "", WeddingDate: "", EngageDate: "", InsertDate: "", UpdateTime: "", BirthDay: "", BirthDay2: "", Note: "" };

    Vue.component("member-form", {
        template: "#member-form-template",
        //replace: true, // cache view...
        props: {
            memberId: { type: String },
            isDetail: { type: Boolean, default: false },
            editMode: { type: Boolean, default: false },
            test: { type: String, default: "12344555MyTest!" }
        },
        data: function () {
            return {
                Member: null,
            }
        },
        watch: {
            "memberId": {
                handler: function (val, oldVal) {
                    //console.log('new: %s, old: %s', val, oldVal);
                    this.fetchData();
                },
                //deep:true
            }
        },
        created: function () {
            //console.log("created member-form", this.memberId, this.isDetail);
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Member/GetOneMember"), { gid: this.memberId }, function (rs) {
                    self.Member = rs;

                    var f = self.Member;
                    f.WeddingDate = ToJavaScriptDate(rs.WeddingDate);
                    f.EngageDate = ToJavaScriptDate(rs.EngageDate);
                    f.Birthday = ToJavaScriptDate(rs.Birthday);
                    f.Birthday2 = ToJavaScriptDate(rs.Birthday2);
                    f.InsertDate = rs.InsertDate;
                    f.UpdateTime = rs.UpdateTime;
                    self.$emit('form-loaded'); //確保資料進來後才會display view.
                });
            },
            hideDetail: function () {
                main.isDetail = false;
            },
            AddNewMember: function () {
                //console.log('addnew');
                this.editMode = true;
                this.Member = NewMember;
            },
            clickEdit: function () {
                this.editMode = true;
            },
            clickSave: function (e) {
                e.preventDefault();
                var self = this;
                $.post(host("Member/SaveMember"), this.Member, function (rs) {
                    self.fetchData();
                    self.editMode = false;
                    self.$parent.$.refMemberlist.fetchData();
                });//.bind(this);
            },
            clickCancel: function () {
                this.editMode = false;
            }
        }

    });

    var main = new Vue({
        el: "#main",
        data: {
            editMode: false,
            MemberID: "",
            isDetail: false
        },
        watch: {
            "MemberID": function () {
                //console.log("main", this.MemberID);
                //this.$.refMemberForm.fetchData();
            }
        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        }
    });



});
