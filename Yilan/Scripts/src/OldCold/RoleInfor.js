﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug=true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        twoWay:true,
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    Vue.component("roles-infor", {
        template: "#roles-infor-template",
        data: function () {
            return {
                store: null,
                storeAdminUser:null
            }
        },
        created: function () {
            this.fetchData();
        },
        methods:{
            fetchData: function () {
                var self = this;
                $.post("/Admin/GetRoleInforList", {}, function (rs) {
                    self.store = rs.Store;
                    self.storeAdminUser = rs.StoreAdminUser;
                    //self.$emit("data-loaded");
                });
            }
        }
    });

    Vue.component("embeded-infor", {
        template: "#showroom-embeded-template"
    });

    var titles = [{ id: 1, n: "主頁面" }, { id: 2, n: "展示櫥窗崁入" }]

    var main = new Vue({
        el: "#main",
        data: {
            titles: titles,
            selectedTitle:2
        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); }
        },
        methods: {
            clickTitle: function (e) {
                var id = e.id;
                this.selectedTitle = id;
            }

        }
    });
   
});

