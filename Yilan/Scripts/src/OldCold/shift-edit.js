﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        "jquery": ['jquery/dist/jquery.min'],
        //"core": ["jqueryui/ui/minified/core.min"],
        "jqueryui": ["jqueryui/jquery-ui.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.min"],
        "fullcalendar": ["fullcalendar/dist/fullcalendar.min"],
        "moment":["moment/min/moment.min"]
    },
    shim: {
        "semantic": { deps: ["jquery"], exports: "semantic" },
        "jqueryui": ["jquery"]
    }
});

define(["vue", "jquery", "fullcalendar", "moment", "jqueryui"], function (Vue, $, fullCalendar) {
    var EmpName = $("#main").attr("data-empname");
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

    var main = new Vue({
        el: "#main",
        data: {
            Employees: {},
            CurrStartDate: "",
            Typ: "",
            CopyFrom:""
        },
        created: function () {
           
            $('#external-events .fc-event').each(function () {
                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()),  // use the element's text as the event title
                    //stick: true                   // maintain when user navigates (see docs on the renderEvent method)
                });

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                })
                .data("duration", "01:00");  // 否則預設每次新增為兩小時， http://fullcalendar.io/docs/event_data/defaultTimedEventDuration/
            });



            var self = this;
            self.CurrStartDate = $("#main").attr("CurrStartDate");
            self.Typ = $("#main").attr("Typ");
            self.CopyFrom = $("#main").attr("CopyFrom");

            $("#calendar").fullCalendar({
                header: {
                    left: '',
                    center: "title",
                    right: ''
                },
                //defaultDate: '2016-04-23',
                titleFormat: "YYYY MM",
                timeFormat:"HH(:mm)",
                dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
                defaultDate: self.CurrStartDate,
                defaultView: 'agendaWeek',
                views: {
                    agendaWeek: {
                        titleFormat: "YYYY/MM/DD",
                        buttonText: "一週六天"
                    },
                },
                editable: true,
                selectable: true,              
                weekNumbers: true,
                hiddenDays: [0],
                allDaySlot: false,
                slotDuration: "01:00:00",
                slotEventOverlap:false,
                minTime: "09:00:00",
                maxTime: "24:00:00",
                eventLimit: false, // allow "more" link when too many events
                events: "/shift/GetSingleEmployee/" + EmpName,
                eventRender: function (event, element) {
                    $(element).attr("data-sn", event.sn)
                            .prepend("<i class='ui circular small inverted remove icon'></i>");
                },
                //eventColor: '#aaa',
                eventOrder: "title",
                eventLimit: 10,
                droppable: true,
                drop: function (date) {
                    var start = date.format()
                        , end = date.add(1, "hours").format();
                    self.updateCalendarEventForDrop(start, end);
                },
                eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                    self.updateCalendarEvent("drag stop", event);
                },
                eventResize: function (event, delta, revertFunc) {
                    self.updateCalendarEvent("resize:", event);
                },
                eventClick: function (event, jsEvent, view) {
                    self.removeShift(event.sn);
                }
            });

            self.fetchData();
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            //accounting: function (v) { return accounting.formatMoney(v); }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("/shift/GetEmployees"), {}, function (rs) {
                    self.Employees = rs;
                    $("#emps-list").fadeIn("slow");
                });
            },
            updateCalendarEventForDrop: function (start, end) {
                var self = this;
                $.post(host("/shift/UpdateOneEmployeeShfit"), {SN:-1, EmpName: EmpName, start: start, end: end, typ: self.Typ, copyFrom: self.CopyFrom }, function (rs) {
                    $("#calendar").fullCalendar("refetchEvents");
                });
            },
            updateCalendarEvent: function (tag, e) {
                var self=this, sn = e.sn, start = e.start.format(), end = e.end.format();
                $.post(host("/shift/UpdateOneEmployeeShfit"), { SN: sn, EmpName: EmpName, start: start, end: end, typ: self.Typ, copyFrom: self.CopyFrom }, function (rs) {
                    $("#calendar").fullCalendar("refetchEvents");
                });
            },
            copyShifToNextWeek: function () {
                var mo = $("#calendar").fullCalendar("getDate"),
                    startDt = mo.startOf("week").add(1, "days").format(),
                    endDt = mo.startOf("week").add(6, "days").format();

                //console.log(mo.format(), mo.weeks(), startDt, endDt);
                $.post(host("/shift/copyShifToNextWeek"), { EmpName: EmpName, start: startDt, end: endDt }, function (rs) {
                    //console.log(rs);
                    //$("#calendar").fullCalendar("refetchEvents");
                });
            },
            removeShift: function (ShiftSN) {
                //console.log("remove", ShiftSN);
                $.post(host("/shift/removeShift"), { ShiftSN: ShiftSN }, function (rs) {
                    $("#calendar").fullCalendar("refetchEvents");
                });
            }
        }
    });

});
