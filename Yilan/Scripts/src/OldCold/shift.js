﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.min"],
        "fullcalendar": ["fullcalendar/dist/fullcalendar.min"],
        "moment":["moment/min/moment.min"]
    },
    shim: {
        "semantic": { deps: ["jquery"], exports: "semantic" },
        "datepicker": ["jquery"]
    }
});

define(["vue", "jquery", "fullcalendar", "moment"], function (Vue, $, fullCalendar) {

    $(document).bind("ajaxSend", function () {
        $("#loading").show();
    }).bind("ajaxComplete", function () {
        $("#loading").hide();
    });

    var main = new Vue({
        el: "#main",
        data: {
            Employees: {},
            isFastCopyHide: true,
            currWkTag:"",
            nextWkTag: "",
            currHeadDate: "", //月曆Title上的日期，(YYYY/MM/DD)
            msgBar: "",
            isHidePatternPanel: true,
        },
        created: function () {
           
            var self = this;
            $("#calendar").fullCalendar({
                customButtons: {
                    CopyFastBtn: { text: "刪除本周班表", click: function () {self.clickRemoveAllWkShift(); } }
                },
                header: {
                    left: 'prev,next,today,CopyFastBtn,SaveAsPatternBtn',
                    center: "title",
                    right: 'agendaFourDay,agendaWeek'
                },
                titleFormat: "YYYY MM",
                timeFormat:"HH(:mm)",
                dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
                defaultView: 'agendaWeek',
                views: {
                    agendaFourDay: {
                        type: "agenda",
                        titleFormat:"YYYY/MM/DD",
                        duration: { days: 4 },
                        buttonText: "4 day"
                    },
                    agendaWeek: {
                        titleFormat: "YYYY/MM/DD",
                        buttonText: "一週六天"
                    },
                },
                editable: false,
                selectable: false,
                weekNumbers: true,
                hiddenDays: [0],
                slotDuration: "01:00:00",
                allDaySlot:false,
                slotEventOverlap:true,
                minTime: "09:00:00",
                maxTime: "24:00:00",
                eventLimit: false, // allow "more" link when too many events
                //events: "/shift/GetAllEmployees",
                events:{
                    url:"/shift/GetAllEmployees",
                    typ: "POST",
                    data: {},
                    error: function () {
                        alert("班表資料截取發生錯誤!");
                    },
                },
                eventRender: function (e, element, view) {
                    //console.log(e.title);
                    var user = e.title;
                    element.append("<div class='ev-panel' data-evid='" + "'><a class='ev-user' href='#' target='_blank'>" + user + "</a></div>");
                },
                
                eventAfterAllRender: function (view) {
                    self.setIsFastCopyHide();
                },
                //eventClick: function (event, element) { 案下某天就會引發
                //    //event.title = "CLICKED!";
                //    alert("clickQQ");
                //    //$('#calendar').fullCalendar('updateEvent', event);
                //},
                //resources: function(callback) {

                //    somethingAsynchonous(function(resourceObjects) {
                //        console.log(resourceObjects)
                //        callback(resourceObjects);
                //    });
                //},
                //eventColor: '#eee',
                eventOrder: "title",
                eventLimit:10
            });

            this.fetchData();

        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            //accounting: function (v) { return accounting.formatMoney(v); }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("/shift/GetEmployees"), {}, function (rs) {
                    self.Employees = rs;
                    $(".sys-title").fadeIn("slow");
                    self.setIsFastCopyHide();
                });
            },
            editOnesShift: function (empName) {
                
                location.href = host("/shift/EditSingleEmployee?EmpName=" + empName + "&currWkTag=" + this.currWkTag);
            },
            getCurrWkTag: function () {
                this.currHeadDate = $(".fc-center h2").html().substr(0, 10);
                var wk = $(".fc-widget-header th:first span").html();
                
                if (wk.length == 2) { wk = "W0" + wk.substr(1, 1); }

                this.currWkTag = this.currHeadDate.substr(2, 2) + wk;
                console.log(this.currHeadDate, wk, this.currWkTag)
            },
            setIsFastCopyHide: function () {
                var self = this;
                this.getCurrWkTag();

                $.post(host("/shift/GetNextWeekTag"), { currWkTag: self.currWkTag }, function (rs) {
                    self.nextWkTag = rs;
                    self.isFastCopyHide = !(rs.currWkTagCC > 0 && rs.nextWkTagCC == 0);
                    self.isHidePatternPanel = rs.currWkTagCC > 0;
                    console.log(rs.currWkTagCC,  self.isFastCopyHide);
                    if (self.isFastCopyHide) {
                        $(".fc-CopyFastBtn-button").hide();
                    } else {
                        $(".fc-CopyFastBtn-button").show();
                    }
                    self.msgBar = "";
                });
            },
            //clickFastCopy: function () {
            //    this.getCurrWkTag();
            //    //alert(this.currWkTag);
            //    var self = this;
            //    $.post(host("/shift/FastCopyToNext"), { currWkTag: self.currWkTag, currHeadDate: self.currHeadDate }, function (rs) {
            //        if (rs == "Success") {
            //            self.msgBar = "複製下周班表成功!";
            //        }
            //    });
            //},
            clickSaveAsPattern: function () {
                var self = this;
                $.post(host("/shift/SaveAsPattern"), { currWkTag: self.currWkTag }, function (rs) {
                    if (rs == "Success") {
                        self.msgBar = "已經存為樣板，請至[預覽樣版]查看。";
                    } else {
                        self.msgBar = rs;
                    }
                });
            },
            clickRemoveAllWkShift: function () {
                var self = this;
                $.post(host("/shift/RemoveAllWkShift"), { currWkTag: self.currWkTag }, function (rs) {
                    if(rs.msg=="Success") {
                        self.msgBar = "整周刪除成功!";
                        $("#calendar").fullCalendar("gotoDate", rs.startdate);
                        //setTimeout("self.msgBar='';", 3000);
                    }else{
                        self.msgBar=rs;
                    }
                });
            }
        }
    });


    //$(".fc-next-button").click(function () {
    //    alert("next");
    //});

});
