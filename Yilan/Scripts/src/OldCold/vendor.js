﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug=true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    Vue.component("dress-img-picker", {
        template: "#dressImg-Picker-Template",
        data: function () {
            return {
                dressImgs: null,
                searchImgs: "",
                icountPicked: 0,
                PickedDress: [],
                PickedDressOrigin: []
            }
        },
        compiled: function () {
            this.fetchData();
            this.$watch('searchImgs', function () {
                this.icountPicked = this.PickedDress.length;
            });
        },
        methods:{
            fetchData: function () {
                var self = this;
                $.post("/Admin/ShowAllDressMasterImage", {}, function (rs) {
                    self.dressImgs = rs;
                    self.$emit("imgs-loaded");
                });
            }
        }
    });

    var main = new Vue({
        el: "#RsrvEditor",
        data: {},
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); }
        }
    });
   
});

