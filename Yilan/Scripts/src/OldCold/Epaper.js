﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "/Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.min"]
    },
    shim: {
        //"semantic": { deps: ["jquery"], exports: "semantic" },
        "datepicker": ["jquery"]
    }
});

// http://localhost:57200/Auth/HiddenLogin?UserCode=4BB6B0A0-A627-47A8-9226-F5A784A7C6D1

define(["vue", "semantic"], function (Vue, semantic) {
    //$(".ui.sticky").sticky();
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

    var main = new Vue({
        el: "#main",
        data: {
            QueryPara: {
                ACCOUNTNO: "",// "711-E40016",
                RECEIVINGNO: "",//"340-E40063",
                INVOICEID: "",//BM31413345,CG28321394
                STOCKINGNO: ""
            },
            IsOpenBigImage: false,
            Invoices: {},
            fileNamesList:[],
            totalImageCC: 0,
            currImageIndex: 0,
            currImageFile: "",
            
            unboundedImages: {},  //未綁定圖檔名
            DateTags:{},          //未綁定圖片標籤，方便USER查詢
            unboundedImagesTotalCC:0,

            selectedDateTag:"",
            selectedItem: {},
            selectedInvoiceID: "",
            selectedUnbindImage: "",
            isOpenBindPanel: false,
            FileTypes: [
                { FileType: 0, typ:0, label: "未綁定" },
                { FileType: 1, typ:1, label: "發票" },
                { FileType: 2, typ:2, label: "明細" },
                { FileType: 3, typ:3, label: "收貨單" },
                { FileType: 4, typ:4, label: "折讓" },
                { FileType: 5, typ:5, label: "後補" },
            ],
            currFileType: 1,
            currIsWaiting:false,
            NewImgToBind: {
                ACCOUNTNO:"", RECEIVINGNO: "", INVOICEID: "", INVOICEDATE:"", STOCKINGNO: "", VENDOR:"",
                SERVERIP: "",FILENAME: "",FILETYPE: 1,MODIFIEDUSER: "",NOTE: "", ISWAITING:"N"
            },
            STT: {},
            isSTT: false,
            isQueryMulti: false,
            currYYMM: "",
            currFileType:"",
        },
        created: function () {
            //var AccountNo = ($("label.actnolb").attr("title") == "") ? "" : $("label.actnolb").attr("title");
            this.QueryPara.ACCOUNTNO = $("label.actnolb").attr("title");
            this.fetchData();
            this.fetchStaticsData();
            window.addEventListener("keyup", this.checkKeyPress);
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            stay: function (v) { return v; },
            trimFileExtend: function (v) { return v.split(".jpg")[0]; },
            formatIsWaiting: function (v) { return (v) ? "需要" : "不需要"; },
            formatFileType: function (v) { return this.FileTypes.filter(function (pp) { return pp.FileType === v })[0].label; }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: host("/Epaper/ImageDataSource"),
                    data: { m: self.QueryPara },
                    //headers: { token: epctoken },
                    success : function(rs){
                        //console.log(rs);
                        if (rs === "no query data") {
                            
                        } else {
                            self.Invoices = rs.tbs;
                        }
                    }
                });

                this.fetchTagedImages();
            },
            fetchTagedImages: function () {
                var self = this;
                $.post(host("/Epaper/UnbindedImageStorage"), { DateTag: self.selectedDateTag }, function (rs) {
                    self.unboundedImages = rs.unbindImages;
                    self.DateTags = rs.DateTags;
                    self.unboundedImagesTotalCC = rs.TotalCC;
                });
            },
            fetchStaticsData: function () {
                var self = this;
                $.post(host("/Epaper/QueryStatics"), {}, function (rs) {
                    var ymo = {},bb="";
                    for (var b in rs) { bb=rs[b].YYMM; ymo[bb] = "yyyymm"; }//console.log("ymo", ymo);

                    var sttarr = [], fltrArr = [], d;
                    for (var ym in ymo) {
                        var dd = {};
                        dd["yymm"] = ym;
                        for (var c in rs) {
                            d = rs[c]; if (d.YYMM == ym) { dd["typ" + d.FILETYPE] = d.CC; }
                            //console.log(dd,d,ym,d.CC);
                        }
                        sttarr.push(dd);
                    }
                    //console.log("stto",sttarr);
                    self.STT = sttarr;
                });
            },
            clickQuery: function () {
                //console.log(this.QueryPara)
                this.isOpenBindPanel = false;
                this.isMulti = false;
                this.fetchData();
            },
            //clickStatics: function () {
            //    this.fetchStaticsData();
            //    this.isSTT = !this.isSTT;
            //},
            clickBigImage: function (imgs, imageName, index) {
                var self= this;
                self.IsOpenBigImage = true
                self.fileNamesList = imgs;
                self.totalImageCC = self.fileNamesList.length;
                console.log(imgs, imageName, index, self.totalImageCC)
                self.currImageIndex = index
                self.currImageFile = imageName
            },
            checkKeyPress: function (kb) {
                
                if (kb.code === "ArrowLeft" || kb.code === "ArrowRight" || kb.code === "Escape") {
                    this.IsOpenBigImage = true;
                    console.log(kb.code, this.currImageIndex); //keyboard
                    if (kb.code === "ArrowLeft") { this.clickBigImageLeft(); }
                    if (kb.code === "ArrowRight") { this.clickBigImageRight(); }
                    if (kb.code === "Escape") { this.clickBigImageRemove(); }
                }
            },
            clickBigImageRemove: function (e) {
                this.IsOpenBigImage = false;
            },
            clickBigImageRight: function () {
                var i = this.currImageIndex + 1;
                this.currImageIndex = (this.totalImageCC > i) ? i : 0;
                this.currImageFile = this.fileNamesList[this.currImageIndex].NEWFILENAME;
                console.log(this.currImageIndex, this.currImageFile, this.totalImageCC)
            },
            clickBigImageLeft: function () {
                var i = this.currImageIndex - 1;
                this.currImageIndex = (i < 0) ? this.totalImageCC-1 : i;
                this.currImageFile = this.fileNamesList[this.currImageIndex].NEWFILENAME;
                console.log(this.currImageIndex, this.currImageFile, this.totalImageCC)
            },
            clickSelectedItem: function (item) {
                this.selectedItem = item;
                console.log(item);
                this.selectedInvoiceID = item.APB11;
                this.isOpenBindPanel = true;
            },
            clickSelectedUnbindImg: function (img) {
                console.log(img);
                this.selectedUnbindImage = img;
            },
            clickBinded: function () {
                var n = this.NewImgToBind;
                var se = this.selectedItem;
                n.SN = 0;
                n.ISWAITING = (this.currIsWaiting) ? "Y" : "N";
                n.FILETYPE = this.currFileType;
                n.FILENAME = this.selectedUnbindImage;
                n.ACCOUNTNO = se.APB01;
                n.RECEIVINGNO = se.APB04;
                n.STOCKINGNO = se.APB21;
                n.INVOICEID = se.APB11;
                n.INVOICEDATE = ToJavaScriptDate(se.APK05);
                n.VENDOR = "";
                n.NOTE = "";

                var self = this;
                $.post(host("/Epaper/SaveImageBinding"), n, function (rs) {
                    if (self.isMulti) {
                        self.queryMutliACCNo(self.currYYMM, self.currFileType);
                    } else {
                        self.fetchData();
                    }
                });
            },
            queryMutliACCNo: function (yymm, filetype) {
                this.isQueryMulti = true;
                this.currYYMM = yymm;
                this.currFileType = filetype;

                var self = this;
                $.post(host("/Epaper/QueryBindedACCNo"), { YYMM: yymm, FileType: filetype }, function (rs) {
                    self.Invoices = rs;
                });
            },
            clickRemoveBindedImg: function (sn) {
                var self = this;
                $.post(host("/Epaper/RemoveBindedImg"), {SN: sn}, function (rs) {
                    self.fetchData();
                });
            },
            clickQueryDateTagImages: function (tag) {
                this.selectedDateTag = tag;
                this.fetchTagedImages();
            },
            clickQueryUnbindACCNo: function (yymm) {
                var self=this;
                $.post(host("/Epaper/QueryUnBindingACCNo"), { yymm: yymm }, function (rs) {
                    self.Invoices = rs;
                });
            },

        }
    });

});
