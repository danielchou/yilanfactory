﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug=true;

    var main = new Vue({
        el: "#Main",
        data: {},
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); }
        },
        methods: {
            clickBig: function () {
                alert("ee");
            }
        }
    });
    
});

