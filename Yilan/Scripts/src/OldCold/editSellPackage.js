﻿requirejs.config({
    baseUrl: "/Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic-ui": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.min"]
    }
});

define(["vue", "validator", "jquery", "accounting", "semantic-ui", "datepicker"]
    , function (Vue, validator, $, accounting) {

        Vue.debug = true;
        var jQuery = $;
        var NewOrderItemCost = {
                OrderID: "",
                Name: "",
                OrderSellPackage: "",
                isPattern: "Y",
                Store:"",
                Cost: 0,
                Count: 1,
                Note: ""
        };

        Vue.component("order-contract", {
            template: "#order-contract-template",
            inherit: true,
            data: function () {
                return {
                    OrderID: "",
                    currTab: "",
                    editMode: false,
                    status: "read",
                    OrderSellPackage: null,
                    isSellPackage: false,
                    SelectedSellPackageID: "",
                    OrderItemCost: {},
                    OrderItemCosts: {},      //已選定的合約項目
                    OrderItemCostPattern: {},  //所有合約項目下拉選單
                    CostSum: 0,
                    currItemCost: "",
                    currTab:"",
                }
            },
            props: {
                orderId: { type: String },
                memberId: { type: String },
            },
            created: function () {
                //console.log("orders created", this.OrderTitleTyps, this.currOrderTitleTyp, this.OrderItemCosts);
                this.fetchData();
            },
            ready: function () {
                $(".ui.dropdown").dropdown();
                //console.log("orders ready", this.memberId, this.OrderTitleTyps); //?
            },
            filters: {
                transferOrderItemTyp: function (v) {
                    var es = this.OrderSellPackage, c, d;
                    for (c in es) {
                        d = es[c]; if (d.GID === v) { return d.Name; }
                    }
                }
            },
            methods: {
                fetchData: function () {
                    var self = this;
                    //console.log("init get orderID:", self.orderId);
                    $.post(host("Order/ShowEditSellPackageJson"), { packageID: this.currTab }, function (rs) {
                        self.OrderItemCosts = rs.items;
                        self.OrderSellPackage = rs.packages;
                        self.CostSum = 0;
                        for (var item in self.OrderItemCosts) {
                            var o = self.OrderItemCosts[item];
                            //console.log(o.Count, o.Cost);
                            self.CostSum += o.Cost * o.Count;
                        }
                    });
                },
                clickTab: function (e) {
                    this.currTab = e;
                    this.fetchData();
                },
                clickNewSellPackage: function () {
                    this.editMode = this.isSellPackage = true;
                },
                clickNewItem: function () {
                    this.status = "新增項目"
                    this.editMode = true;
                    this.isSellPackage = false;
                    this.OrderItemCost = NewOrderItemCost;
                },
                ChoiceSellPackage: function (sellPackageID) {
                    this.SelectedSellPackageID = sellPackageID;
                    console.log(main.OrderID, sellPackageID);
                },
                clickSave: function (e) {
                    e.preventDefault();
                    this.editMode = false;
                    var self = this;
                    self.OrderItemCost.OrderID = "temp";
                    self.OrderItemCost.OrderSellPackage = self.currTab;
                    self.OrderItemCost.Store = $("#main").attr("data-store");
                    //console.log("save.", self.OrderItemCost);
                    $.post(host("Order/SaveOrderItemCost"), self.OrderItemCost, function (rs) {
                        var list = self.$parent.$.refOrder;
                        list.fetchData();
                    });
                },
                clickEdit: function (OrderItemCost) {
                    var i = OrderItemCost;
                    this.editMode = true;
                    this.isSellPackage = false;
                    this.currItemCost = i.GID;
                    this.OrderItemCost = i;  //這邊套入的很輕鬆!
                },
                clickRemove: function (GID) {
                    var self = this;
                    if (confirm("確定要刪除這筆資料?")) {
                        console.log(GID);
                        $.post(host("Order/RemoveOrderItemCost"), { OrderItemCostGID: GID }, function (rs) {
                            var list = self.$parent.$.refOrder;
                            list.fetchData();
                        });
                    }
                },
                clickCancel: function () {
                    this.editMode = false;
                    this.OrderItemCost = {};
                },
                clickEditSellPackageList: function () {
                    location.href = host("Order/ShowEditSellPackageList/");
                }

            }

        });

        var main = new Vue({
            el: "#main",
            data: {
                editMode: false,
                OrderID: "",
            },
            created: function () {},
            filters: {
                formatDate: function (v) { return ToJavaScriptDate(v); },
                formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
                accounting: function (v) { return accounting.formatMoney(v); }
            },
            methods: {}
        });

        $(document).bind("ajaxSend", function () {
            $("#loading").show();
        }).bind("ajaxComplete", function () {
            $("#loading").hide();
        });


    });