﻿requirejs.config({
    baseUrl: "/Scripts/vendor/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        "vue": ['vue/dist/vue.min'],
        "validator": ['validator/validator'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"]
    }
});

define(["vue", "validator", "jquery", "datepicker"], function (Vue, validator, $) {
    Vue.debug=true;
    Vue.use(validator);
    Vue.directive('datepicker', {
        //twoWay: true,
        deep:true,
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            //console.log(this.el, key, $(this.el).attr("id"));
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val,oldVal) {
            //console.log("inside the custom datepicker.", val, oldVal);
            $(this.el).datepicker('setDate', val);
        }
    });


    Vue.component("dress-img-picker", {
        template: "#dressImg-Picker-Template",
        inherit: true,  //inherit all properties of parent.
        data: function () {
            return {
                //isEditMode:false,
                dressImgs: null,
                searchImgs: "",
                icountPicked: 0,
                PickedDress: [],
                PickedDressOrigin: [],
                UsagedDress: null,
                iUsagedDress:0,
            }
        },
        compiled: function () {
            this.fetchData();
            this.$watch('searchImgs', function () {
                this.icountPicked = this.PickedDress.length;
                //console.log("triggered search", this.iUsagedDress);
                
                //this._children.map(this.addUsagedInfor);

                /* 找尋的路也太辛苦了!! */
                var iMatched = this.$children.filter(function (v) { return v._raw.isUsaged; }).length;
                /*for (var c in this.$children) {
                    var d = this.$children[c];
                    console.log(d._raw.isUsaged);
                }*/
                this.iUsagedDress = iMatched;
                console.log(this.$children, iMatched);
            });
        },
        watch: {
            "UsagedDress": function (val, oldVal) {
                deep: true,
                this.dressImgs=this.dressImgs.map(this.addUsagedInfor);
                //console.log( this.dressImgs, "triggered");
            }
        },
        filters: {
            IsUsaged: function (v) {
                
                var uds=this.UsagedDress,
                    d=null;
                for (var c in uds) {
                    //console.log(v, uds);
                    d=uds[c];
                    return (v == d.TargetID);
                        //return "- " + d.callname + "-" + d.CountDays + "-" + d.EventName;
                }
                //return "Y";//(v==this.UsagedDress.TargetID)
            },
            GetUsageInfo: function () {

            }
        },
        methods:{
            fetchData: function () {
                var self = this;
                console.log("img",self.isEditMode);
                $.post("/Admin/ShowAllDressMasterImage", {}, function (rs) {
                    //console.log(rs);
                    self.dressImgs = rs;
                    self.$emit("imgs-loaded");
                });
            },
            openDressImgs: function () {
                this.isEditMode = true;
            },
            OnPickedDress: function (dress, isUsaged, obj) {
                var self=this;
                /*var d = dress.$data;  //舊的寫法不行!! */
                var d = dress._raw;
                console.log(d, obj);
                if (!isUsaged) {
                    self.PickedDressOrigin.push(d);
                    self.dressImgs.$remove(d);
                    self.icountPicked = self.PickedDressOrigin.length;
                } else {
                    var u = obj;
                    alert("被 " + u.callname + "於" + ToJavaScriptDate(u.start) + "~" + ToJavaScriptDate(u.end) + " 預約了!")
                }
            },
            RemovePickedDress: function (dress) {
                var self = this;
                var d = dress.$data;
                self.PickedDressOrigin.$remove(d);
                self.dressImgs.unshift(d);
                self.icountPicked = self.PickedDressOrigin.length;
                //console.log(self.PickedDressOrigin);
            },
            addUsagedInfor: function (dress) {
                /*觀念：self不用會卡住，*/
                var self = this, tid = dress.TargetID;
                var usagedInfo = self.UsagedDress.filter(function (v) { return v["TargetID"] == tid; })[0];
                var isUsaged = (usagedInfo != undefined);
                //if (isUsaged) { console.log("inside the dressImgs", tid, usagedInfo); }
                dress.$set("isUsaged", isUsaged);
                dress.$set("usagedInfor", usagedInfo);
                if (isUsaged) { this.iUsagedDress += 1; console.log("ddd"); }
                return dress;
            }
        }
    });

    var EmptyRsrvData = {
        SN: "",
        Typ: "Calendar",
        Event: "",
        Data:"",
        StartDateTime: "",
        EndDateTime: "",
        Note: "",
        CustomerID: "",
        DayTyp:""
    }

    Vue.component("rsrv-main", {
        template: "#Rsrv-Main-tmeplate",
        inherit: true,  //inherit all properties of parent.
        /* props, data都是暫存區資料，只是一個對外一個對內 */
        props: {
            customerId: { type: String, default:"" },
            eventId: { type: String, default: "" }
        },
        data: function () {
            return {
                RsrvID:"",
                RsrvTyp: null,
                RsrvData: null,
                currRsrvTypID: "EBE13137",  //Todo: get init data from DOM.
                isShowEndDate: true,
                isEditMode: false,
                currDayTyp: "all",
                dayTyps :[
                    { dtkey: "all", dscr: "全天", dt1: "08:00:00", dt2: "22:00:00" },
                    { dtkey: "am", dscr: "上午", dt1: "08:00:00", dt2: "12:00:00" },
                    { dtkey: "pm", dscr: "下午", dt1: "12:00:00", dt2: "18:00:00" },
                    { dtkey: "night", dscr: "晚上", dt1: "18:00:00", dt2: "22:00:00" },
                    { dtkey: "cross", dscr: "跨日", dt1: "08:00:00", dt2: "18:00:00" }
                ]
            }
        },
        created: function () {
            this.fetchData();
        },
        compiled: function () {
            var self = this;
            if (this.isNewAdd) {
                self.RsrvData = EmptyRsrvData;
                self.RsrvData.CustomerID = self.customerId;
                self.RsrvData.Event = self.eventId;
                //console.log("??", self.RsrvData.InsertDate, self.RsrvData.InsertDate == undefined);
            }
            this.$watch("RsrvData.StartDate + RsrvData.EndDate + currDayTyp", function (val, oldVal) {
                this.isShowEndDate = (this.currDayTyp == "cross");
                this.DayTypHandler();
            });
        },
        methods: {
            fetchData: function () {
                if (this.RsrvID != "") { this.isEditMode = true; }
            },
            /*合併日期與時間*/
            DayTypHandler: function () {
                var me = this,
                    r = me.RsrvData;
                /* 從物件陣列中找出唯一符合的key值，但是吐回陣列，所以要找第一個元素 */
                var dkey = me.dayTyps.filter(function (v) { return v["dtkey"] == me.currDayTyp; })[0];
                //if (r.EndDate == null) { r.EndDate = r.StartDate; }
                //console.log(me, me.currDayTyp, r);
                r.DayTyp = me.currDayTyp;
                r.StartDateTime = r.StartDate + " " + dkey.dt1;
                r.EndDateTime = ((me.isShowEndDate) ? r.EndDate : r.StartDate) + " " + dkey.dt2;
                //console.log(r.StartDateTime, r.EndDateTime, me.currDayTyp, dkey.dt1, r.EndDate);
                this.GetReservedDress();
            },
            SaveRsrvDate: function () {
                var self = this;
                var RsrvDetails = self.$parent.$.dressImgsPicker.PickedDressOrigin;
                this.DayTypHandler();
                //console.log("Save:", self.RsrvData, RsrvDetails, self.RsrvData.DayTyp);
                $.post("/Calendar/SaveRsrvDate", { RsrvData: self.RsrvData, RsrvDetail: RsrvDetails }, function (rs) {
                    location.href = "/timeline/treeview/" + self.RsrvData.CustomerID;
                });
            },
            RemoveRsrvDate: function () {
                var self = this;
                $.post("/Calendar/RemoveRsrvDate", { RsrvData: self.RsrvData}, function (rs) {
                    location.href = "/timeline/treeview/" + self.RsrvData.CustomerID;
                });

            },
            GetReservedDress: function () {
                var self = this, r = self.RsrvData;
                $.post("/Calendar/GetReservedDress", { StartDateTime: r.StartDateTime, EndDateTime: r.EndDateTime }, function (rs) {
                    var o = self.$parent.$.dressImgsPicker;
                    o.UsagedDress = rs;
                    //console.log(rs);
                });
            }
        }
    });

    var main = new Vue({
        el: "#RsrvEditor",
        props: {
            rsrvId: { type: String, default: "" },
            isNewAdd: { type: String, default: "" },
            isEditMode: { type: Boolean, default: true }
        },
        data: {
            customerName: "",
            RsrvName: "",
            RsrvIcon: "",
            isEditMode:false
        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        },
        created: function () {
            console.log(this.rsrvId);
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var self = this;
                if (self.isNewAdd == "Y") {
                    self.isEditMode = true;
                } else { 
                    $.post("/Calendar/GetRsrvDetails", { RsrvID: self.rsrvId }, function (rs) {
                        var f = self.$.refForm;
                        f.RsrvData = rs.ReservDate;                                 //master
                        f.currDayTyp = f.RsrvData.DayTyp;
                        f.RsrvData.StartDate = ToJavaScriptDate(f.RsrvData.StartDateTime);
                        f.RsrvData.EndDate = ToJavaScriptDate(f.RsrvData.EndDateTime);
                        self.RsrvName = rs.RsrvTyp.Name;
                        self.RsrvIcon = rs.RsrvTyp.Icon;
                        self.$.dressImgsPicker.PickedDressOrigin = rs.RsrvDetails;  //siblings
                        self.$data.customerName = rs.CustomerName;                  //names pass to parent.
                       // console.log("dayTyp:", f.currDayTyp, dayTyps[f.currDayTyp]);
                    });
                }
            }
        }
    });
   
});

