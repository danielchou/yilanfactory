﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.min"]
    },
    shim: {
        "semantic": {
            deps: ["jquery"],
            exports:"semantic"
        },
        "datepicker":["jquery"]
    }
});

define(["vue", "validator", "jquery", "accounting", "semantic", "datepicker"], function (Vue, validator, $, accounting) {

    $(document).bind("ajaxSend", function () {
        $("#loading").show();
    }).bind("ajaxComplete", function () {
        $("#loading").hide();
    });




    Vue.debug = false;
    Vue.use(validator);
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    Vue.component("member-list", {
        debug: true,
        template: "#memberlist-template",
        inherit: true,
        data: function () {
            return {
                members: null,
                searchText: "",
                icountMembers: 0,
                selectedMemberID: "",
                selectedOrderID:"",
            }
        },
        watch: {
            "searchText": function (val, oldVal) {
                this.icountMembers = this.$children.length;
            }
        },
        created: function () {
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Member/GetList"), {}, function (rs) {
                    self.members = rs.members;
                    self.icountMembers = self.members.length;
                    self.$emit("list-loaded");
                });
            },
            clickAddNewMember: function () {
                self.selectedMemberID = "";
                main.$.refMemberForm.AddNewMember();
                //console.log('from list, addnew2', cc.editMode, cc.Member, NewMember);
            },
            getOneMember: function (member) {
                var gid = member.GID;         //Pass to parent...
                var oid = member.OrderID;
                this.selectedMemberID = gid;
                this.selectedOrderID = oid;
                main.MemberID = gid;
                main.OrderID = oid;
                main.editMode = false;
                main.isOrders = !(oid === null);
                main.$.refOrder.editMode = false;
                //main.isDetail = (oid === null);
                main.tabs.orders.isShow = main.isOrders;
                //this.$parent.$.refMemberForm.memberId = gid;  //透過parent傳遞比較沒有接空參數的問題....
                //console.log(this.$parent.$.refMemberForm, gid, oid);
            },
        }
    });

    /******************************************************************************************************************************************/
    var NewMember =  { GID: "", CallName: "", Mate: "", TEL: ""
                , Email: "", Email2: "", LineID:""
                , Mobile: "", Mobile2: ""
                , Restarent1: "", Restarent2: "", Restarent3: ""
                , Address: "", Address2: ""
                , BirthDay: "", BirthDay2: ""
                , WeddingDate: "", EngageDate: "", InsertDate: "", UpdateTime: "", Note: "" };

    Vue.component("member-form", {
        template: "#member-form-template",
        inherit: true,
        data:{
            currTab:"",
        },
        props: {
            memberId: { type: String },
            editMode: { type: Boolean, default: false },
            onAddNewMember: Function,   //在HTML外面被轉為 on-add-new-member 
            Member: { twoWay: true }
        },
        created: function () {
            //console.log("memberForm created", this.memberId , this.Member);
            var self = this;
            this.$watch('memberId', function () {
                self.fetchData();
            });
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Member/GetOneMember"), { gid: self.memberId }, function (rs) {
                    self.Member = rs;
                    var f = self.Member;
                    
                    f.WeddingDate = ToJavaScriptDate(rs.WeddingDate);
                    f.EngageDate = ToJavaScriptDate(rs.EngageDate);
                    f.BirthDay = ToJavaScriptDate(rs.BirthDay);
                    f.BirthDay2 = ToJavaScriptDate(rs.BirthDay2);
                    /*f.InsertDate = rs.InsertDate;
                    f.UpdateTime = rs.UpdateTime;
                    f.Restarent1 = rs.Restarent1;
                    f.Restarent2 = rs.Restarent2;
                    f.Restarent3 = rs.Restarent3;
                    f.LineID = rs.LineID;*/
                    //console.log("memberForm fetchData", rs.EngageDate, rs.WeddingDate, self.Member.EngageDate);
                    //self.$emit('form-loaded'); //確保資料進來後才會display view.
                });
            },
            AddNewMember: function () {
                this.editMode = true;
                this.Member = { GID: "", CallName: "", Mate: "", TEL: ""
                , Email: "", Email2: "", LineID:""
                , Mobile: "", Mobile2: ""
                , Restarent1: "", Restarent2: "", Restarent3: ""
                , Address: "", Address2: ""
                , BirthDay: "", BirthDay2: ""
                , WeddingDate: "", EngageDate: "", InsertDate: "", UpdateTime: "", Note: "" };
            },
            clickEdit: function () {
                this.editMode = true;
            },
            clickSave: function (e) {
                e.preventDefault();
                var self = this;
                $.post(host("Member/SaveMember"), self.Member, function (rs) {
                    var list = self.$parent.$.refMemberlist;
                    list.fetchData();
                    list.selectedMemberID = rs;
                    self.memberId = rs;
                    self.editMode = false;
                });
            },
            clickCancel: function () {
                this.editMode = false;
            },
            clickRemove: function () {
                if (confirm("確定要移除這客戶?")) {
                    var self = this;
                    $.post(host("Member/RemoveMember"), { MemberID: self.memberId }, function (rs) {
                        var list = self.$parent.$.refMemberlist;
                        list.fetchData();
                        //console.log(rs);
                    });
                }
            },
            clickAddOrder: function () {
                var self = this;
                $.post(host("Order/AddNewOrder"), { id: self.memberId }, function (rs) {
                    //var list = self.$parent.$.refMemberlist;
                    //list.fetchData();
                    //console.log(rs);
                    location.href = host("Order/Design2/" + self.memberId);
                });
            },
           
        }

    });

    var NewOrderItemCost = {
        OrderID: "",
        OrderTitleTyp: "09C52536CC644D",
        Name: "",
        Cost: 0,
        Count: 1,
        Note: ""
    };

    Vue.component("order-contract", {
        template: "#order-contract-template",
        inherit: true,
        data: function(){ 
            return{
                OrderID:"",
                currTab: "",
                editMode: false,
                status:"read",
                OrderTitleTyps: [  //<--todo:這邊需要cache or redis.
                    {gid:"09C52536CC644D", title:"婚禮記錄"},
                    {gid:"A5F93EBE208F40", title:"新娘秘書"},
                    {gid: "F8F961C948B74D", title: "婚紗攝影" },
                    { gid: "AA904FB52B2C46", title: "贈品" },
                    { gid: "E80D9282AD1043", title: "禮服出借" }
                ],
                OrderSellPackage: null,
                isSellPackage:false,
                SelectedSellPackageID:"",
                OrderItemCost:{},
                OrderItemCosts: {},      //已選定的合約項目
                OrderItemCostPattern: {},  //所有合約項目下拉選單
                selectedPackages:[], //已選擇的套餐...
                CostSum: 0,
                currItemCost:"",
            }
        },
        props: {
            orderId: { type: String },
            memberId: { type: String },
        },
        created: function () {
            //console.log("orders created", this.OrderTitleTyps, this.currOrderTitleTyp, this.OrderItemCosts);
            var self = this;

            this.OrderItemCost = NewOrderItemCost;  //init new data...
            this.$watch('memberId', function () {
                self.fetchData();
            });
        },
        ready: function () {
            $(".ui.dropdown").dropdown();
            console.log("test");
            $("#mypopuptest").popup();
            $(".ui.service-flow-icon-popup").popup();

            //console.log("orders ready", this.memberId, this.OrderTitleTyps); //?
        },
        filters: {
            transferOrderItemTyp: function (v) {
                var es = this.OrderSellPackage, c, d;
                for (c in es) {
                    d = es[c];
                    if (d.GID === v) {
                        //console.log("filter", c, d);
                        return d.Name;
                    }
                }
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                //console.log("init get orderID:", self.orderId);
                $.post(host("Order/ShowListOrderItemCost"), { OrderID: self.orderId }, function (rs) {
                    self.OrderItemCosts = rs.items;
                    self.OrderSellPackage = rs.packages;
                    self.selectedPackages = rs.selectedPackages;
                    self.CostSum = 0;
                    for (var item in self.OrderItemCosts) {
                        var o = self.OrderItemCosts[item];
                        //console.log(o.Count, o.Cost);
                        self.CostSum += o.Cost * o.Count;
                    }
                    
                });
                //$.post(host("Member/OrderItemCostPattern"),{}, function(rs){   
                //    self.OrderItemCostPattern=rs;     
                //});
            },
            clickNewSellPackage: function () {
                this.editMode = this.isSellPackage = true;
            },
            clickNewItem: function () {
                this.status = "新增項目"
                this.editMode = true;
                this.isSellPackage = false;
                this.OrderItemCost = NewOrderItemCost;
            },
            ChoiceSellPackage:function(sellPackageID){
                this.SelectedSellPackageID = sellPackageID;
                console.log(main.OrderID,sellPackageID);
            },
            clickSaveSellPackage: function () {
                this.editMode = false;
                var self = this;
                self.OrderItemCost.OrderID = main.OrderID;

                $.post(host("Order/SaveOrderSellPackage"), { orderID: main.OrderID, SelectedSellPackageID: self.SelectedSellPackageID }, function (rs) {
                    var list = self.$parent.$.refOrder;
                    list.fetchData();
                });
            },
            clickSave: function (e) {
                e.preventDefault();
                this.editMode = false;
                var self = this;
                self.OrderItemCost.OrderID = main.OrderID;
                //console.log("save.", self.OrderItemCost);
                $.post(host("Order/SaveOrderItemCost"), self.OrderItemCost, function (rs) {
                    var list = self.$parent.$.refOrder;
                    list.fetchData();
                });
            },
            clickEdit: function (OrderItemCost) {
                var i = OrderItemCost;
                this.editMode = true;
                this.isSellPackage = false;
                this.currItemCost = i.GID;
                this.OrderItemCost = i;  //這邊套入的很輕鬆!
            },
            clickRemove: function (GID) {
                var self = this;
                if (confirm("確定要刪除這筆資料?")) {
                    $.post(host("Order/RemoveOrderItemCost"), { OrderItemCostGID: GID }, function (rs) {
                        var list = self.$parent.$.refOrder;
                        list.fetchData();
                    });
                }
            },
            clickCancel: function () {
                this.editMode = false;
                this.OrderItemCost = {};
            },
            clickEditSellPackageList:function(){
                location.href=host("Order/ShowEditSellPackageList");
            },
            clickRemoveSelectedPackage: function (e) {
                if (confirm("確定刪除 " + e.PackageName + " 套餐所有明細?")) {
                    var self = this;
                    $.post(host("Order/RemoveSelectedPackage"), { OrderID: self.orderId, SelectedPackageID: e.PackageID }, function () {
                        var list = self.$parent.$.refOrder;
                        list.fetchData();
                    });
                }
            }
        }

    });

    var main = new Vue({
        el: "#main",
        data: {
            editMode: false,
            MemberID: "",
            OrderID: "",
            currTab: "detail",
            tabs: {
                detail: { n: "客戶明細", b: "active", icon:"user" },
                orders: { n: "訂單-產品-服務", b: "" , icon:"file" },
            },
            isDetail: true,
            isOrders: false,
        },
        created: function () {
            //console.log("from main created,", this.currTab);
        },
        filters: {
            formatDate: function (v) { if(v!=null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            accounting: function (v) { return accounting.formatMoney(v); }
        },
        methods: {
            clickTab: function (e) {
                var raw = e.targetVM._raw;
                for (var c in this.tabs) { this.tabs[c].b = ""; }
                raw.b = "active";
                this.currTab = e.targetVM.$key;
                this.isDetail = (this.currTab === "detail");
                this.isOrders = (this.currTab === "orders");
                
                //this.editMode = false;
                //console.log(raw, this.currTab);
            }
            // Passing callback as Props，將整個refMemberForm 
            //onTriggerNewMember: function (cmf) { //<----cmf 變數才是重點!!透過callback穿到後面去抓參數
            //    this.MemberID = "4fabfa0e336741";
            //    this.editMode = true;
            //    console.log(cmf);
            //    cmf.Member = NewMember;
            //    /* 這樣一開始就抓不到，refMemberForm根本還沒生成。
            //    //this.$.refMemberForm.Member = NewMember; 
            //    //console.log('from list, addnew', child);
            //}
        }
    });

   


});
