﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "/Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "semantic":["semantic-ui/dist/semantic.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "sortable": ["Sortable/Sortable.min"],
        "moment": ["moment/min/moment.min"],
        "locale": ["moment/min/locales.min"],
        "zh-tw": ["moment/locale/zh-tw"]
    },
    shim: {
        //"moment": {
        //    deps: ["zh-tw", "locale","moment"],
        //    exports: "moment"
        //},
        "semantic": {
            deps: ["jquery"],
            exports:"semantic"
        },
        "datepicker":["jquery"]
    }
});

//define(id?, dependencies?, factory);
define(["jquery", "vue", "validator","sortable","moment", "semantic", "datepicker"]
, function (jQuery, Vue, validator, Sortable, moment) {

    //var $=jQuery;
    $(document).bind("ajaxSend", function () {
        $("#loading").show();
    }).bind("ajaxComplete", function () {
        $("#loading").hide();
    });

    $('.ui.sticky').sticky({ context: '#example1' });

    var CustomerID;
    CustomerID = $("#main").attr("data-user");

    Vue.use(validator);
    Vue.directive('datepicker', {
        twoWay: true,
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    //v-checkbox
    Vue.directive("checkbox", {
        twoWay: true,
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            var el = $(this.el);
            el.checkbox({
                onChecked: function () {
                    el.checkbox("set checked");//var gid = $(this).parent().find("label")[0].attributes["data-item-gid"];
                    vm.$set(key, "Y");
                    //console.log("onChanged", "Y");
                },
                onUnchecked: function () {
                    el.checkbox("set","unchecked");
                    vm.$set(key, "N");
                    //console.log("onUnchanged", "N");
                },
            });
        },
        update: function (val) {
            $(this.el).checkbox((val === "Y") ? "check" : "uncheck");
        }
    });
   

    //-- Component從這邊開始-----------------------------------------------------------------

    //Flow狀態表
    Vue.component("service-list", {
        template: "#serviceList-template",
        inherit: true,
        replace: true,
        props: {
            flowItems: { type: Array }
        }
    });

    
    //明細那一層
    Vue.component("item-list", {
        template: "#item-template",
        inherit: true,
        replace: true,
        props:{
            itemsList: { type: Array },
            isItemEditable: { type: Boolean }
        },
        data: function () {
            return {
                selectedItemID: "",
                isSelectedID: false,
                customverSrvStartDate: "",
                RemindDateTime: "",
            }
        },
        //watch: {
        //    "itemsList": function (val, oldVal) {
        //        console.log(val, oldVal);
        //    }
        //},
        created:function() {
          //console.log("item created", this.itemsList);
        },
        methods: {
            clickCheckStatus: function (e) {
                var chk = (e.IsCheck == "Y") ? "N" : "Y";
                //console.log(e.GID, chk);
                $.post(host("Order/SetCheckboxValue"), { ItemGID: e.GID, v: chk }, function (rs) {
                    main.fetchData();
                });
            },
            clickOpenEditItem: function (e) {
                this.selectedItemID = e.GID;
                this.isSelectedID = !this.isSelectedID;
                console.log(this.isSelectedID);
                this.toggleEditItem();
                this.customverSrvStartDate = ToJavaScriptDate(e.StartDateTime);
                this.RemindDateTime = ToJavaScriptDate(e.RemindDateTime);
            },
            toggleEditItem: function () {
                if (this.isSelectedID == false) { this.selectedItemID = ""; }
            },
            clickCancelEditItem: function () {
                this.isSelectedID = !this.isSelectedID;
                this.selectedItemID = "";
            },
            clickSaveItemDetails: function (e) {
                console.log(e, this.customverSrvStartDate, this.RemindDateTime);
                var self = this;
                $.post(host("Order/SaveOrderItemDetails"), {
                    GID: e.GID,
                    Note: e.Note,
                    StartDateTime: self.customverSrvStartDate,
                    RemindDateTime: self.RemindDateTime,
                }, function (rs) {
                    main.fetchData();
                });
            },
            clickStarIcon: function (GID) { //這個做為mark使用，單純標記
                $.post(host("Order/clickStarIcon"), { OrderItemGID: GID }, function (rs) {
                    main.fetchData();
                });
            },
           
            clickSaveOpenGroup: function (item) {
                console.log(item);
            }
        }
    });
   
    
    moment.locale("zh-tw");
    
    /* Dress list component. */
    var main=new Vue({
        el:"#main",
        //inherit: true,
        data: function () {
            return {
                CustomerID: $("#main").attr("data-user"),
                CurrServiceID: $("#main").attr("data-serviceid"),
                CurrServiceName:"",
                member: {},
                ServiceStatus: {},
                OrderFlows: {},
                flow: {},
                EditItemID: "",
                IsOrderGroupEdit: false,

                NewFlowName: "",
                NewShortFlowName: "",
                IsSaveAsTemplateFlow: false,
                selectedOrderGroup: "",
                NewOrderItemDscr: "",
                IsEditOrderItem: false,
                IsEditOrderGroup: false,
                //SortableEnabled: false
            }
        },
        created: function () {
            this.fetchData();
        },
        ready: function () {

        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            moment: function (v) { return moment(v).fromNow(); }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("Order/GetOrderData"), { CustomerID: self.CustomerID, ServiceID: self.CurrServiceID }, function (rs) {
                    self.member = rs.member;
                    self.ServiceStatus = rs.ServiceStatus;  //原details
                    self.OrderFlows = rs.OrderFlows;        //新加入
                    $("#main").fadeIn(200);
                });
            },
            clickFindService: function (ServiceID, ServiceName) {
                console.log(this.CustomerID, ServiceID);
                this.IsEditOrderItem = false;
                this.CurrServiceID = ServiceID;
                this.CurrServiceName = ServiceName;
                this.fetchData();
            },
            clickOpenEditOrderGroup: function (GroupGID) {
                this.selectedOrderGroup = GroupGID;
                this.IsEditOrderItem = true;

                var el = document.getElementById(GroupGID);
                var sortableItems = new Sortable(el, {
                    animation: 350,
                    handle: ".my-handle.item",
                    onEnd: function (e) {
                        var m = main;
                        console.log(e.oldIndex, e.newIndex, GroupGID, m.CustomerID, m.CurrServiceID);
                        $.post(host("Order/SaveSortOrderItem"), {
                            oldIndex: e.oldIndex,
                            newIndex: e.newIndex,
                            ServiceID: m.CurrServiceID,
                            GroupName: GroupGID,
                        }, function () {
                            //m.fetchData();
                        });
                    }
                });
            },
            clickCloseOrderGroup: function () {
                this.selectedOrderGroup = "";
                this.IsEditOrderItem = false;
            },
            clickSaveOrderGroup: function (OrderGroup) {
                console.log(this.selectedOrderGroup, OrderGroup);
                var self = this;
                $.post(host("Order/ChangeOrderGroupName"), { OrderGroup: OrderGroup }, function () {
                    self.fetchData();
                    this.selectedOrderGroup = "";
                });
            },

            clickOrderGroupCreateNew: function () {
                var self = this;
                var currService = self.ServiceStatus.filter(function (v) { return (v.ServID === self.CurrServiceID); })[0];
                var ServName = currService.ServName; //這段真的太強大了!!
                //console.log(self.ServiceStatus, self.CurrServiceID, ServName);
                self.CurrServiceName = ServName;

                $('.ui.modal').modal('show');
            },
            clickOrderGroupEdit: function () {
                this.IsEditOrderGroup = true;

                var el = document.getElementById("OrderFlowContent");
                var sortable = new Sortable(el, {
                    animation: 350,
                    handle: ".my-handle",
                    // dragging ended
                    onEnd: function (e) {
                        var m = main;
                        console.log(e.oldIndex, e.newIndex, m.CustomerID, m.CurrServiceID);
                        $.post(host("Order/SaveSortOrderGroup"), {
                            oldIndex: e.oldIndex, newIndex: e.newIndex,
                            CustomerID: m.CustomerID, ServiceID: m.CurrServiceID
                        }, function () {
                            m.fetchData();
                        });
                    }
                });
            },
            clickOrderGroupEditCancel: function () {
                this.IsEditOrderGroup = false;
                this.fetchData();
            },
           
            clickSaveAddNewOrderGroup: function () {
                var self=this;
                var NewOrderGroup = {
                    CustomerID: self.CustomerID,
                    ServiceID: self.CurrServiceID,
                    Flow: this.NewFlowName,
                    ShortFlowName: this.NewShortFlowName,
                };
                console.log(NewOrderGroup);
                $.post(host("Order/SaveOrderGroup"), { OrderGroup: NewOrderGroup }, function (rs) {
                    self.fetchData();
                    $('.ui.modal').modal('hide');
                });
            },
            clickCancelAddNewOrderGroup: function () {
                $('.ui.modal').modal('hide');
            },
            clickSaveNewOrderItem: function (FlowGID) {
                
                var self=this;
                var NewOrderItem = {
                    ServiceID: self.CurrServiceID,
                    GroupName: FlowGID,
                    dscr: self.NewOrderItemDscr
                };
                console.log(NewOrderItem, self.CustomerID);
                $.post(host("Order/SaveNewOrderItem"), { OrderItem: NewOrderItem, CustomerID: self.CustomerID }, function (rs) {
                    self.fetchData();
                    self.NewOrderItemDscr = ""; //清空
                    //$('.ui.modal').modal('hide');
                });
            },
            BlurSaveItemDscr: function (item) {
                var self = this;
                //var currFlow = self.OrderFlows.filter(function (v) { return (v.GID === self.selectedOrderGroup); })[0];
                //var currItemDscr = currFlow.Items.filter(function (v) { return (v.GID === item.GID); })[0].Dscr;
                //console.log(item.GID, item.Dscr, currItemDscr);
                $.post(host("Order/SaveEditOrderItem"), { itemGID: item.GID, itemDscr: item.Dscr }, function (rs) {

                });
            },
            deleteOrderItem: function (item) {
                var self = this;
                $.post(host("Order/deleteOrderItem"), { itemGID: item.GID }, function (rs) {
                    var currFlowItems = self.OrderFlows.filter(function (v) { return (v.GID === self.selectedOrderGroup); })[0].Items;
                    currFlowItems.$remove(item); //這段真的太強大了!!
                });
            },
            clickGoToPattern: function () {
                location.href = host("order/editOrderPattern/") + this.CurrServiceID;
            }
        }
    });






});
