﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        'validator': ['validator/validator'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.min"],
        "fullcalendar": ["fullcalendar/dist/fullcalendar.min"],
        "moment":["moment/min/moment.min"]
    },
    shim: {
        "semantic": { deps: ["jquery"], exports: "semantic" },
        "datepicker": ["jquery"]
    }
});

define(["vue", "jquery", "fullcalendar", "moment"], function (Vue, $, fullCalendar) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

    var main = new Vue({
        el: "#main",
        data: {
            Employees: {},
            isFastCopyHide: true,
            currWkTag:"",
            nextWkTag: "",
            currHeadDate: "", //月曆Title上的日期，(YYYY/MM/DD)
            msgBar: "",

            tmplateList: {},
            currWk:"",
            currStartDate:"",
            currTmplate: "",
            isHideSaveBtn :true
        },
        created: function () {
            this.fetchData();


            var currStartDate = $("#main").attr("startDate");
            var tmpCode = $("#main").attr("templateCode");
            self.currTmplate = tmpCode;

            //initialize fullCanlendar.
            $("#calendar").fullCalendar({
                header: {
                    left: 'SaveAsPatternBtn',
                    center: "title",
                    right: 'agendaFourDay,agendaWeek'
                },
                titleFormat: "YYYY MM",
                timeFormat: "HH(:mm)",
                dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
                defaultDate: currStartDate,
                defaultView: 'agendaWeek',
                views: {
                    agendaFourDay: {
                        type: "agenda",
                        titleFormat: "YYYY/MM/DD",
                        duration: { days: 4 },
                        buttonText: "4 day"
                    },
                    agendaWeek: {
                        titleFormat: "YYYY/MM/DD",
                        buttonText: "一週六天"
                    },
                },
                editable: false,
                selectable: false,
                weekNumbers: true,
                hiddenDays: [0],
                slotDuration: "01:00:00",
                allDaySlot: false,
                slotEventOverlap: true,
                minTime: "09:00:00",
                maxTime: "24:00:00",
                eventLimit: false, // allow "more" link when too many events
                events: {
                    url: "/shift/GetTmpShiftData?Pattern=" + tmpCode,
                    error: function (r) {
                        console.log(r);
                    },
                    success: function (s) {
                        //console.log("success", s);
                    }

                },
                eventRender: function (e, element, view) {
                    //console.log(e.title);
                    var user = e.title;
                    element.append("<div class='ev-panel' data-evid='" + "'><a class='ev-user' href='#' target='_blank'>" + user + "</a></div>");
                },

                eventAfterAllRender: function (view) {
                    //self.setIsFastCopyHide();
                },
                eventOrder: "title",
                eventLimit: 10
            });

        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            //accounting: function (v) { return accounting.formatMoney(v); }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post(host("/shift/GetDropdownListTmplateData"), {}, function (rs) {
                    self.tmplateList = rs;
                    self.currTmplate = $("#main").attr("templateCode");
                    self.currWk = $("#main").attr("currWk");
                    self.isHideSaveBtn = (self.currWk == "prv");
                    console.log(self.currWk, self.isHideSaveBtn);
                });
            },
            SelectTmplateCode: function () {
                location.href = "/Shift/ChooseTmplateWeek?TmplateCode=" + this.currTmplate + "&currWk=" + $("#main").attr("currWk");
            },
            CopyPatternToCalendar: function () {
                var self = this;
                $.post(host("/shift/CopyPatternToCalendar"), { currWkTag: self.currWk, tmpCode: self.currTmplate }, function (rs) {
                    if (rs == "Success") {
                        self.msgBar = "COPY到" + self.currWk + " 成功，請回到班表確認。";
                    }
                });
            }
        }
    });



});
