﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['vue/dist/vue.min'],
        'validator': ['validator/validator'],
        "core": ["jqueryui/ui/core"],
        "datepicker": ["jqueryui/ui/datepicker"]
    }
});

//define(id?, dependencies?, factory);
define(["vue", "jquery", "datepicker"], function (Vue, $) {
    //Vue.debug=true;
    Vue.directive('datepicker', {
        bind: function () {
            var vm = this.vm;
            var key = this.expression;
            $(this.el).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function (date) {
                    vm.$set(key, date);
                }
            });
        },
        update: function (val) {
            $(this.el).datepicker('setDate', val);
        }
    });

    var main = new Vue({
        el: "#main",
        data: {
            CustomerID:"",
            CustomerName: "",
            RsrvTypButton: null,
            RsrvData: null,
            isOK:false
        },
        filters: {
            formatDate: function (v) { return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        },
        created: function () {
            this.fetchData();
        },
        methods: {
            fetchData: function () {
                var self = this;
                self.CustomerID = document.getElementById("custID").value;
                
                $.post("/TimeLine/GetTimeLineData", { CustID: self.CustomerID }, function (rs) {
                    self.CustomerName = rs.CustomerName;
                    self.RsrvTypButton = rs.RsrvTypButton;
                    self.RsrvData = rs.RservData;
                    self.isOK = true;
                    //console.log("post,", self.RsrvData);
                    //self.$.refTimeLine.fetchData();
                    //self.$.refTimeLine.RservData = rs.RservData;  //這邊非同步 component會接收不到!
                    //console.log(self.$.refTimeLine.RservData);
                    //self.$emit("treenode-loaded");
                });
            },
            addRserv: function (e) {
                var RsID = e.targetVM._raw.GID;
                location.href = "/calendar/AddRsrvDate?r="+RsID+"&c="+this.CustomerID;
            }
        }
    });
  
});

