﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting, showdown) {
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

   

    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            codeName: document.getElementById("main").getAttribute("data-content-codename"),
            user: document.getElementById("main").getAttribute("data-user"),
            imgs: [],
            store: {},
            isFavoriteStore: 0,

            dm1: {},
            dm2: {},
            dm3: {},
        },
        created: function () {
            this.fetchData();
            
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post("/getdata/FindRightDM", { codeName: this.codeName }, function (rs) {
                    self.store = rs.store;
                    self.imgs = rs.imgs;
                    self.isFavoriteStore = rs.isFavoriteStore;
                    self.dm1 = rs.items[0];
                });
            },
        
        }
    });

});