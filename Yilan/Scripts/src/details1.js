﻿define(["vue", "accounting", "showdown", "moment", "semantic"], function (Vue, accounting, showdown) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var converter = new showdown.Converter();
    var aan = document.getElementById("main").getAttribute("data-aan");
    var uid = $("#UserID").html();

    function adminurl(v) {
        return "/" + aan + "/" + v;
    }

    var main = new Vue({
        el: "body",
        data: {
            currContentGuid: "",
            currContentSN: 0,
            currItemGuid: "",
            aan: "",
            RoutePath: "",

            Content:{},
            item: {},
            myCartCC: 0
        },
        created: function () {
            this.currContentSN = document.getElementById("main").getAttribute("data-content-sn");
            this.currContentGuid = document.getElementById("main").getAttribute("data-content-guid");
            this.currItemGuid = document.getElementById("main").getAttribute("data-item-guid");
            this.fetchItems();
            
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            formatMDE: function (v) { return converter.makeHtml(v); },
            formatDscr: function (v) { return (v == null) ? "" : v.substring(1, 100) + "..."; },
            formatImgSrc: function(v, uploadURL,width,height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
            formatImgSrcFirst: function(arr, uploadURL,width,height) 
            {
                console.log("arr", arr)
                if (arr == undefined){ return "" }
                if (arr.length > 0) {
                    return uploadURL + "/" + arr[0].FileNameNew + "?w=" + width + "&h=" + height + "&mode=crop";
                }
            }
        },
        computed: {
            TransRoutePath: function () {
                var arr = this.RoutePath.split("|"), ss = "", key, val, ar = [], brr =[];
                for (var c in arr) {
                    if (arr[c]) {
                        ar = arr[c].split(":");
                        brr.push({ val : ar[0], key: ar[1] });
                    }
                }
                return brr;
            }
        },
        methods: {
            fetchItems: function () {
                var self = this;
                var url = "/getdata/GetSingleItem";

                $.post(url, { ItemGUID: this.currItemGuid, uid: uid }, function (rs) {
                    self.myCartCC = rs.Parent.myCartCC;
                    self.Content = rs.Parent;
                    self.RoutePath = rs.RoutePath;
                    self.item = rs.Item
                });
            },
            splitSubTitleArr: function (v) {
                console.log(v, v.split("X"));
                return v.split("X");
            }
        }
        //=======

    });

});