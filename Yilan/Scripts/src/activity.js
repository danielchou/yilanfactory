﻿define(["vue", "accounting", "showdown", "moment", "semantic"], function (Vue, accounting, showdown) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var converter = new showdown.Converter();

    var main = new Vue({
        el: "body",
        data: {
            Images: [],
            BigTitle: "活動專區",
            Contents: {},
            myCartCC: 0
        },
        created: function () {
            this.currContentSN = document.getElementById("main").getAttribute("data-content-sn");
            this.currContentGuid = document.getElementById("main").getAttribute("data-content-guid");
            this.fetchItems();
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            formatMDE: function (v) { return converter.makeHtml(v); },
            formatDscr: function (v) { return (v == null) ? "" : v.substring(0, 100) + "..."; },
            formatImgSrc: function (v, uploadURL, width, height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
            formatImgSrcFirst: function (arr, uploadURL, width, height) {
                if (arr != undefined) {
                    return uploadURL + "/" + arr[0].FileNameNew + "?w=" + width + "&h=" + height + "&mode=crop";
                }
            },
        },
        methods: {
            fetchItems: function () {
                var self = this;
                $.post("/h/GetActivity", {}, function (rs) {
                    self.Contents = rs;
                    $.post("/GetData/ShowMemberCartCC", {}, function (rs) {
                        //console.log(rs);
                        self.myCartCC = rs.myCartCC
                    });
                });
            }
           
        }
    });

});