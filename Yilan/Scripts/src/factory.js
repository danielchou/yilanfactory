﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting, showdown) {
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            mainImgHeight: 500,
            spot2s: ["B","C","D","E","F","G","H"],
            spots: [],
            currStore: "",
            currStoreTyp: "",
            currSpots: []
        },
        created: function () {
            this.initSpots();
            var self = this;
            window.addEventListener("resize", function () {
                self.initSpots();
            });
        },
        watched: {
        },
        filters: {
          
        },
        computed: {
            
        },
        methods: {
            initSpots: function () {
                var h = document.getElementById("maining").offsetHeight;
                //console.log("resize", h);
                this.mainImgHeight = h;
                var self = this;
                this.spots = [
                    { id: "s1", y: 14, x: 515, code:"tigerfood" },
                    { id: "s2", y: 57, x: 585, code:"rabbit1" },
                    { id: "s3", y: 62, x: 565, code:"rden" },
                ];

                $.post("/getdata/ReadStoresMapXY", {}, function (rs) {
                    self.spots = rs;
                });
            },
            clickGetCurrStoreTyp: function (s) {
                this.currStoreTyp = s;
                this.currSpots = this.spots.filter(x => x.typ == s);
                console.log("self.spots:", this.spots, this.currSpots);
            },
            setStoreTop: function(v) {
                return this.mainImgHeight * v / 100 + 'px';
            },
            setStoreIconTop: function(v) {
                return (this.mainImgHeight * v / 100) - 65 + 'px';
            },
            showFactoryIcon: function(v) {
                return "../Content/Images/icons/StoreTyp" + v + ".png";
            },
            mouseOverStore: function (s) {
                //https://segmentfault.com/q/1010000005663366 mouseOver有更漂亮的寫法
                this.currStore = s.id;
            },
            mouseLeaveStore: function () {
                this.currStore = "";
            },
            clickFactoryInfo: function (code) {
                location.href = "../factoryinfo/" + code;
            }
        }
    });

});