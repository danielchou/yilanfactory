﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    

    var main = new Vue({
        el: "body",
        data: {
            myCartCC:0,
            cartFd: ["#","img","品名","單價","數量","金額",""],
            carts: {},
            totalCC: 0,
            totalFee: 0
        },
        created: function () {
            this.fetchData();
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            formatImgSrc: function(v, uploadURL,width,height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
        },
        computed: {
            totalFee: function () {
                var sum = 0, p;
                var carts = this.carts;
                for (var c in carts) {
                    p = carts[c].prod; console.log(p);
                    sum += carts[c].count * p.Cost;
                }
                return sum;
            },
            totalCC: function () {
                return this.carts.length;
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post("/member/GetCartList", {}, function (rs) {
                    console.log(rs.cart)
                    self.myCartCC = rs.myCartCC;
                    self.carts = rs.cart;
                });
            },
        }
    });

});