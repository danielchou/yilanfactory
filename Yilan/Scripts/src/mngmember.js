﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var aan = document.getElementById("main").getAttribute("data-aan");

    var main = new Vue({
        el: "#main",
        data: {
            members: {}
        },
        created: function () {
            this.fetchData();
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        },
        methods: {
            fetchData: function () {
                var self = this;

                $.post("/" + aan + "/ShowMembers", {}, function (rs) {
                    console.log(rs)
                    self.members = rs;
                });
            } 
        }
    });

});