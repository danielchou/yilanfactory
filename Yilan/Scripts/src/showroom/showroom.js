﻿var _WRS_Store_ = $("#wrsHiddenSeed").attr("data-store");
var _WRS_URL_ = $("#wrsHiddenSeed").attr("data-hurl");
/*console.log(_WRS_Store_, _WRS_URL_);*/
Vue.component("color-selector", {
    template: "#Color-Selector-tmeplate",
    data: function () {
        return {
            wrsurl: _WRS_URL_,
            Store: _WRS_Store_,
            colors: null,
            typs: null,
            selectedColor:"",
            selectedTyp: "",
            msg: ""
        }
    },
    created: function () {
        this.fetchData();
        this.$watch('selectedColor+selectedTyp', function () {
            this.fetchData();
            this.msg = this.selectedColor + "," + this.selectedTyp;
        });
    },
    methods: {
        fetchData: function () {
            var self = this;
            $.post(self.wrsurl + "/showroom/GetColors", { store: self.Store }, function (rs) {
                self.colors = rs.colors;
                self.typs = rs.typs;
                self.colors.push({ SN: 0, Name: "ALL", Dscr: "不區分顏色" });
                self.typs.push({ SN: 0, Name: "不區分款式", Dscr: "" });
            });
        },
        onClickColor: function (e) {
            var raw = e.targetVM._raw, sn = raw.SN;
            this.selectedColor = sn;
            /*console.log("Color", sn);*/
            this.$parent.$.refDressList.selectedColor = sn;
        },
        onClickTyp: function (e) {
            var raw = e.targetVM._raw, sn = raw.SN;
            this.selectedTyp = sn;
            /*console.log("Typ", sn);*/
            this.$parent.$.refDressList.selectedTyp = sn;
        }
    }
});

Vue.component("showroom-body", {
    template: "#Showroom-Dress-tmeplate",
    data: function () {
        return {
            wrsurl: _WRS_URL_,
            Store: _WRS_Store_,
            USKey: "",
            dresses: null,
            selectedColor: 20, /*這邊要挖掉!! 代表初始顏色*/
            selectedTyp:0,
            icountDress: 0,
            carts: {},
            icarts: 0,
            msg: ""
        }
    },
    created: function () {
        this.GetUniqueSessionID();
        this.fetchData();
        this.ShowCartList();
        this.$watch('selectedColor+selectedTyp', function () {
            this.fetchData();
            this.msg = this.selectedColor + this.selectedTyp;
            this.icountDress = this.dresses.length;
        });
    },
    methods: {
        fetchData: function () {
            var self = this;
            $.post(self.wrsurl + "/showroom/GetAllDressData", { store: self.Store, colorid: self.selectedColor, typid: self.selectedTyp }, function (rs) {
                self.dresses = rs;
                self.icountDress = self.dresses.length;
            });
        },
        GetUniqueSessionID:function() {
            var key = newUniqueID(15);
            if (window.localStorage.getItem("USKey") == undefined) {
                window.localStorage["USKey"] = key;
            } else {
                if (window.localStorage["USKey"] === null) window.localStorage["USKey"] = key;
            }
            this.USKey = window.localStorage["USKey"];
        },
        showBigImg: function (e) {
            var raw = e.targetVM._raw;
            var code = raw.Code;
            window.open(this.wrsurl+"/"+this.Store+"/showroom/dressdetail2/" + code);
        },
        onclickShare: function (e) {
            var raw = e.targetVM._raw;
            var name = raw.Name;
            /*console.log("share", name);*/
        },
        dislikeDress: function (e) {
            var r = e.targetVM._raw,
                gid = r.gid,
                name = r.name;
            /*console.log("dislike", gid, name);*/
            delete this.carts[gid];
            window.localStorage[this.USKey + ".cart"] = JSON.stringify(this.carts);
            this.ShowCartList();
        },
        /* --------------- cart ----------------*/
        onclickLike: function (e) {
            var r = e.targetVM._raw,
                gid = r.GUID,
                name = r.Name,
                file = r.Img.FileName;
            /*console.log(this.carts, gid);*/
            this.carts[gid] = { gid: gid, name: name, file: file };
            window.localStorage[this.USKey + ".cart"] = JSON.stringify(this.carts);
            this.ShowCartList();
        },
        ShowCartList: function (e) {
            var self = this;
            if (window.localStorage[this.USKey + ".cart"] != undefined) {
                var cart_str = window.localStorage[this.USKey + ".cart"];
                self.carts = JSON.parse(cart_str);
                self.icarts = Object.keys(self.carts).length;
                /*console.log("show", cart_str, self.carts);*/
            }
        },

    }
});

var main = new Vue({
    el: "#WressSRMX",
    data: {}
});


function newUniqueID(iLength, UserName) {
    var id = "";
    for (var i = 1; i <= iLength; i++) {
        id += Math.floor(Math.random() * 32.0).toString(32);
    }
    return id;
}