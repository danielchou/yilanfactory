﻿
function loadScript(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) { //IE  
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others  
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function loadjscssfile(filename, filetype, id) {
    if (filetype == "js") { //if filename is a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype == "css") { //if filename is an external CSS file
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
    }
    else if (filetype == "xjs") { //if filename is an external CSS file
        var fileref = document.createElement('script');
        fileref.setAttribute("type", "text/x-template");
        fileref.setAttribute("src", filename);
        fileref.setAttribute("id", id);
    }
    if (typeof fileref != "undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}
var whirurl = "http://wressly.com";
var whirurl2 = "https://localhost:44300";
loadjscssfile("http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css","css","");
loadjscssfile(whirurl + "/content/showroom.css", "css", "");

loadScript(whirurl + "/bower_components/jquery/dist/jquery.min.js", function () {
    loadScript(whirurl + "/bower_components/vue/dist/vue.min.js", function () {
        $("#WressSRMX").before("{{replaceMe}}").html("<color-selector v-ref='refColors'></color-selector><showroom-body v-ref='refDressList'></showroom-body>");
        loadScript(whirurl+"/Scripts/src/showroom/min1.js?v=250819-000", function(){
            
        });
    });
});