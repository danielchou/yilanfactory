﻿define(["vue", "accounting", "showdown", "moment", "semantic"], function (Vue, accounting, showdown) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    //Ref: https://github.com/NextStepWebs/simplemde-markdown-editor
    //var simplemde = new SimpleMDE({ element: document.getElementById("MyID") });
    var converter = new showdown.Converter();

    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            Images: [],
            Contents: {},
            myCartCC: 0, 
        },
        created: function () {
            this.currContentSN = document.getElementById("main").getAttribute("data-content-sn");
            this.currContentGuid = document.getElementById("main").getAttribute("data-content-guid");
            this.fetchItems();
        },
        filters: {},
        methods: {
            fetchItems: function () {
                var self = this;
                $.post("/getdata/GetItems", { contentSN: 2 }, function (rs) {
                    //console.log(rs.Items[0].imgs);
                    self.Images = rs.Items[0].imgs;                    
                });
            },
            goToLink: function (i) {
                var links = ["tourismFactory", "marketing", "plantrip"];
                location.href = "/" + links[i];
            }
           
        }
    });

});