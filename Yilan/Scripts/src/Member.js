﻿define(["vue", "accounting", "showdown", "moment", "semantic"], function (Vue, accounting, showdown) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var converter = new showdown.Converter();
    var aan = document.getElementById("main").getAttribute("data-aan");
    var uid = $("#UserID").html();

    function adminurl(v) {
        return "/" + aan + "/" + v;
    }

    var main = new Vue({
        el: "body",
        data: {
            currContentSN:0,
            aan: "",

            myCartCC:0,
            currRouteName: "preschool",
            currContentGuid: "",
            ContentSplitBody: {},
            Images: [],
            SN: 0,

            currBlockId: 3,
            menus:[
                {id:1,Name:"會員權益"},
                {id:2,Name:"追蹤的訂單"},
                {id:3,Name:"喜歡的商品"},
                {id:4,Name:"個人資料"},
                {id:5,Name:"退換貨流程"}
            ],
            Favors: [],
            returnFlow: "",
            memberRight: "",

        },
        created: function () {
            this.currContentSN = document.getElementById("main").getAttribute("data-content-sn");
            this.currContentGuid = document.getElementById("main").getAttribute("data-content-guid");
            this.fetchItems();
            
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            formatMDE: function (v) { return converter.makeHtml(v); },
            formatDscr: function (v) { return (v == null) ? "" : v.substring(1, 100) + "..."; },
            formatImgSrc: function(v, uploadURL,width,height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
            formatImgSrcFirst: function(arr, uploadURL,width,height) 
            {
                if (arr.length > 0 ) {
                    return uploadURL + "/" + arr[0].FileNameNew + "?w=" + width + "&h=" + height + "&mode=crop";
                }
            },
            splitSubTitle: function (v) {
                return v.split("X");
            }
        },
        computed: {
            TransRoutePath: function () {  
                var arr = this.RoutePath.split("|"), ss = "", key, val, ar = [], brr =[];
                for (var c in arr) {
                    if (arr[c]) {
                        ar = arr[c].split(":");
                        brr.push({ val : ar[0], key: ar[1] });
                    }
                }
                return brr;
            }
        },
        methods: {
            fetchItems: function () {
                var self = this;
                url = "/getdata/getMemberPrivateData";

                $.post(url, { ContentSN: this.currContentSN, uid: uid }, function (rs) {
                    self.Favors = rs.favors;
                    var rts = rs.Rights;
                    self.memberRight = rts.filter(function (v) { return v.SN==65 })[0].Body;
                    self.returnFlow = rts.filter(function (v) { return v.SN == 74 })[0].Body;
                });
            },
            clickSelectBlock: function (id) {
                this.currBlockId = id;
            }

           
        }
    });

});