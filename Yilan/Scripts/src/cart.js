﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var aan = document.getElementById("main").getAttribute("data-aan");
    var hosturl = document.getElementById("host").getAttribute("data-hosturl");

    var main = new Vue({
        el: "body",
        data: {
            myCartCC:0,
            cartFd: ["#","img","品名","單價","數量","金額",""],
            carts: {},
            totalCC: 0,
            totalFee: 0,
            op: {
                stCode:"",
                order_status: "A01",
                rv_name: "",
                rv_mobile: "",
                rv_email: "",
                rv_addr: "",
                rv_zip: "",

                suid:"service@studentweekly.com.tw",
                processID:"A0001", 
                webPara: this.rv_name + "|" + this.rv_mobile + "|" + this.rv_email
            },
            invalid: {
                errName: false,
                errMobile: false,
                errEmail: false,
                errZip: false,
                errAddr: false,
                errStore: false
            },
            discountCode: "",
            redirectURL: hosturl + "/pay/GetMapInfor",
            warning: "",
            pageType: "",
            isOver8000: false,
        },
        created: function () {
            this.pageType = document.getElementById("cSMIF").getAttribute("data-pageType");

            this.fetchData();
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
            formatImgSrc: function(v, uploadURL,width,height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
        },
        computed: {
            totalFee: function () {
                var sum = 0, p;
                this.isOver8000 = false;
                var carts = this.carts;

                for (var c in carts) {
                    p = carts[c].prod; console.log(p);
                    sum += carts[c].count * p.Cost;
                }

                if (sum > 8000) {
                    this.isOver8000 = true;
                    this.warning = "超過代收最高8000元的限制";
                } else {
                    this.isOver8000 = false;
                    this.warning = "";
                }
                return sum;
            },
            totalCC: function () {
                return this.carts.length;
            },
            isRVAddr: function() { 
                return this.op.order_status=="A05";
            },
            isConfirmMapBtn: function () {
                    var i = 0;
                    var op = this.op;
                    if (op.rv_name == "") { i += 1; this.invalid.errName = true; } else { this.invalid.errName = false; }
                    if (op.rv_mobile == "") { i += 1; this.invalid.errMobile = true; } else { this.invalid.errMobile = false; }
                    if (op.rv_email == "") { i += 1; this.invalid.errEmail = true; } else { this.invalid.errEmail = false; }
                    return i == 0;
            },
            isConfirmOrderBtn: function () {
                var i = 0;
                var op = this.op;
                if (op.rv_name == "") { i += 1; this.invalid.errName = true; } else { this.invalid.errName = false; }
                if (op.rv_mobile == "") { i += 1; this.invalid.errMobile = true; } else { this.invalid.errMobile = false; }
                if (op.rv_email == "") { i += 1; this.invalid.errEmail = true; } else { this.invalid.errEmail = false; }
                if (op.rv_zip == "" ) { i += 1; this.invalid.errZip = true; } else { this.invalid.errZip = false; }
                if (op.rv_addr == "" ) { i += 1; this.invalid.errAddr = true; } else { this.invalid.errAddr = false; }
                return i == 0;
            },
            webPara: function () {
                var op = this.op;
                return escape(op.rv_name) + "|" + op.rv_mobile + "|" + op.rv_email;
            },
            webPara2: function () {
                var o = document.getElementById("webpara2"),
                    rv_name = o.getAttribute("data-rv_name"),
                    rv_mobile = o.getAttribute("data-rv_mobile"),
                    rv_email = o.getAttribute("data-rv_email"),
                    stName = o.getAttribute("data-stName");

                //console.log(rv_name, rv_mobile, rv_mobile)
                return escape(rv_name) + "|" + rv_mobile + "|" + rv_email + "|" + escape(stName);
            },
            rvName: function () {
                var name = document.getElementById("rvName").attributes["data-name"];
                return escape(name);
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                var url="/member/GetCartList";
                var orderid="";

                if (this.pageType == "showLastOrder") {
                    url="/member/GetOrderMList"
                    orderid= document.getElementById("cSMIF").getAttribute("data-orderid");
                }

                $.post(url, {orderid: orderid}, function (rs) {
                    console.log(rs.cart)
                    self.myCartCC = rs.myCartCC;
                    self.carts = rs.cart;
                });
            },
            addCCBtn: function (sn, cc) {
                console.log(sn);
                var self = this;
                
                $.post("/member/AddItemCC", { CartSN: sn }, function (rs) {
                    //self.myCartCC = rs.myCartCC;
                    self.carts = rs.cart;
                });
            },
            reduceCCBtn: function (sn, cc) {
                var self = this;
                if (cc > 0) {
                    $.post("/member/ReduceItemCC", { CartSN: sn }, function (rs) {
                        //self.myCartCC = rs.myCartCC;
                        self.carts = rs.cart;
                    });
                } else {
                    alert("數量不能低於0")
                }
            },
            removeItemInCart: function (cart) {
                $.post("/member/removeItemInCart", { CartSN: sn }, function (rs) {
                    //self.myCartCC = rs.myCartCC;
                    self.carts = rs.cart;
                });
            },
            clickCreateOrder: function () {
                var url = "/member/createorder";
                location.href = url;
            },
           
            checkStoreMap: function () {
                //var ezshipURL = "http://map.ezship.com.tw/ezship_map_web.jsp?suid=service@studentweekly.com.tw&processID=A0001&rtURL=" + hosturl + "/pay/GetMapInfor&";
                //var op = this.op;
                //var ee = "webPara=" + escape(op.rv_name) + "|" + op.rv_mobile + "|" + op.rv_email;

                //ezshipURL = ezshipURL + ee;
                //alert(ezshipURL );
                //location.href = ezshipURL;


                /*
                $.ajax({
                    url: 'http://map.ezship.com.tw/ezship_map_web.jsp',
                    //url: ezshipURL,
                    cache: false,
                    dataType: 'html',
                    type:'get', 
                    contentType: 'application/x-www-form-urlencoded; charset=Big5',
                    data: this.op,
                    error: function(xhr) {
                        console.log('Ajax request 發生錯誤');
                    },
                    success: function(response) {
                        //$('#msg').html(response);
                        //$('#msg').fadeIn();
                        console.log("success?", response)
                    }
                });

                var redirect = function(url, method) {
                    var form = document.createElement('form');
                    form.method = method;
                    form.acceptCharset="Big5";
                    //form.contentType = "application/x-www-form-urlencoded";
                    form.action = url;
                    form.submit();
                };*/

                //redirect(ezshipURL, 'get');

            },
            prviewOrder: function () {

            },
            clickCheckDiscountCode: function () {
                //todo:
                console.log("折扣碼", this.discountCode);
            },
            createOrder: function () {
                var ezshipURL = "https://www.ezship.com.tw/emap/ezship_request_order_api_ex.jsp?",
                    p = document.getElementById("cSMIF"),
                    rv_name = p.getAttribute("data-rv_name"),
                    rv_email = p.getAttribute("data-rv_email"),
                    rv_mobile = p.getAttribute("data-rv_mobile"),
                    st_cate = p.getAttribute("data-st_cate"),
                    st_code = p.getAttribute("data-st_code");

                //urlEncode(rv_name, "big5", function (rs) {
                    var arr = [
                    "su_id=service@studentweekly.com.tw",
                    "&order_id=A0001",
                    "&order_status=A01",
                    "&order_type=3",
                    "&order_amount=50",
                    "&rv_name=", rv_name,
                    "&rv_email=", rv_email,
                    "&rv_mobile=", rv_mobile,
                    "&st_code=", st_cate + st_code,
                    "&rtn_url=", hosturl + "/pay/orderinfor"
                    ];
                    ezshipURL = ezshipURL + arr.join("");
                    console.log(ezshipURL);
                    //location.href = ezshipURL;
                //})
                
            }

        }
    });



});