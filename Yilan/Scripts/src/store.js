﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var aan = document.getElementById("main").getAttribute("data-aan");
    var main = new Vue({
        mixins: [mixin_m, mixin_upload],
        el: "body",
        data: {
            currPage: "editStore",
            myCartCC:0,
            store: {},
            SelectedImgsCC: 1,
            errMsg: "",
            selectedTyp:"B",
            
            shared:[],
            individual: [],
            queryDate: "",
            queryKeyword:"",
        },
        created: function () {
            this.currPage = document.getElementById("main").getAttribute("data-currPage");
            switch (this.currPage) {
                case "editStore": this.fetchStoreData(); break; 
                case "factoryInfo": this.fetchMarketingData(); break;
                case "marketing": this.fetchMarketingData(); break;
            }
        },
        computed: {
           
        },
        watch: {
            "queryDate":function(val, oldval) {
                //console.log(val, oldval);
                this.fetchMarketingData();
            },
            "queryKeyword": function (val,oldval) {
                //console.log(val, oldval);
                this.fetchMarketingData();
            }
        },
        methods: {
            fetchStoreData: function () {
                var self = this,
                    contentSN= document.getElementById("main").getAttribute("data-contentSN");

                $.post("store/ReadStoreByContentSN", { contentSN: contentSN }, function (rs) {
                    self.store = rs;
                });
            },
            saveStoreInfor: function () {
                var self = this;
                var typ = this.store.typ;
                this.store.color = this.typOptions.filter(function (n) { return typ === n.value})[0].color;

                $.post("/store/EditStore", this.store, function (rs) {
                    self.errMsg = rs;
                    setTimeout(function () {
                        self.errMsg = "";
                    },3000);
                });
            },

            fetchMarketingData: function(){
                var self = this, arr=[];
                $.post("/getdata/ReadMarketingDataAll", {
                    shared: 99,
                    individual: 95,
                    dt: this.queryDate,
                    keyword: this.queryKeyword
                }, function (rs) {
                    self.shared = rs.shared;
                    arr = rs.individual;
                    self.individual = arr.sort(function () { return 0.5 - Math.random() });
                });
            },
            uploadImage: function (typ) {
                this.selectUploadImage(this.store.guid, typ);
            }


        }
    });



});