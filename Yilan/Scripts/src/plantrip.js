﻿define(["vue", "accounting", "sortable", "moment", "semantic"], function (Vue, accounting, Sortable) {
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            isBeginMap:true,
            codeName: document.getElementById("main").getAttribute("data-content-codename"),
            user: document.getElementById("main").getAttribute("data-user"),
            hostUrl: document.getElementById("main").getAttribute("data-hostUrl"),

            markerBasicLabes: { 0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F", 6: "G", 7: "H", 8: "I", 9: "J", 10: "K", 11: "L" },
            currFavorTrip: 0,    //目前喜愛的收藏路線
            
            sharedTrips: [],     //我的收藏商店、歷史紀錄
            storeMakers: [],     //店家+景點、美食的排序列表


            //美食、景點、交通、住宿 暫存區
            currTypedMarker : "", 
            TypMarkers: [],       
            isTypMarkersShow :false, 
            currStartLocation: "",

            //新加入@20180330
            currFacStore: 0, //單一商家
            currFacStores: [], //多商家
            markers:[],
            facStores: [],  //選擇觀光工廠
            unionMarketings: [], //聯合行銷
            currUnionMarketing: 1,
            isMainBtnsShow: false,
        },
        created: function () {
            this.currFavorTrip = 0;
            this.fetchData();
            //this.getFavorMarkders();
            //this.drawMarkers();
        },
        ready: function () {
            
        },
        watch: {
            currFacStore: function (n, o) {
                //console.log(n, n.name, n.lat, n.lng)
                this.deleteMarkers();
                this.addMarker(n);
                this.setMapOnAll(map);
            },
            currUnionMarketing: function (n, o) {
                //console.log(n, o);
                //聯合行銷的MARKER.................
                var uStores = n.ActiveStores,
                    uu = JSON.parse(uStores), d, e;

                this.deleteMarkers();
                for (c in uu) {
                    d = uu[c];
                    e = this.facStores.filter(function (n) { return (n.id === d) })[0]; 
                    console.log(d, e);
                    //setInfoWindow(e.name);
                    this.addMarker(e);
                }
                this.setMapOnAll(map);
            }
        },
        filters: {
            formatDigit2Albat: function (v, arr) {  
                var brr = arr.filter(function (n) { return n.isVisible === "Y"; });
                var t=-1;
                for (var i in brr) {
                    if (brr[i] == v) {
                        t=i
                    }
                }
                return (t < 0) ? "  " : this.markerBasicLabes[t];
            }
        },
        methods: {
            saveStartPoint: function () {
                var self = this;
                //console.log("currStartLocation,", this.currStartLocation);
                $.post("/trip/changeStartPoint", { tripid: this.currFavorTrip, startLocation: this.currStartLocation }, function () {
                    self.fetchData();
                    self.fetchFavoriteStore();
                });
            },
            fetchData: function () {
                var self = this;    
                //取得觀光工廠經緯度
                $.post("/trip/getFacStores", {}, function (rs) {
                    self.facStores = rs.facStores;
                    self.unionMarketings = rs.unionMarketings;

                    var _dd = document.getElementById("main").getAttribute("data-id");
                    var target = self.facStores.filter(function (n) { return _dd == n.id; })[0];
                    self.currFacStore = (target == undefined) ? 0: target;
                });
            },
            gotoTripGMap: function () {
                //console.log("gototripgmap:", this.currFacStore);
                var store = this.currFacStore;
                window.open(store.gmapTripUrl);
            },

            //一開始就把全部所有markers先製作好，但還不要放在map內。
            addMarker: function (p) {

                //console.log("p", p);
                if (p != undefined) {
                    //console.log("pw", typeof(p.lat), typeof(p.lng));
                    getGLatlng(p.lat, p.lng);
                    setGMarker(latlng, p.name);
                    var info = new google.maps.InfoWindow({
                        content: p.name
                    });

                    var _marker = marker;
                    marker.addListener('click', function () {
                        info.open(map, _marker);
                    });

                    this.markers.push(marker);
                }
            },
            setMapOnAll: function (map) {
                for (var i = 0; i < this.markers.length; i++)
                {
                    this.markers[i].setMap(map);
                }
            },
            clearMarkers: function () {
                this.setMapOnAll(null);
            },
            deleteMarkers: function () {
                this.clearMarkers();
                this.markers = [];
            },


            fetchFavoriteStore: function () {
                var self = this;
                $.post("/trip/GetStoreMarkers", { tripid: this.currFavorTrip }, function (rs) {
                    self.storeMakers = rs;
                    //console.log("my spots:", spots, rs);
                    self.drawDirectionServ();

                    var el = document.getElementById("favor-markers")
                    var sort = Sortable.create(el, {
                        group: 'foo',
                        sort: true,
                        draggable: ".ittem",
                        chosenClass: "sortable-chosen",
                        dragClass:"sortable-drag",
                        onEnd: function (e) {
                            var itemEl = e.item;//dragged DOM.
                            //console.log(e.from, e.to, e.oldIndex, e.newIndex);
                            var arr = self.storeMakers;
                            var nn = arr[e.newIndex];
                            arr[e.newIndex] = arr[e.oldIndex];
                            arr[e.oldIndex] = nn;

                            //console.log("arr:", arr, self.currFavorTrip);
                            $.post("trip/SortingMarkers", { tripid: self.currFavorTrip, oldSortSN: e.oldIndex, newSortSN: e.newIndex}, function () {
                                //TODO: 把目前排序寫入DB內重新
                                self.fetchData();
                                self.fetchFavoriteStore();
                            }); 
                        }
                    });

                });
            },
            changeStartPoint: function (ss) {
                
            },
            drawDirectionServ: function () {
                var c, d, markers = this.storeMakers;
                spots = [];

                //spots.push(self.startP.Name);
                for (c in markers) {
                    d = markers[c];
                    if (d.isVisible == "Y") {
                        spots.push(d.Name);
                    }
                }
                //console.log("after spots:", spots);
                //calculateAndDisplayRoute(directionsService, directionsDisplay, markerArray, stepDisplay, map);
            },
            getOpenTypsMarker: function (typ) {
                var self = this;
                $.post("/trip/getOpenTypsMarker", { typ: typ }, function (rs) {
                    self.TypMarkers = rs;
                });

            },
            ToggleMarkerVisible: function (storeSN) {
                var self = this;
                $.post("/trip/ToggleMarkerVisible", { sn: storeSN, tripid: this.currFavorTrip }, function (rs) {
                    self.storeMakers = rs;
                    self.drawDirectionServ();
                });
            },
            eyeSlashed: function (v) {
                return (v == "Y") ? "" : "-slash"
            },
            hiddenMarker: function (v) {
                return (v == "Y") ? "":"hidden";
            },
            formatIsLastRed: function (name, arr) {
                var brr = arr.filter(function (n) { return n.isVisible === "Y"; });
                var maxi = brr.length-1;
                return (name === brr[maxi].Name)?"red":"";
            },
            clickOpenTypsMarker: function (typ) {
                var self = this;
                $.post("/trip/getOpenTypsMarker", { typ: typ }, function (rs) {
                    self.currTypedMarker = typ;
                    self.TypMarkers = rs;
                    //self.drawDirectionServ();
                    self.isTypMarkersShow = true;
                });
            },
            appendTypedMarker: function (markerid, markerTyp) {
                var self = this;
                $.post("/trip/appendTypedMarker", { markerid: markerid, markerTyp: this.currTypedMarker, tripId : this.currFavorTrip }, function (rs) {
                    self.fetchFavoriteStore();
                    self.drawDirectionServ();
                    self.isTypMarkersShow = false;
                });
            },
            closeTypMarkersPanel: function () {
                this.isTypMarkersShow = false;
            },
            getTypMarkerColor: function (v) {
                return (v == undefined)? "": "bg-" + this.fnBtns.filter(function (n) {  return n.func === v })[0].color;
            },
            clickTripBtn: function (v) {
                var act = { "plan": this.newTrip() };
                act[v];
            },
            help: function () { },
            //儲存目前的路徑
            newTrip: function () {
                var self = this;
                var routeName = prompt("請幫這個行程規劃命名，若空白則不儲存。", "");
                if (routeName.trim() == "" || routeName == null) {
                    alert("沒有設定名稱");
                }
                else
                {
                    $.post("/trip/newTrip", { routeName: routeName }, function (rs) {
                        self.currFavorTrip = rs.maxFavorTripID;
                        self.fetchData();
                        self.fetchFavoriteStore();
                    });
                }
            },

        }
    });

});