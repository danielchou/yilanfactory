﻿define(["vue", "accounting","SimpleMDE", "moment", "semantic"], function (Vue, accounting, SimpleMDE) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    //Ref: https://github.com/NextStepWebs/simplemde-markdown-editor
    var simplemde = new SimpleMDE({ element: document.getElementById("MyID") });

    var main = new Vue({
        el: "#main",
        data: {            
           
        },
        created: function () {
            this.currContentSN = document.getElementById("main").getAttribute("data-content-sn");
            this.currContentGuid = document.getElementById("main").getAttribute("data-content-guid");
            this.fetchContentNodeList();
            this.fetchOneContent();
        },
        filters: {
            formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
            formatDateTime: function (v) { return ToJavaScriptDateTime(v); }
        },
        methods: {
            fetchContentNodeList: function () {
                var self = this;
                this.status = "read";
                $.post("/Admin/GetContentNodeList", { }, function (rs) {
                    //console.log(rs);
                    self.ContentNodes = rs;
                });
            },
            fetchOneContent: function () {
                var self = this;
                $.post("/Admin/GetOneContent", { contentGUID: self.currContentGuid }, function (rs) {
                    var cntt = rs.OneContent; console.log(cntt)
                    self.OneContent = cntt;
                    self.currContentParentSN = cntt.ParentSN;
                    self.currContentSN = cntt.SN;
                    simplemde.value((cntt.Body == null) ? "" : cntt.Body);

                    self.fetchItems();
                });
            },
            fetchItems: function () {
                var self = this;
                console.log(self.currContentSN);
                $.post("/Admin/GetItems", { contentSN: self.currContentSN }, function (rs) {
                    self.Items = rs.Items;
                });
            }
           
        }
    });

});