﻿var mixin_upload = {
    data: {
        HookID: "", //靠這個變數與外面所有任意物件綁定 /*非常重要!*/
        UploadTyp: "",  //上傳檔案類型: pdf? imagefile?....
        currImgGuid: "", //目前點選到的圖檔是什麼?
        isUploadFilePanelOpen: false,
        aan: document.getElementById("main").getAttribute("data-aan"),
        selectedImgsCC: 0,
        beforeUploadInfor: "ssssss",
        isFileLoading: false,
        currAttachFileSN: -1,
    },
    created: function () {
    },
    filters: {
        formatImgSrc: function (v, uploadURL, width, height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
    },
    methods: {
        onFileChange: function (e) {
            var files = e.target.files || e.dataTransfer.files;
            //console.log(files);
            if (!files.length)
                return;
            
            this.createImage(files);
        },
        OpenContentImagePanel: function () {
            this.HookID = this.currContentGuid;
            this.currAttachFileSN = -1;
            this.UploadTyp = "Content";
            this.isUploadFilePanelOpen = true;
            this.beforeUploadInfor = "";
        },
        saveFile: function (cb) {
            var self = this,
                fd = new FormData(),
                token = document.getElementsByName("__RequestVerificationToken")[0].value,
                blob = document.getElementById("files").files,
                len = blob.length;
                

            for (var i = 0; i < len; i++) {
                fd.append("files", document.getElementById("files").files[i]);
            }
            fd.append("__RequestVerificationToken", token);
            fd.append("HookID", this.HookID);
            fd.append("AttachFileSN", this.currAttachFileSN);
            fd.append("UploadTyp", this.UploadTyp);

            console.log("fd", fd);
            (cb && typeof (cb) === "function") && cb();

            $.ajax({
                url: this.aan + "/UploadFiles",
                type: "POST",
                data: fd,
                beforeSend: function () { self.isFileLoading = true; },
                success: function () {
                    //self.imgInforBeforeUpload = "";
                    self.isFileLoading = false;
self.currAttachFileSN = -1;
                    self.isUploadFilePanelOpen = false;
                    document.getElementById("files").value=""; //清空
                    cb();
                },
                error: function () { console.log("Err"); },
                processData: false,  // tell jQuery not to procefss the data
                contentType: false   // tell jQuery not to set contentType
            });
        },
        deleteImg: function(img, cb) {
            var self = this;
            (cb && typeof(cb) === "function") && cb();
            console.log("deleteImg", typeof(cb));
            if (confirm("確定要刪除該圖片?")) {
                $.post(this.aan + "/RemoveItemImage", { imageGuid: img.Guid }, function (rs) {
                    //self.fetchItems(self.currContentSN);
                    cb();
                });
            }
        },
        editImgRe: function (img) {
            console.log(img.SN);
            this.currAttachFileSN = img.SN
            this.isUploadFilePanelOpen = true;
        },
        createImage: function(files) {
            var image = new Image();

            var self = this;
            var c, ss = "";
            var i = 0;
            for (d in files) {
                c = files[d].name;
                if (c === undefined || c === "item") {
                }
                else
                {
                    ss += c + "[" + Math.round(files[d].size / 1024) + "KB]，";
                    i += 1;
                }
                
                //var reader = new FileReader();
                //reader.onload = function(e) { self.image = e.target.result; };
                //reader.readAsDataURL(c);
            }
            self.selectedImgsCC = i;
            this.beforeUploadInfor = ss;

        },
        closeUploadPanel: function () {
            this.isUploadFilePanelOpen = false;
        },
        editImg: function (img) {
            console.log(img);
            this.currImgGuid = img.Guid;
        },
        imgDscrSave: function (img) {
            var self = this;
            $.post(this.aan + "/EditDscrItemImage", { imageGuid: img.Guid, imageDscr: img.Dscr }, function (rs) {
                console.log(self.currContentSN);
                self.fetchItems(self.currContentSN);
                self.currImgGuid = "";
            });
        },
        imgDscrCancel: function () {
            this.currImgGuid = "";
        },
        lockImg: function (img) {
            console.log(img);
        },
        onMasterImg: function (img) {
            var self = this;
            $.post(this.aan + "/onMasterImg", { hookID: img.HookID, imageGuid: img.Guid }, function (rs) {
                self.fetchItems(self.currContentSN);
                self.currImgGuid = "";
            });
        },

        selectUploadItemImage: function (itemGuid) {
            this.HookID = itemGuid;
            this.UploadTyp = "Item";
            this.isUploadFilePanelOpen = true;
            this.beforeUploadInfor = "";
            document.getElementById("files").value = ""; //清空
        },
        //這是比較通用型的
        selectUploadImage: function (itemGuid, typ) {
            this.HookID = itemGuid;
            this.UploadTyp = typ;
            this.isUploadFilePanelOpen = true;
            this.beforeUploadInfor = "";
            document.getElementById("files").value = ""; //清空
        },
        closeUploadPanel: function () {
            this.isUploadFilePanelOpen = false;
        },
    }
}