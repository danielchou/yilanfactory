﻿var mixin_m = {
    data: {
        currItemlbTyp: "market",
        isMainBtnsShow: false,
        typOptions: [
            { value: 'A', text: "無", color:'#aaa'  },
            { value: 'B', text: "烘培", color:'#f90d5c' },
            { value: 'C', text: "酒廠", color: '#ff664b' },
            { value: 'D', text: "特產", color: '#FE9B3E'  },
            { value: 'E', text: "養生", color: '#5ece57'  },
            { value: 'F', text: "生技", color: '#00DDAA' },/*#5ece57*/
            { value: 'G', text: "水產", color: '#3d44ab'  },
            { value: 'H', text: "文創", color: '#ad7ff7'  }
        ],
        btns: [
            { id: 1, name: "+新行程規劃", func:"plan", icon: "map outline marker" },
            //{ id: 2, name: "歷史路徑", func: "history",icon: "history" },
            //{ id: 3, name: "儲存", func: "save", icon: "save" },
            //{ id: 4, name: "說明", func: "help", icon: "question mark" },
        ],
        fnBtns: [
            { id: 0, name: "起始", func: "start", color: "red" },
            { id: 1, name: "景點", func: "spot", color: "green" },
            { id: 2, name: "美食", func: "food", color: "orange" },
            { id: 3, name: "交通", func: "trafic", color: "blue" },
            { id: 4, name: "住宿", func: "lodging", color: "purple" },
        ]
    },
    computed: {
        itemlb: function () {
            var item= {
                title1: "大標題(500字)AAA",
                title2: "標題2(500字)",
                price_title: "銷售產品相館",
                price_origin: "原價(元)",
                price_discount: "折扣(元)",
                prod_type: "產品分類",
                other: "其他",
                background_color: "背景顏色",
                sorting: "排列順序",
                name: "簡短文(1000)",
                dscr: "簡短描述",
                main_content: "主要內容說明(最多2000字)"
             };
            var market = {
                title1: "行銷名稱",
                title2: "行銷副標",
                dateRange: "行銷優惠日期",
                startDate: "開始日期",
                endDate: "截止日期",
                price_title: "銷售產品相館",
                price_origin: "原價(元)",
                price_discount: "折扣(元)",
                prod_type: "產品分類",
                other: "其他",
                background_color: "背景顏色",
                sorting: "排列順序",
                name: "行銷內容(套版DM2時填寫)",
                dscr: "簡短描述",
                main_content: "主要內容說明(最多2000字)"
            };
            return (this.currItemlbTyp=="item")? item : market;
        }
    },
    filters: {
        formatIsChecked: function (v) { return (v==="Y") ? "<i class='ui icon large check circle green' ></i>" : "" },
        formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
        formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
        accounting: function (v) { return accounting.formatMoney(v); },
        accountingGer: function (v) { return accounting.formatMoney(v, "", 0); },
        toBoolean: function (v) { return (v === "Y"); },
        toTableTypName: function (v) { return this.TableTyps[v]; },
        formatImgSrc: function (v, uploadURL, width, height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
        formatTypMarkerDisplayName: function (v) { return this.fnBtns.filter(function (n) { return n.func == v })[0].name; },
    },
    methods: {
        toggleMainBtns: function ()
        {
            console.log(this.isMainBtnsShow);
            this.isMainBtnsShow = !this.isMainBtnsShow;
            console.log(this.isMainBtnsShow);
        },
        login2Google: function () {
            window.open("/account/login?returnUrl=/admin", "_blank");
        }
    }
}