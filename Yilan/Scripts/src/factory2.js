﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting, showdown) {
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            codeName: document.getElementById("main").getAttribute("data-content-codename"),
            user: document.getElementById("main").getAttribute("data-user"),
            imgs: [],
            store: {},
            placeInfo: [],
            rating: 4,
            isFavoriteStore: 0,
            shared_dscr: "",
            isSharedDscrShow: false,
            isCloseComment: false,
        },
        created: function () {
            
            this.fetchData();
        },
        ready: function () {
            var self = this;
            place.getDetails(request, function (place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    o_placeInfo = place.reviews;
                    o_rating = place.rating;
                    console.log(o_placeInfo, o_rating)

                    self.placeInfo = o_placeInfo;
                    self.rating = o_rating;
                    $('.ui.star.rating').rating();
                }
            });
            
            $('.ui.star.rating').rating();
        },
        watched: {
        },
        filters: {
            formatTag: function (v) { v += ""; return v.split("|")[0].replace("M", ""); },
            formatBanner: function (v) { v += ""; return v.split("|")[1];}
        },
        computed: {
            isFavorColor: function () {
                return (this.isFavoriteStore === 1) ? "red" : "blue";
            },
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post("/getdata/GetSingleStoreContent", { codeName: this.codeName }, function (rs) {
                    self.store = rs.store;
                    self.isCloseComment = rs.store.isCloseComment;
                    self.imgs = rs.imgs;
                    self.isFavoriteStore = rs.isFavoriteStore;
                });
            },
            find3Func: function () {
                //為了很糟糕的排序.....
                console.log(this.imgs);
                return [this.findTag(this.imgs, "享食"), this.findTag(this.imgs, "玩樂"), this.findTag(this.imgs, "服務") ];
            },
            find3FuncMobile: function () {
                //為了很糟糕的排序.....
                console.log(this.imgs);
                return [this.findTag(this.imgs, "享食M"), this.findTag(this.imgs, "玩樂M"), this.findTag(this.imgs, "服務M")];
            },
            findTag: function (arr, v) {
                return arr.filter(function (n) {
                    var tag = n.Dscr.split("|")[0];
                    return (tag == v);
                })[0];
            },
            findBanner: function () {
                return this.imgs.filter(function (n) {
                    var tag = n.Dscr.split("|")[0];
                    return (tag === "Banner");
                });
            },
            isMoreDscr: function (ss) {
                var dscr2 = ss.split("|")[2];
                //console.log("isMoreDscr:", dscr2);
                return !( dscr2 == undefined || dscr2 == null || dscr2.trim() == "");
            },
            gotoFB: function () {
                window.open(this.store.fb);
            },
            gotoWeb: function () {
                window.open(this.store.url);
            },
            ////舊版沒有用了
            //myfavorite: function (storeid) {
            //    var self = this;
            //    console.log("stet,", storeid);
            //    if (this.user === "") {
            //        location.href = "/account/login?returnUrl=" + location.pathname;
            //    } else {
            //        $.post("/store/SaveMyFavoriteStore", { storeId: storeid }, function () {
            //            self.fetchData();
            //        });
            //    }
            //},
            gotoNavigator: function (storeid) {
                var self = this;
                console.log("stet,", storeid);
                location.href = "/plantrip/" + storeid;
            },
            discountInfo: function () {
                location.href ="/DM/"+this.codeName
            },
            sendMoreDscr: function (ss) {
                var dscr2 = ss.split("|")[2];
                this.shared_dscr = dscr2;
                this.isSharedDscrShow = true;
            },
            closeDscrPanel: function () {
                this.isSharedDscrShow = false;
            }

        }
    });

});