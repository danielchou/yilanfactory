﻿define(["vue", "accounting", "showdown", "moment", "semantic"], function (Vue, accounting, showdown) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    var converter = new showdown.Converter();
    var aan = document.getElementById("main").getAttribute("data-aan");
    var uid = $("#UserID").html();

    function adminurl(v) {
        return "/" + aan + "/" + v;
    }

    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            currContentSN:0,
            aan: "",

            myCartCC:0,
            currRouteName: "preschool",
            currContentGuid: "",
            Content: {},
            Content2: {},
            RoutePath:"",
            ContentSplitBody: {},
            Images: [],
            SN: 0,
            items: {},
            ftRouter: [],

            //下載管理元
            DwnURLTyps:[],
            downloadfiles: [],
            currDwnloadTyp:"",
            currDwnloadTyp2: "",
            ProdTyps: [],
            isShowPanel: false,
            currDownloadItemSN:0,
            mp4: { sn: 0, typ: "", typ2: "", name: "", dwurl: "", imgurl: "" },

            //訂單管理相關
            orderHeaders: ["訂單編號","會員號碼","會員資料","配送超商","配送狀況","訂單產生時間","購買明細"],
            Orders:[]
        },
        created: function () {
            this.currContentSN = document.getElementById("main").getAttribute("data-content-sn");
            this.currContentGuid = document.getElementById("main").getAttribute("data-content-guid");
            this.fetchItems();
            
        },
        filters: {
            formatMDE: function (v) { return converter.makeHtml(v); },
            formatDscr: function (v) { return (v == null) ? "" : v.substring(1, 100) + "..."; },
            formatImgSrc: function (v, uploadURL, width, height) {
                return (uploadURL=='')
                       ?                   v + "?w=" + width + "&h=" + height + "&mode=crop"
                       : uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop";
            },
            formatImgSrcFirst: function(arr, uploadURL,width,height) 
            {
                if (arr.length > 0 ) {
                    return uploadURL + "/" + arr[0].FileNameNew + "?w=" + width + "&h=" + height + "&mode=crop";
                }
            },
            splitSubTitle: function (v) {
                return v.split("X");
            }
        },
        computed: {
            getImgHeight: function(v) {
                return document.getElementById("mainimg").offsetHeight * v / 100 +'px';
            }
        },
        methods: {
            fetchItems: function () {
                var self = this;
                var url = "getdata/GetRightItemUnderSomeContent";

                $.post(url, { ContentSN: this.currContentSN }, function (rs) {
                    
                    var r = rs[0];
                    self.Content = r.Parent;
                    self.Content2 = r.Children;
                    self.RoutePath = r.RoutePath;
                    self.SN = r.SN;
                    self.items = r.Parent.items; console.log(self.items)
                    self.myCartCC = r.Parent.myCartCC;
                    self.DwnURLTyps = r.DwnURLTyp;
                });
            },
            toggleWeeklyMainTitle: function (routeName) {
                this.currRouteName = routeName;
            },
            addCartFavor: function (itemSN, typ) {
                //typ: /*cart|favor*/
                var self = this;
                if (uid == null || uid == undefined) {
                    location.href = "/account/login";
                } else {
                    var cart = { sn: 0, custID: uid, prodID: itemSN, typ: typ, isRemove:false };
                    $.post(adminurl("addCartFavor"), { cart: cart }, function (rs) {
                        self.fetchItems();
                    });
                }
            },
            removeCartFavor: function (itemSN, typ) {
                var self = this;
                if (uid == null || uid == undefined) {
                    location.href = "/account/login";
                } else {
                    var cart = { sn: itemSN, custID: uid, prodID: itemSN, typ: typ, isRemove:true };
                    $.post(adminurl("removeCartFavor"), { cart: cart }, function (rs) {
                        self.fetchItems();
                    });
                }
            },
            /* download-manger */
            clickGetTyp2: function (typ,typ2Name) {
                //console.log(typ2Name);
                this.currDwnloadTyp = typ;
                this.currDwnloadTyp2 = typ2Name;
                var self = this;
                $.post("/getdata/getDwnList", { typ2: this.currDwnloadTyp2 }, function (rs) {
                    self.downloadfiles = rs
                });
            },
            clickCancelPanel: function () {
                this.isShowPanel = false;
            },
            openEditPanel: function (e) {
                this.isShowPanel = true;
                var m = this.mp4
                m.typ = e.typ
                m.typ2 = e.typ2
                m.name = e.name
                m.dwurl = e.dwurl
                m.imgurl = e.imgurl
                m.sn = e.sn
                this.currDownloadItemSN = e.sn
            },
            clickSaveDownloadItem() {
                var self = this;
                $.post("/getdata/saveDownloadItem", { downloaditem: self.mp4 }, function (rs) {
                    self.fetchItems();
                    self.clickGetTyp2(self.currDwnloadTyp, self.currDwnloadTyp2);
                    self.isShowPanel = false;
                });
            },
            clickAddNewDownloadItem() {
                this.isShowPanel = true;
                var m = this.mp4
                m.typ = this.currDwnloadTyp
                m.typ2 = this.currDwnloadTyp2
                m.name = ""
                m.dwurl = ""
                m.imgurl = ""
                m.sn = 0
            },
            clickDeleteDwnItem(e) {
                var self = this;
                if (confirm("確定要刪除?")) {
                    $.post("/getdata/DeleteDwnItem", { downloaditem: e }, function (rs) {
                        self.fetchItems();
                        self.clickGetTyp2(self.currDwnloadTyp,self.currDwnloadTyp2);
                        self.isShowPanel = false;
                    });
                }
            }

           
        }
    });

});