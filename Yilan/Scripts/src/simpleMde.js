﻿define(["vue", "accounting","SimpleMDE","showdown", "moment", "semantic"], function (Vue, accounting, SimpleMDE, showdown) {

    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });
    
    //Ref: https://github.com/NextStepWebs/simplemde-markdown-editor
    var simplemde = new SimpleMDE({ element: document.getElementById("MyID") });
    var converter = new showdown.Converter();

    var main = new Vue({
        mixins: [mixin_m, mixin_upload],
        el: "#main",
        data: {           
            aan: "admin", // alias admin name的縮寫
            isEdit: false,
            status: "",
            //-------------------------------
            ContentNodes: {},
            OneContent : {
                SN: (this.status == "addnew") ? 0 : this.currContentSN
                , ParentSN: this.currContentSN, RouteName: "", BigTitle: "", SubTitle: "", Body: "", Dscr: "", Note: "", MetaKeyword: "", MetaDescription:""
            },
            currContentSN: 1,
            currContentParentSN:0,
            currContentGuid: "",
            Items: {},
            Item: {
                SN: 0, ContentSN: 0, Title: "", BgImageUrl: "", ImageName: ""
                , YoutubeUrl: "", Dscr: "", Cost: 0, Cost2: 0, startDate: "", endDate: "", Typ: "", BgColor: "#fff", imgs: {}, MetaKeyword: "", MetaDescription: ""
            },
            
            currItemSN: 0,
            ItemsCC: {},
            NewItem: { SN: 0, ContentSN: 0, Title: "", BgImageUrl: "", ImageName: "", YoutubeUrl: "", Dscr: "", Cost: 0, Cost2: 0, Typ: "", BgColor : "", imgs: {} },
            //======================
            isHeadContent: true, //預設是content? item?
            isItemEdit: false,
            isMarkdownShow: true,
            isAdminAccount: true,
            storeList: [],
            currActiveStore: [],
        },
        computed: {
            isAddNewStore: function () {
                //是否新增店家按鈕?
                return (this.currContentSN == 95); 
            }
        },
        beforeCreate: function () {
            //this.isItemEdit=false;
        },
        created: function () {
            var m = document.getElementById("main");
            this.currContentSN = m.getAttribute("data-content-sn"); 
            //console.log("currContentSN:", this.currContentSN);
            this.currContentGuid = m.getAttribute("data-content-guid");
            this.aan = "/"+m.getAttribute("data-aan");
            this.fetchContentNodeList();
            this.fetchOneContent();
            this.fetchStoreList();
        },
        watch: {
            "select33edFiles": function (val, oldVal) {
                this.oldSelectedFiles = oldVal;
            },
        },
        filters: {
            formatMDE: function (v) { if (v != null) return converter.makeHtml(v); }
        },
        methods: {
            fetchContentNodeList: function () {
                var self = this;
                this.status = "read";
                $.post(this.aan + "/GetContentNodeList", {}, function (rs) {
                    
                    self.ContentNodes = rs;
                    self.isAdminAccount = (rs.isAdminAccount == null);
                });
            },
            fetchOneContent: function() {
                var self = this;
                if (self.currContentGuid != null) {
                    $.post(this.aan + "/GetOneContent", { contentGUID: self.currContentGuid }, function (rs) {
                        var cntt = rs.OneContent; //console.log(cntt)
                        self.OneContent = cntt;
                        self.currContentParentSN = cntt.ParentSN;
                        self.currContentSN = cntt.SN;

                        //console.log(cntt.Body);
                        simplemde.value((cntt.Body == null) ? "" : cntt.Body);
                        self.fetchItems();
                    });
                }
            },
            fetchItems: function () {
                var self = this;
                //console.log(self.currContentSN);
                $.post(this.aan + "/GetItems", { contentSN: self.currContentSN }, function (rs) {
                    self.Items = rs.Items;
                });
            },
            fetchItemsCC: function () {
                var self = this;
                $.post(this.aan + "/GetItemsCC", {}, function (rs) {
                    self.ItemsCC = rs;
                });
            },
            fetchItemImages: function (itemSN) {
                $.post(this.aan + "/GetItemImages", { itemSN: itemSN }, function (rs) {
                    //self.Items = rs.Items;
                });
            },
            fetchStoreList: function () {
                var self = this;
                $.post(this.aan + "/GetStoreList", { }, function (rs) {
                    self.storeList = rs;
                });
            },
            AddNewContent: function () {
                this.isEdit = true;
                this.status = "addnew";
                this.isMarkdownShow = true;

                var me = this.OneContent;
                me.SN = 0;
                me.ParentSN = this.currContentSN;
                me.BigTitle = "";
                me.SubTitle = "";
                me.Body = "";
                simplemde.value("");
            },
            UpdateContent: function () {
                this.isEdit = true;
                this.status = "update";
                this.isMarkdownShow = true;
            },
            SaveContent: function () {
                var self = this;
                this.isEdit = false;
                this.OneContent.Body = simplemde.value();
                $.post(this.aan + "/SaveContent", self.OneContent, function (rs) {
                    self.fetchOneContent();
                });
            },
            SelectContentNode: function (n) {
                var self = this;
                this.currContentGuid = n.GUID;
                this.currContentSN = n.SN;
                console.log(n);

                this.isItemEdit = false;
                this.isMarkdownShow = false;
                this.fetchOneContent();
                //window.location.href = "admin?k=" + guid;
            },
            clickCancelEdit: function () {
                this.isEdit = false;
                this.status = "read";

            },
            RemoveContent: function () {
                this.isEdit = false;
                this.status = "remove";
                var self = this;
                $.post(this.aan + "/RemoveContent", { contentGUID: this.currContentGuid }, function (rs) {
                    self.fetchContentNodeList();
                    self.fetchData();
                });
            },

            clickSelectHeadContent: function () {
                this.isHeadContent = true;
                this.isMarkdownShow = true;
            },
            clickSelectItems: function () {
                this.isHeadContent = false;
            },
            AddNewItem: function () {
                this.isItemEdit = true;
                this.currActiveStore = [];
                this.Item = {
                    SN: 0, Title: "", BgImageUrl: "", ImageName: ""
                    , YoutubeUrl: "", Dscr: "", Cost: 0, Cost2: 0, startDate: "", endDate: "", Typ: "", BgColor: "#fff", imgs: {}, MetaKeyword: "", MetaDescription: ""
                };
            },
            RemoveItem: function () {

            },
            SaveItem: function () {
                var self = this;
                self.Item.ContentSN = this.currContentSN;
                //self.Item.Dscr = simplemde.value();
                //console.log(this.currActiveStore, JSON.stringify(self.currActiveStore) );
                self.Item.ActiveStores = JSON.stringify(self.currActiveStore);

                $.post(this.aan + "/SaveItem", self.Item, function (rs) {
                    self.fetchItems(self.currContentSN);
                    self.isItemEdit = false;
                });
            },
            CancelEditItem: function () {
                this.isItemEdit = false;
            },
            clickEditItem: function (item) {
                var self = this;

                $.post(this.aan + "/EditItem", { itemSN: item.SN }, function (rs) {

                    self.isHeadContent = false;
                    self.isItemEdit = true;
                    self.Item = rs;
                    self.Item.startDate = ToJavaScriptDate(rs.startDate);
                    self.Item.endDate = ToJavaScriptDate(rs.endDate);

                    
                    var arr = JSON.parse(JSON.stringify(rs.ActiveStores));
                    arr = eval(arr);
                    self.currActiveStore = (arr == null)? []: arr;
                });
            },
            clickRemoveItem: function (item) {
                var self = this;
                $.post(this.aan + "/RemoveItem", { itemSN: item.SN }, function (rs) {
                    self.fetchItems(self.currContentSN);
                });
            },
            GotoAddNewStore: function () {
                location.href = "/store";
            }
        }
    });

});