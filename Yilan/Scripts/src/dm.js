﻿define(["vue", "accounting", "moment", "semantic"], function (Vue, accounting, showdown) {
    $(document).bind("ajaxSend", function () { $("#loading").show(); }).bind("ajaxComplete", function () { $("#loading").hide(); });

   

    var main = new Vue({
        mixins: [mixin_m],
        el: "body",
        data: {
            codeName: document.getElementById("main").getAttribute("data-content-codename"),
            user: document.getElementById("main").getAttribute("data-user"),
            imgs: [],
            store: {},
            isFavoriteStore: 0,

            dm1: {},
            dm2: {},
            dm3: {},
        },
        created: function () {
            this.fetchData();
            
        },
        ready: function () {
            $('.ui.star.rating').rating();
        },
        filters: {
            formatTag: function (v) { return v.split("|")[0] },
            formatBanner: function (v) {return v.split("|")[1];}
        },
        computed: {
            isFavorColor: function () {
                return (this.isFavoriteStore == 1) ? "red" : "blue";
            },
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.post("/getdata/GetDMContent", { codeName: this.codeName }, function (rs) {
                    self.store = rs.store;
                    self.imgs = rs.imgs;
                    self.isFavoriteStore = rs.isFavoriteStore;
                    self.dm1 = rs.items;
                });
            },
            find3Func: function () {
                return this.imgs.filter(function (n) {
                    var tag = n.Dscr.split("|")[0]
                    return (tag === "享食" || tag === "玩樂" || tag === "服務");
                });
            },
            findBanner: function () {
                return this.imgs.filter(function (n) {
                    var tag = n.Dscr.split("|")[0];
                    return (tag === "Banner");
                });
            },
            findDM1: function () {
                return this.imgs.filter(function (n) {
                    var tag = n.Dscr.split("|")[0];
                    return (tag === "DM1");
                });
            },
            gotoFB: function () {
                window.open(this.store.fb);
            },
            gotoWeb: function () {
                window.open(this.store.url);
            },
            myfavorite: function (storeid) {
                var self = this;
                console.log("stet,", storeid);
                if (this.user == "") {
                    location.href = "/account/login";
                } else {
                    $.post("/store/SaveMyFavoriteStore", { storeId: storeid }, function () {
                        self.fetchData();
                    });
                }
            },
            discountInfo: function () {
                location.href ="/DM/"+this.codeName
            }

        }
    });

});