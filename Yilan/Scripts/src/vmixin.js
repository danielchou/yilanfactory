﻿var mixin = {
    data: {
        isUploadFilePanelOpen:false,
    },
    created: function () {
        console.log("mixin hook called");
    },
    filters: {
        formatDate: function (v) { if (v != null) return ToJavaScriptDate(v); },
        formatDateTime: function (v) { return ToJavaScriptDateTime(v); },
        formatImgSrc: function (v, uploadURL, width, height) { return uploadURL + "/" + v + "?w=" + width + "&h=" + height + "&mode=crop"; },
    },
    methods: {
        onFileChange: function (e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            //console.log(files);
            this.createImage(files);
        },
        saveFile: function () {
            var fd = new FormData(),
                token = document.getElementsByName("__RequestVerificationToken")[0].value,
                HookID = document.getElementById("HookID").value,
                blob = document.getElementById("files").files,
                len = blob.length,
                self = this;

            for (var i = 0; i < len; i++) {
                fd.append("files", document.getElementById("files").files[i]);
            }
            fd.append("__RequestVerificationToken", token);
            fd.append("HookID", this.HookID);
            fd.append("UploadTyp", this.UploadTyp); //console.log("fd", fd);
            //console.log("fd", fd);
            $.ajax({
                url: this.aan + "/UploadFiles",
                type: "POST",
                data: fd,
                beforeSend: function () { self.isFileLoading = true; },
                success: function () {
                    self.fetchOneContent(); ///self.fetchItems();
                    //self.imgInforBeforeUpload = "";
                    self.isFileLoading = false;
                    self.isUploadFilePanelOpen = false;
                    document.getElementById("files").files = {};
                },
                error: function () { console.log("Err"); },
                processData: false,  // tell jQuery not to procefss the data
                contentType: false   // tell jQuery not to set contentType
            });
        },
        closeUploadPanel: function () {
            this.isUploadFilePanelOpen = false;
        },

    }
}