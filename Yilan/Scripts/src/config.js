﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../Scripts/vendor/",
    paths: {
        'vue': ['vue/dist/vue'],
        "jquery": ['jquery/dist/jquery.min'],
        "core": ["jqueryui/ui/minified/core.min"],
        "datepicker": ["jqueryui/ui/minified/datepicker.min"],
        "semantic": ["semantic-ui/dist/semantic.min"],
        "accounting": ["accounting.js/accounting.min"],
        "fullcalendar": ["fullcalendar/dist/fullcalendar.min"],
        "moment": ["moment/min/moment.min"],
        "SimpleMDE": ["simplemde/dist/simplemde.min"],
        "showdown": ["showdown/dist/showdown.min"],
        "sortable": ["Sortable/Sortable.min"],
    },
    shim: {
        "semantic": { deps: ["jquery"], exports: "semantic" },
        "datepicker": ["jquery"]
    }
});
