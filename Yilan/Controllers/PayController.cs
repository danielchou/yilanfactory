﻿using CMWebApp.Models;
using NLog;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Attr;
using System.Collections.Specialized;

namespace CMWebApp.Controllers
{

    public class PayController : Controller
    {

        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;
        Common.EZShip ez = new Common.EZShip();

        [Big5CharsetAttribute]
        [HttpPost]
        public ActionResult GetMapInfor(
            string processID,
            string stCate,
            string stCode,
            string stName,
            string stAddr,
            string stTel,
            string webPara
            )
        {
            // 接受到的碼 %u8521%u4F9D%u6797|02-09234234234|asfqwerqwer@gmail.com
            string ww = ""+webPara; 
            webPara = Server.UrlDecode(webPara);
            
            //processID Y varchar 10 處理序號或訂單編號 由開通網站自行提供的編號(KEY值)。ezShip原值回傳。
            //stCate Y varchar 3 取件門市通路代號 TFM - 全家超商；TLF - 萊爾富超商；TOK - OK超商
            //stCode Y varchar 6 取件門市代號
            //stName Y varchar 60 取件門市名稱
            //stAddr N varchar 120 取件門市地址
            //stTel N varchar 10 取件門市電話
            //webPara  你傳遞的任意資料
            ViewData["processID"] = processID;
            ViewData["stCate"] = stCate;
            ViewData["stCode"] = stCode;
            ViewData["stName"] = stName;    
            ViewData["stAddr"] = stAddr;
            ViewData["stTel"] = stTel;

            string[] arr = webPara.Split(new string[] { "|" }, StringSplitOptions.None);
     
            
            ViewData["rv_name"] = arr[0]; 
            ViewData["rv_mobile"] = arr[1];
            ViewData["rv_email"] = arr[2];

            string maxOrderID = "";
            int k = 0;
            using(db=new DB_ShiziEntities())
            {
                if (db.OrderM.Count() == 0)
                {
                    k = 1;
                    maxOrderID = DateTime.Now.Year.ToString().Substring(2, 2)
                                    + DateTime.Now.Month.ToString("D2")
                                    + DateTime.Now.Day.ToString("D2")
                                    + "-"
                                    + k.ToString("D3");
                }
                else
                {
                    maxOrderID = db.OrderM.Max(x => x.id); //format: 17A00012  170908-002
                    k = int.Parse(maxOrderID.Substring(8, 3)) + 1;
                    maxOrderID = DateTime.Now.Year.ToString().Substring(2,2)
                                    + DateTime.Now.Month.ToString("D2")
                                    + DateTime.Now.Day.ToString("D2")
                                    + "-"
                                    + k.ToString("D3");
                }
            }
            ViewData["order_id"] = maxOrderID;
            return View();
        }

        private static NameValueCollection GetEncodedForm(System.IO.Stream stream, Encoding encoding)
        {
            System.IO.StreamReader reader = new System.IO.StreamReader(stream, Encoding.ASCII);
            return GetEncodedForm(reader.ReadToEnd(), encoding);
        }

        private static NameValueCollection GetEncodedForm(string urlEncoded, Encoding encoding)
        {
            NameValueCollection form = new NameValueCollection();
            string[] pairs = urlEncoded.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (string pair in pairs)
            {
                string[] pairItems = pair.Split("=".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);
                string name = HttpUtility.UrlDecode(pairItems[0], encoding);
                string value = (pairItems.Length > 1) ?
                        HttpUtility.UrlDecode(pairItems[1], encoding) : null;
                string kk = "";
                if(name== "webPara")
                {
                    kk = value;
                }
                form.Add(name, value);
            }
            return form;
        }

        public string ConvertBig5(string strUtf)
        {
            Encoding utf81 = Encoding.GetEncoding("utf-8");
            Encoding big51 = Encoding.GetEncoding("big5");
            Response.ContentEncoding = big51;
            byte[] strUtf81 = utf81.GetBytes(strUtf.Trim());
            byte[] strBig51 = Encoding.Convert(utf81, big51, strUtf81);

            char[] big5Chars1 = new char[big51.GetCharCount(strBig51, 0, strBig51.Length)];
            big51.GetChars(strBig51, 0, strBig51.Length, big5Chars1, 0);
            string tempString1 = new string(big5Chars1);
            return tempString1;
        }



        public ActionResult orderinfor(
                string order_id,
                string sn_id,
                string order_status,
                string webPara
            ) {


            ViewData["order_id"] = order_id;
            ViewData["sn_id"] = sn_id;
            ViewData["order_status"] = order_status;
            ViewData["oss"] = ez.orderStatusDisplay(order_status);
            string webpara = Server.UrlDecode(webPara);
            ViewData["webpara"] = webpara;

            //order_id = "17A00005";
            //ViewData["order_id"] = order_id;
            //ViewData["sn_id"] = "207441105";
            //ViewData["order_status"] = "S01";
            //order_status = "S01";

            //string webpara = "周杰倫|0923422123|daniel0422@gmail.com|全家通額電";

            string[] arr = webpara.Split(new[] { "|" }, StringSplitOptions.None);
            string custName = arr[0],
                        mobile = arr[1],
                        email = arr[2],
                        stName = arr[3],
                        memberId = User.Identity.Name;

            ViewData["rv_name"] = custName;
            ViewData["rv_mobile"] = mobile;
            ViewData["rv_email"] = email;
            ViewData["stName"] = stName;
            string guid = Guid.NewGuid().ToString();
            if (order_status == "S01")
            {

                using(db=new DB_ShiziEntities())
                {
                    int? Sum = 0;
                    int? Cost = 0;

                    //客戶id在購物車內數量大於0的產品要變更狀態
                    var es = db.vCart.Where(x => x.custID == memberId && x.typ == "cart" && x.isRemove == false && x.count>0).ToList();
                    foreach(var e in es)
                    {
                        var m = db.Cart.Where(x => x.sn == e.sn).SingleOrDefault();
                        m.orderID = order_id;
                        m.typ = "order";
                        m.updateTime = DateTime.Now;

                        Cost = (e.Cost2 == null) ? e.Cost : e.Cost2;
                        Sum += m.count * e.Cost;
                        db.SaveChanges();
                    }

                    

                    //存寫入訂單主檔
                    OrderM o = new OrderM()
                    {
                        id = order_id,
                        isRemove = false,
                        createDate = DateTime.Now,
                        memberId = memberId,
                        email = email,
                        guid = guid,
                        custName = custName,
                        mobile = mobile,
                        stName = stName,
                        totalPrice=Sum
                    };
                    //寫入訂單主檔
                    db.OrderM.Add(o);
                    db.SaveChanges();
                }
                
            }else
            {
                ViewData["webpara"] = "抱歉，您申請的訂單有問題，請洽客服人員，感謝您。";
            }

            return RedirectToAction("ShowLastOrder", new { g = guid } );
        }


        public ActionResult ShowLastOrder(string g)
        {
            string guid = g;
            using (db = new DB_ShiziEntities())
            {
                var es = db.OrderM.Where(x => x.guid == guid).SingleOrDefault();
                ViewData["order_id"] = es.id;
                return View(es);
            }
        }








    }
}