﻿using CMWebApp.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Attr;

namespace CMWebApp.Controllers
{
    public class MemberController : Controller
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;

        // GET: Member
        public ActionResult Cart()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetCartList()
        {
            using(db=new DB_ShiziEntities())
            {
                string uid = User.Identity.Name;
                var myCartCC = db.vCart.Where(x => x.custID == uid && x.isRemove == false && x.typ == "cart").Count();
                var es = db.vCart.Where(x => x.custID == uid && x.isRemove == false && x.typ == "cart")
                    .Select(g => new
                    {
                        g.sn,
                        g.prodID,
                        g.count,
                        prod = db.Item.Where(x => x.Cost != null && x.SN == g.prodID)
                            .Select(p => new
                            {
                                p.Title,
                                p.BgImageUrl,
                                p.Typ,
                                p.Guid,
                                p.Cost,
                                img = db.AttachFile.Where(x => x.HookID == p.Guid)
                                        .Select(m => new
                                        {
                                            m.FileNameNew,
                                            m.HookID,
                                            m.Guid,
                                            m.Title,
                                            m.UpdateTime
                                        })
                                        .OrderBy(x => x.UpdateTime)
                                        .FirstOrDefault()
                            }).FirstOrDefault(),
                        g.insertDate
                    })
                    .OrderBy(x=>x.insertDate)
                    .ToList();

                return Json(new {
                    cart = es,
                    myCartCC = myCartCC
                });
            }

        }

        /// <summary>
        /// 用於訂單成功之後的購物明細資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetOrderMList(string orderid)
        {
            using (db = new DB_ShiziEntities())
            {
                string uid = User.Identity.Name;
                var myCartCC = db.vCart.Where(x => x.custID == uid && x.isRemove == false && x.typ == "cart").Count();
                var es = db.vCart.Where(x => x.custID == uid && x.isRemove == false && x.typ == "order" && x.orderid == orderid)
                    .Select(g => new
                    {
                        g.sn,
                        g.prodID,
                        g.count,
                        prod = db.Item.Where(x => x.Cost != null && x.SN == g.prodID)
                            .Select(p => new
                            {
                                p.Title,
                                p.BgImageUrl,
                                p.Typ,
                                p.Guid,
                                p.Cost,
                                img = db.AttachFile.Where(x => x.HookID == p.Guid)
                                        .Select(m => new
                                        {
                                            m.FileNameNew,
                                            m.HookID,
                                            m.Guid,
                                            m.Title,
                                            m.UpdateTime
                                        })
                                        .OrderBy(x => x.UpdateTime)
                                        .FirstOrDefault()
                            }).FirstOrDefault(),
                        g.insertDate
                    })
                    .OrderBy(x => x.insertDate)
                    .ToList();

                return Json(new
                {
                    cart = es,
                    myCartCC = myCartCC
                });
            }

        }


        [HttpPost]
        public JsonResult AddItemCC(int CartSN) {
            using (db = new DB_ShiziEntities())
            {
                string uid = User.Identity.Name;
                var e = db.Cart.Where(x => x.sn == CartSN && x.custID==uid && x.typ=="cart").SingleOrDefault();
                e.count += 1;
                e.updateTime = DateTime.Now;
                db.SaveChanges();

                return this.GetCartList();
            }
        }

        [HttpPost]
        public JsonResult ReduceItemCC(int CartSN)
        {
            using (db = new DB_ShiziEntities())
            {
                string uid = User.Identity.Name;
                var e = db.Cart.Where(x => x.sn == CartSN && x.custID == uid && x.typ=="cart").SingleOrDefault();
                e.count -= 1;
                e.count = (e.count < 0) ? 0 : e.count;
                e.updateTime = DateTime.Now;
                db.SaveChanges();

                return this.GetCartList();
            }
        }

        [Big5CharsetAttribute]
        public ActionResult CreateOrder() {
            return View();
        }

    }
}