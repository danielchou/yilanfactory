﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using CMWebApp.Models;
using CMWebApp.Attr;

namespace CMWebApp.Controllers
{
    [AllowAnonymous]
    [AllowCrossSiteJson]
    public partial class HomeController : Controller
    {

        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;

        public class ContentTreeNode
        {
            public int SN { get; set; }
            public int? ParentSN { get; set; }
            public string GUID { get; set; }
            public string BigTitle { get; set; }
            public int Level { get; set; }
            public string Sort { get; set; }
        }

       

        [HttpPost]
        public JsonResult GetContentsLV1()
        {
            using (db = new DB_ShiziEntities())
            {
                using (db = new DB_ShiziEntities())
                {
                    var es = db.Database.SqlQuery<ContentTreeNode>("select * from vContentTreeNode where level =1 and sn not in (6,7) order by sort")
                        .Select(g => new
                        {
                            g.BigTitle,
                            g.GUID,
                            g.Level,
                            g.SN,
                            g.Sort,
                            Body = db.Content.Where(x => x.SN == g.SN).SingleOrDefault().Body
                        }).ToList();
                    return Json(es);
                }
            }

        }

        [HttpPost]
        public JsonResult GetActivity()
        {
            using (db = new DB_ShiziEntities())
            {
                using (db = new DB_ShiziEntities())
                {
                    string sql = "select * from vContentTreeNode where parentSN =10 order by sort";
                    var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                        .Select(g => new
                        {
                            g.BigTitle,
                            g.GUID,
                            g.Level,
                            g.SN,
                            Content = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                                        .Select(c => new {
                                            c.Head,
                                            c.Body,
                                            c.Dscr,
                                            c.RouteName,
                                            c.Guid,
                                            c.BigTitle,
                                            items = db.Item.Where(x => x.ContentSN == c.SN && x.IsRemove != "Y")
                                                .Select(t => new {
                                                    t.Guid,
                                                    t.Dscr,
                                                    t.Subtitle,
                                                    t.Title,
                                                    t.Name,
                                                    t.Sort,
                                                    t.BgImageUrl,
                                                    firstImg = db.AttachFile.Where(x => x.HookID == t.Guid).OrderBy(x => x.UpdateTime).FirstOrDefault()
                                                })
                                                .OrderBy(x=>x.Sort)
                                                .ToList()
                                        })
                                        .SingleOrDefault(),
                            g.Sort,
                        }).ToList();
                    return Json(es);
                }
            }

        }



        public ActionResult contextDetail(string Guid)
        {
            using (db = new DB_ShiziEntities())
            {
                Guid = Guid.Replace("gid-", "");
                var e = db.Item.Where(x => x.Guid == Guid)
                            .OrderByDescending(x => x.SN)
                            .FirstOrDefault();

                if (e == null)
                {
                    ViewBag.Title = "沒有此Content內容";
                }
                else
                {
                    ViewBag.Title = e.Title;
                    //ViewBag.ContentGuid = e.Guid;
                    ViewBag.ContentSN = e.SN;
                    ViewBag.ItemGuid = Guid;
                }
                return View("details1");
            }
        }
        /// <summary>
        /// 通用讀取Content專用
        /// </summary>
        /// <param name="RouteName"></param>
        /// <returns></returns>
        public ActionResult Context(string RouteName, string id)
        {
            using (db = new DB_ShiziEntities())
            {
                if(RouteName == null) { RouteName = "mainland"; }

                //if (RouteName.Substring(0, 3) == "gid") {
                //    return RedirectToAction("contextDetail", new { Guid = RouteName });
                //}

                var e = db.Content.Where(x => x.RouteName == RouteName).OrderByDescending(x => x.SN).FirstOrDefault();
                var store = db.Store.Where(x => x.codeName == id).SingleOrDefault();
               

                if (e == null)
                {
                    ViewBag.Title = "沒有此Content內容";
                }else
                {

                    ViewBag.Title = (e.SN ==126 || e.SN ==129 || e.SN == 131)? store.name : e.BigTitle;
                    

                    ViewBag.ContentGuid = e.Guid;
                    ViewBag.ContentSN = e.SN;
                    ViewBag.id = id; //這邊可以客製化區別
                    ViewBag.PlaceId = (store == null)? "": store.PlaceID;
                    ViewBag.Keywords = (store == null) ? "" : store.keyword;
                    ViewBag.Description = (store == null) ? "" : store.description;
                    ViewBag.StoreColor = (store == null) ? "" : store.color;
                }

                if (RouteName == "DM")
                {
                    var content = db.Content.Where(x => x.RouteName == store.codeName).SingleOrDefault();
                    var item = db.Item.Where(x => x.ContentSN == content.SN && x.IsRemove !="Y").OrderByDescending(x => x.Sort).FirstOrDefault();
                    ViewBag.KeySubTitle = (e.SN == 126 || e.SN == 129 || e.SN == 131) ? item.Title : "";

                    if (item == null)
                    {
                        return View("PleaseWait");
                    }

                    var ff = db.AttachFile.Where(x => x.HookID == item.Guid && x.IsMaster == "Y").FirstOrDefault();
                    if (ff != null)
                    {
                        string[] arr = ff.Dscr.Split(new string[] { "|" }, StringSplitOptions.None);
                        if (arr.Length > 0)
                        {
                            if(arr[0] == "DM1" || arr[0] == "DM2" || arr[0] == "DM3")
                            {
                                return View(arr[0]);
                            }
                            else
                            {
                                return View("PleaseWait");
                            }
                        }
                    }
                }


                return View(e.RouteName);
            }
        }

        public ActionResult TaipeiSchool()
        {
            return View();
        }

        public ActionResult Cart()
        {

            return View();
        }


        public ActionResult WeeklyMain()
        {
            return View();
        }

        public ActionResult Weekly() {
            return View();
        }

        public ActionResult studentweeklyshow()
        {
            return View();
        }
        public ActionResult tutorial()
        {
            return View();
        }
        public ActionResult purchase()
        {
            return View();
        }
        public ActionResult download()
        {
            return View();
        }

       


    }
}