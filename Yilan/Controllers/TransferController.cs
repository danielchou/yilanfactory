﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMWebApp.Controllers
{
    [AllowAnonymous]
    public class TransferController : Controller
    {
        // GET: Transfer
        public ActionResult science_60(string videoclass)
        {
            ViewData["videoclass"] = videoclass;
            return View();
        }

        public ActionResult story(string videoclass)
        {
            ViewData["videoclass"] = videoclass;
            return View();
        }

        public ActionResult ARAPP_Android(string videoclass)
        {
            return View();
        }

        public ActionResult ARAPP_IOS(string videoclass)
        {
            return View();
        }

        public ActionResult ARAPP_video(string videoclass)
        {
            return View();
        }

        public ActionResult ReadPen(string Params, string A, string B,  string C)
        {
            return View();

        }

        public ActionResult FullPHP(string file)
        {
            ViewData["phpFile"] = file;
            return View();
        }

        public ActionResult ReadPen_A(string A, string file, 
            string func, string Class, string Srange, string Erange,
            string fullfilename)
        {
            ViewData["A"] = A;
            ViewData["phpFile"] = file;

            ViewData["func"] = (func==null) ? "" : "func=" + func;
            ViewData["Class"] = (Class == null) ? "" : "class=" + Class;
            ViewData["Srange"] = (Srange == null) ? "" : "Srange=" + Srange;
            ViewData["Erange"] = (Erange == null) ? "" : "Erange=" + Erange;
            ViewData["fullfilename"] = (fullfilename == null) ? "" : "fullfilename=" + fullfilename;
            return View();
        }


        //測試用
        public ActionResult HTML(string page)
        {
            //編碼參考這篇文章: https://dotblogs.com.tw/PowerHammer/archive/2008/03/24/2185.aspx
            //%u5468%u80B2%u6B63  => 周育正
            string source = "周育正";
            string target = this.Utf8ToBig5(source);
            return View();
        }

        public string Utf8ToBig5(string source)
        {
            Byte[] strBytes = System.Text.Encoding.Default.GetBytes(source);
            return System.Text.Encoding.GetEncoding(950).GetString(strBytes);
        }
    }
}