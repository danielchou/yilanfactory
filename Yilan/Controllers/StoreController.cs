﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Models;

using AutoMapper;
using NLog;

namespace CMWebApp.Controllers
{
    
    public class StoreController : Controller
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;


        [AllowAnonymous]
        // GET: Store
        public ActionResult Index(int? ContentSN)
        {
            using(db=new DB_ShiziEntities())
            {
                if (ContentSN != null)
                {
                    var e = db.Store.Where(x => x.contentSN == ContentSN).SingleOrDefault();

                    ViewData["StoreName"] = e.name;
                    ViewData["ContentSN"] = ContentSN;
                }
                return View();
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ReadStore(Store m)
        {
            using(db=new DB_ShiziEntities())
            {
                var e = db.Store.Where(x => x.name == m.name && x.owner == User.Identity.Name).SingleOrDefault();
                return Json(e);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ReadStoreDetail(string CodeName)
        {
            using (db = new DB_ShiziEntities())
            {
                var e = db.Store.Where(x => x.codeName == CodeName)
                            .Select(g => new {
                                g.codeName,
                                g.color,
                                g.name,
                                items = db.StoreDetail.Where(x => x.storeID == g.id).ToList(),

                            })
                            .SingleOrDefault();
                return Json(e);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ReadStoreByContentSN(int contentSN)
        {
            using (db = new DB_ShiziEntities())
            {
                var e = db.Store.Where(x => x.contentSN == contentSN).SingleOrDefault();
                return Json(e);
            }
        }


        [AllowAnonymous]
        [HttpPost]
        public string EditStore(Store m)
        {
           using(db=new DB_ShiziEntities())
           {
                string errmsg = "";
                try
                {
                    if (m.id == 0)
                    {
                        //同時建立在網頁內容架構內，以方便可以讓協會人員統一管理行銷活動。
                        Guid guid = System.Guid.NewGuid();
                        Content newCTT = new Models.Content()
                        {
                            BigTitle = m.name,
                            Creator = User.Identity.Name,
                            InsertDate = DateTime.Now,
                            ParentSN = 95
                        };
                        newCTT.Guid = guid.ToString();
                        db.Content.Add(newCTT);
                        db.SaveChanges();

                        //建立商家資料，把content SN抓來記錄相關聯
                        m.createDate = DateTime.Now;
                        Guid g2 = System.Guid.NewGuid();
                        m.guid = g2.ToString();
                        var maxContentSN = db.Content.Max(x => x.SN);
                        m.contentSN = maxContentSN;
                        db.Store.Add(m);
                    }
                    else
                    {
                        var e = db.Store.Where(x => x.id == m.id).SingleOrDefault();
                        //有了AutoMapper可少寫很多code
                        Mapper.Initialize(cfg => cfg.CreateMap<Store, Store>()
                                               .ForMember(x => x.createDate, opt =>   opt.Ignore()));
                        Mapper.Map(m, e);
                    }
                    db.SaveChanges();
                    errmsg = "儲存成功";
                }
                catch (Exception ex)
                {
                    errmsg=ex.ToString();
                }
                return errmsg;
           }
        }

        /// <summary>
        /// 收藏到我的最愛的商店
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult SaveMyFavoriteStore(int storeId)
        {
            var user = User.Identity.Name;

            using ( db =new DB_ShiziEntities())
            {
                string msg = "";
                try
                {
                    var e = db.TripRouting.Where(x => x.userId == user && x.tripID == 0 && x.storeID == storeId).AsQueryable();
                    int? maxSort = db.TripRouting.Where(x => x.userId == user && x.tripID == 0).Max(x=>x.sortNo); //算出排序大小
                    maxSort = (maxSort == null) ? 1 : maxSort +1;

                    if (e.Count()==0)
                    {

                        TripRouting tr = new TripRouting()
                        {
                            createDate = DateTime.Now,
                            userId = user,
                            isVisible = "Y",
                            tripID = 0,
                            sortNo = maxSort.Value,
                            storeID = storeId,
                            typ = "favor"
                        };

                        db.TripRouting.Add(tr);
                        msg = "加入我的最愛成功";
                    }
                    else
                    {
                        var e2 = e.SingleOrDefault();
                        db.TripRouting.Remove(e2);
                    }
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    msg = ex.Message.ToString();
                }
                return Json(msg);
            }
        }
    }
}