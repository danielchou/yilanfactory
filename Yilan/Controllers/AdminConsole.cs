﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Attr;
using CMWebApp.Models;
using System.IO;
using NLog;

namespace CMWebApp.Controllers
{
    [AllowAnonymous]
    public partial class AdminController : Controller
    {

        public ActionResult MngMember() {
            using (db = new DB_ShiziEntities())
            {
                return View();
            }
        }


        [HttpPost]
        public JsonResult ShowMembers()
        {
            using (db = new DB_ShiziEntities())
            {
                var es = db.AspNetUsers
                    .Select(g => new {
                        g.UserName,
                        g.PhoneNumber,
                        g.Email,
                        g.EmailConfirmed,
                        g.AccessFailedCount,
                        provider = db.AspNetUserLogins.Where(x => x.UserId == g.Id).FirstOrDefault().LoginProvider
                    })
                    .OrderByDescending(x => x.EmailConfirmed)
                    .ToList();


                return Json(es);
            }
        }



        [HttpPost]
        public JsonResult GetCustCartFavorCC(string custID)
        {
            using(db=new DB_ShiziEntities())
            {
                var es = db.Cart.Where(x => x.custID == custID).AsQueryable();
                var iFavor = es.Where(x => x.typ == "favor").Count();
                var iCart = es.Count() - iFavor;

                return Json(new
                {
                    iFavor = iFavor,
                    iCart = iCart
                });

            }
        }

        [HttpPost]
        public JsonResult AddCartFavor(Cart cart)
        {
            using (db = new DB_ShiziEntities())
            {
                var e = db.Cart.Where(x => x.prodID == cart.prodID && x.custID == cart.custID && x.typ == cart.typ).SingleOrDefault();

                if (e == null)
                {
                    int MaxSN = 1;
                    if (db.Cart.Count() > 0)
                    {
                        MaxSN = db.Cart.Max(x => x.sn) + 1;
                    }
                    if(cart.typ=="cart") { cart.count = 1; }
                    cart.sn = MaxSN;
                    cart.insertDate = DateTime.Now;
                    db.Cart.Add(cart);
                }
                else
                {
                    e.isRemove = false;
                    e.updateTime = DateTime.Now;
                }
                db.SaveChanges();

                return GetCustCartFavorCC(cart.custID);
            }
        }


        [HttpPost]
        public JsonResult RemoveCartFavor(Cart cart)
        {
            using (db = new DB_ShiziEntities())
            {
                var e = db.Cart.Where(x => x.custID == cart.custID && x.prodID ==cart.prodID && x.typ==cart.typ).SingleOrDefault();

                if (e != null)
                {
                    e.updateTime = DateTime.Now;
                    e.isRemove = true;
                    db.SaveChanges();
                }
                return GetCustCartFavorCC(cart.custID);
            }
        }

        [HttpPost]
        public JsonResult GetCustomers() {
            using (db = new DB_ShiziEntities()) {
                var es = db.AspNetUsers.Select(g => new
                {
                    g.Id,
                    g.UserName,
                    g.Email,
                    g.EmailConfirmed
                })
                .ToList();

                return Json(es);
            }
        }

    }
}