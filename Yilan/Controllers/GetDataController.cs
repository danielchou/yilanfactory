﻿using CMWebApp.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMWebApp.Controllers
{
    [AllowAnonymous]
    public partial class GetDataController : Controller
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;

        public class ContentTreeNode
        {
            public int SN { get; set; }
            public int? ParentSN { get; set; }
            public string GUID { get; set; }
            public string BigTitle { get; set; }
            public int Level { get; set; }
            public string Sort { get; set; }
            public string RoutePath { get; set; }
        }

        [HttpPost]
        public JsonResult GetSingleContent(int ContentSN)
        {
            int sn = ContentSN;

            using (db = new DB_ShiziEntities())
            {

                string sql = "select * from vContentTreeNode where SN ={0} order by sort";
                sql = string.Format(sql, ContentSN);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.Level,
                        g.RoutePath,
                        Parent = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                                    .Select(c => new
                                    {
                                        c.Head,
                                        c.Body,
                                        c.Dscr,
                                        c.RouteName,
                                        c.Guid,
                                        c.BigTitle,
                                        store = db.Store.Where(x => x.contentSN == c.SN).SingleOrDefault(),
                                        imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y")
                                                .Select(f => new
                                                {
                                                    f.Dscr,
                                                    f.FileNameNew,
                                                    f.Sort,
                                                }).OrderBy(x => x.Sort).ToList(),
                                        items = db.Item.Where(x => x.ContentSN == c.SN && x.IsRemove != "Y")
                                .Select(t => new
                                {
                                    t.Guid,
                                    t.Dscr,
                                    t.Subtitle,
                                    t.Title,
                                    t.Name,
                                    t.Typ,
                                    t.Cost,
                                    t.SN,
                                    t.BgImageUrl,
                                    t.Sort,
                                    firstImg = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y")
                                                .OrderBy(x => x.UpdateTime)
                                                .Select(f => new
                                                {
                                                    f.Dscr,
                                                    f.FileNameNew
                                                }).FirstOrDefault(),
                                    imgs = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y")
                                                .Select(f => new
                                                {
                                                    f.Dscr,
                                                    f.FileNameNew,
                                                    f.Sort,
                                                }).OrderBy(x => x.Sort).ToList()
                                }).OrderByDescending(x => x.Sort).ToList()

                                    }).SingleOrDefault(),
                        g.Sort,
                    })
                    .ToList();

                return Json(es);
            }
        }

       


        [HttpPost]
        public JsonResult GetItems(int contentSN)
        {
            using (db = new DB_ShiziEntities())
            {
                try
                {
                    var e = db.Item.Where(x => x.ContentSN == contentSN && x.IsRemove != "Y")
                        .Select(g => new
                        {
                            g.Title,
                            g.Dscr,
                            g.Guid,
                            g.Name,
                            g.SN,
                            g.Subtitle,
                            g.UpdateTime,
                            g.YoutubeUrl,
                            g.BgImageUrl,
                            g.ContentSN,
                            g.Cost,
                            g.Typ,
                            g.Creator,
                            imgs = db.AttachFile
                                .Where(a => a.HookID == g.Guid && a.IsRemove != "Y")
                                .OrderBy(x => x.Sort).ToList(),
                            g.InsertDate
                        })
                        .OrderByDescending(x => x.SN).ToList();

                    return Json(new
                    {
                        Items = e
                    });
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
            }
        }

        [HttpPost]
        public JsonResult GetSingleContentProd(int ContentSN, string uid)
        {
            int sn = ContentSN;

            using (db = new DB_ShiziEntities())
            {

                string sql = "select * from vContentTreeNode where SN ={0} order by sort";
                sql = string.Format(sql, ContentSN);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.Level,
                        g.RoutePath,
                        Parent = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                                    .Select(c => new {
                                        c.Head,
                                        c.Body,
                                        c.Dscr,
                                        c.RouteName,
                                        c.Guid,
                                        c.BigTitle,
     myCartCC = db.Cart.Where(x=>x.custID == uid && x.isRemove==false && x.typ=="cart").Count(),
     items = db.Item.Where(x => x.ContentSN == c.SN && x.IsRemove != "Y")
        .Select(t => new {
            t.Guid,
            t.Dscr,
            t.Subtitle,
            t.Title,
            t.Name,
            t.Typ,
            t.Cost,
            t.Cost2,
            t.Sort,
            t.SN,
            t.BgImageUrl,
            firstImg = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y")
                .OrderBy(x => x.UpdateTime)
                .Select(f => new {
                    f.Dscr,
                    f.FileNameNew
                }).FirstOrDefault(),
            isCart = db.Cart.Where(x => x.custID == uid && x.prodID == t.SN && x.typ == "cart" && x.isRemove==false).Count(),
            isFavor = db.Cart.Where(x => x.custID == uid && x.prodID == t.SN && x.typ == "favor" && x.isRemove==false).Count(),
            iFavorsCC = db.Cart.Where(x=>x.prodID ==t.SN && x.typ=="favor").Count()
        })
        .OrderBy(x=>x.Sort)
        .ToList()
                                    })
                                    .SingleOrDefault(),
                        g.Sort,
                    }).ToList();

                return Json(es);
            }
        }


        [HttpPost]
        public JsonResult GetSingleContentWithChild(int ContentSN)
        {
            int sn = ContentSN;

            using (db = new DB_ShiziEntities())
            {

                string sql = "select * from vContentTreeNode where SN ={0} order by sort";
                sql = string.Format(sql, ContentSN);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.Level,
                        g.Sort,
                        g.SN,
                        g.RoutePath,
                        Parent = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                                    .Select(c => new {
                                        c.Head,
                                        c.Body,
                                        c.Dscr,
                                        c.RouteName,
                                        c.Guid,
                                        c.BigTitle,
                                        imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y").ToList(),

                                    })
                                    .SingleOrDefault(),
                        Children = db.Content.Where(x => x.ParentSN == g.SN && x.IsRemove != "Y")
                                    .Select(c => new {
                                        c.Head,
                                        c.Body,
                                        c.Dscr,
                                        c.RouteName,
                                        c.Guid,
                                        c.SN,
                                        c.BigTitle,
                                        c.SubTitle,
                                        c.SortNo,
                                        imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y").ToList()
                                    }).OrderBy(x => x.SortNo).ToList()

                    }).ToList();

                return Json(es);
            }
        }


        [HttpPost]
        public JsonResult GetSingleContentWithChildItems(int ContentSN)
        {
            int sn = ContentSN;

            using (db = new DB_ShiziEntities())
            {

                string sql = "select * from vContentTreeNode where SN ={0} order by sort";
                sql = string.Format(sql, ContentSN);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.Level,
                        g.Sort,
                        g.SN,
                        g.RoutePath,
                        Parent = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                                    .Select(c => new {
                                        c.Head,
                                        c.Body,
                                        c.Dscr,
                                        c.RouteName,
                                        c.Guid,
                                        c.BigTitle,
                                        imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y").ToList(),

                                    })
                                    .SingleOrDefault(),
Children = db.Content.Where(x => x.ParentSN == g.SN && x.IsRemove != "Y")
    .Select(c => new {
        c.Head,
        c.Body,
        c.Dscr,
        c.RouteName,
        c.Guid,
        c.SN,
        c.BigTitle,
        c.SubTitle,
        items = db.Item.Where(x=>x.ContentSN == c.SN)
            .Select(t=>new
            {
                t.ContentSN,
                t.Dscr,
                t.Guid,
                t.Name,
                t.Subtitle,
                t.Title,
                t.YoutubeUrl,
                itemImgs= db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y").ToList()
            })
            .ToList(),
        imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y").ToList()
    }).OrderBy(x => x.SN).ToList()

                    }).ToList();

                return Json(es);
            }
        }

        [HttpPost]
        public JsonResult GetSingleItem(string ItemGUID)
        {

            using (db = new DB_ShiziEntities())
            {
                int ContentSN = db.Item.Where(x => x.Guid == ItemGUID).SingleOrDefault().ContentSN;

                string sql = "select * from vContentTreeNode where SN ={0} order by sort";
                sql = string.Format(sql, ContentSN);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.Level,
                        g.Sort,
                        g.SN,
                        g.RoutePath,
                        Parent = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                            .Select(c => new {
                                c.Head,
                                c.Body,
                                c.Dscr,
                                c.RouteName,
                                c.Guid,
                                c.BigTitle,
                                //imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y").ToList(),
                            })
                            .SingleOrDefault(),
                        Item = db.Item.Where(x => x.Guid == ItemGUID && x.IsRemove != "Y")
                            .Select(t => new
                            {
                                t.InsertDate,
                                t.UpdateTime,
                                t.BgImageUrl,
                                t.Dscr,
                                t.Name,
                                t.Subtitle,
                                t.Title,
                                imgs = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y").ToList(),
                                t.YoutubeUrl
                            }).SingleOrDefault()
                    }).SingleOrDefault();

                return Json(es);
            }
        }


        [HttpPost]
        public JsonResult ShowMemberCartCC() {
            using(db=new DB_ShiziEntities())
            {
                string uid = User.Identity.Name;
                int cc = 0;
                if (uid != "")
                {
                    cc = db.vCart.Where(x => x.custID == uid && x.typ == "cart" && x.isRemove == false).Count();
                }

                return Json(new {
                    myCartCC = cc
                });

            }
        }


        [HttpPost]
        public JsonResult GetFooterRouteList()
        {
            using (db = new DB_ShiziEntities())
            {
                //string sql0 = "select * from vContentTreeNode where level in (1) order by sort";
                string sql = "select * from vContentTreeNode where level in (1, 2) order by sort";
                sql = string.Format(sql);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.BigTitle,
                        g.SN,
                        g.ParentSN,
                        RouteName = db.Content.Where(x => x.SN == g.SN).FirstOrDefault().RouteName,
                        g.Level
                    })
                    .ToList();

                //var es2= db.Database.SqlQuery<ContentTreeNode>(sql0)
                //    .Select(g=> new
                //    {
                //        g.BigTitle,
                //        g.SN,
                //        RouteName = db.Content.Where(x => x.SN == g.SN).FirstOrDefault().RouteName,
                //        g.Level,
                //        Children= es.Where(x=>x.SN == g.ParentSN).ToList()
                //    })
                //    .ToList();



            return Json(es);
            }
        }

        [HttpPost]
        public JsonResult getMemberPrivateData() {
            using(db=new DB_ShiziEntities())
            {
                string user = User.Identity.Name;
                //我最愛的商品
                var favors = db.vCart.Where(x => x.custID == user && x.typ=="favor" && x.isRemove==false)
                    .Select(g => new
                    {
                        g.Cost,
                        g.Cost2,
                        g.count,
                        g.ProdGuid,
                        g.prodID,
                        g.ProdTyp,
                        g.title,
                        g.subtitle,
                        g.firstImg,
                        g.typ,
                        g.updateTime,
                        g.insertDate,
                        g.sort
                    })
                    .OrderBy(x => x.sort)
                    .ToList();

                //相關權益說明
                var Rights = db.Content.Where(x => x.SN == 65 || x.SN == 74)
                    .Select(g => new
                    {
                        g.BigTitle,
                        g.Body,
                        g.UpdateTime,
                        g.SN
                    })
                    .ToList();

                return Json(new {
                    favors=favors,
                    Rights = Rights
                });
            }
        }


        [HttpPost]
        public JsonResult GetSingleContentDWURL(int ContentSN)
        {
            int sn = ContentSN;

            using (db = new DB_ShiziEntities())
            {

                string sql = "select * from vContentTreeNode where SN ={0} order by sort";
                sql = string.Format(sql, ContentSN);
                var es = db.Database.SqlQuery<ContentTreeNode>(sql)
                    .Select(g => new
                    {
                        g.Level,
                        g.RoutePath,
                        Parent = db.Content.Where(x => x.SN == g.SN && x.IsRemove != "Y")
                                .Select(c => new
                                {
                                    c.Head,
                                    c.Body,
                                    c.Dscr,
                                    c.RouteName,
                                    c.Guid,
                                    c.BigTitle
                                }).FirstOrDefault(),
                        DwnURLTyp = db.downloaditem.Where(x=>x.isRemove==false)
                                .GroupBy(x => new { x.typ })
            .Select(a => new
            {
                a.Key.typ,
                typ2s = db.downloaditem.Where(x=>x.typ== a.Key.typ && x.isRemove==false)
                    .GroupBy(x=> new { x.typ2})
                    .Select(b=>new {
                        b.Key.typ2,
                        CC=b.Count()
                    })
                    .OrderBy(x=>x.typ2)
            }).OrderBy(x => x.typ).ToList()
                    }).ToList();
                          
                return Json(es);
            }
        }


        [HttpPost]
        public JsonResult getDwnList(string typ2)
        {
            using (db = new DB_ShiziEntities())
            {
                var es = db.downloaditem.Where(x => x.typ2 == typ2 && x.isRemove==false)
                    .OrderByDescending(x => x.name)
                    .ToList();
                return Json(es);
            }
        }

        [HttpPost]
        public JsonResult getProductTyp73() {
            using(db=new DB_ShiziEntities())
            {
                var es = db.Item.Where(x => x.ContentSN == 73 && x.IsRemove == null)
                    .GroupBy(x => new { x.Typ })
                    .Select(g => new
                    {
                        g.Key.Typ,
                        CC=g.Count()
                    })
                    .ToList();

                return Json(es);
            }
        }


        [HttpPost]
        public void saveDownloadItem(downloaditem downloaditem)
        {
            using (db = new DB_ShiziEntities())
            {
                var m = downloaditem;
                if (m.sn == 0)
                {
                    int MaxSN = db.downloaditem.Max(x => x.sn) + 1;
                    m.sn = MaxSN;
                    m.insertDate = DateTime.Now;
                    m.isRemove = false;
                    db.downloaditem.Add(m);
                }else
                {
                    var e = db.downloaditem.Where(x => x.sn == m.sn).SingleOrDefault();
                    e.name = m.name;
                    e.typ = m.typ;
                    e.typ2 = m.typ2;
                    e.imgurl = m.imgurl;
                    e.dwurl = m.dwurl;
                    e.insertDate = DateTime.Now;
                }
                db.SaveChanges();
            }
        }


        [HttpPost]
        public void DeleteDwnItem(downloaditem downloaditem)
        {
            using (db = new DB_ShiziEntities())
            {
                var m = downloaditem;
                var e = db.downloaditem.Where(x => x.sn == m.sn).SingleOrDefault();
                e.isRemove = true;
                e.insertDate = DateTime.Now;
                db.SaveChanges(); 
            }
        }

        
    }
}