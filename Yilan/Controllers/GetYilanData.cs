﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Attr;
using CMWebApp.Models;
using System.IO;
using NLog;

namespace CMWebApp.Controllers
{
    public partial class GetDataController : Controller
    {
        [HttpPost]
        public JsonResult GetSingleStoreContent(string codeName)
        {
            using (db = new DB_ShiziEntities())
            {
                string user = User.Identity.Name;
                var _store = db.Store.Where(x => x.codeName == codeName).SingleOrDefault();
                //var tripRoutes = db.TripRouting.Where(x => x.userId == user && x.tripID == 0).AsQueryable();

                int contentSN = _store.contentSN.Value;
                int storeID = _store.id;

                var es = db.Content.Where(x => x.SN == contentSN && x.IsRemove != "Y")
                                    .Select(c => new
                                    {
                                        c.Head,
                                        c.Body,
                                        c.Dscr,
                                        c.RouteName,
                                        c.Guid,
                                        c.BigTitle,
                                        isFavoriteStore =db.TripRouting.Where(x=>x.userId ==User.Identity.Name && x.storeID == storeID).Count(),
                                        store = db.Store.Where(x => x.contentSN == c.SN).FirstOrDefault(),
                                        imgs = db.AttachFile.Where(x => x.HookID == c.Guid && x.IsRemove != "Y")
                                                .Select(f => new
                                                {
                                                    f.Dscr,
                                                    f.FileNameNew,
                                                    f.Sort,
                                                }).OrderBy(x => x.Sort).ToList(),
                                        items = db.Item.Where(x => x.ContentSN == c.SN && x.IsRemove != "Y")
                                            .Select(t => new
                                            {
                                                t.Guid,
                                                t.Dscr,
                                                t.Subtitle,
                                                t.Title,
                                                Name = t.Name,
                                                t.Typ,
                                                t.Cost,
                                                t.SN,
                                                t.BgImageUrl,
                                                t.Sort,
                                                t.startDate,
                                                t.endDate,
                                                firstImg = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y")
                                                            .OrderBy(x => x.UpdateTime)
                                                            .Select(f => new
                                                            {
                                                                f.Dscr,
                                                                f.FileNameNew
                                                            }).FirstOrDefault(),
                                                imgs = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y")
                                                            .Select(f => new
                                                            {
                                                                f.Dscr,
                                                                f.FileNameNew,
                                                                f.Sort,
                                                            }).OrderByDescending(x => x.Dscr).ToList()

                                            })
                                    }).FirstOrDefault();

                return Json(es);
            }
        }


        [HttpPost]
        public JsonResult GetDMContent(string codeName)
        {
            using (db = new DB_ShiziEntities())
            {
                string user = User.Identity.Name;
                var _store = db.Store.Where(x => x.codeName == codeName).SingleOrDefault();
                //var tripRoutes = db.TripRouting.Where(x => x.userId == user && x.tripID == 0).AsQueryable();

                int contentSN = _store.contentSN.Value;
                int storeID = _store.id;

                var es = db.Content.Where(x => x.SN == contentSN && x.IsRemove != "Y")
    .Select(c => new
    {
        store = db.Store.Where(x => x.contentSN == c.SN).FirstOrDefault(),
        items = db.Item.Where(x => x.ContentSN == c.SN && x.IsRemove != "Y")
            .Select(t => new
            {
                t.Guid,
                t.Dscr,
                t.Subtitle,
                t.Title,
                Name = t.Name,
                t.Typ,
                t.Cost,
                t.SN,
                t.BgImageUrl,
                t.Sort,
                t.startDate,
                t.endDate,
                dm1Img = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y" && x.Dscr.Contains("DM1|"))
                            .OrderBy(x => x.UpdateTime)
                            .Select(f => new
                            {
                                f.Dscr,
                                f.FileNameNew
                            }).FirstOrDefault(),
                dm2Img = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y" && x.Dscr.Contains("DM2|"))
                            .OrderBy(x => x.UpdateTime)
                            .Select(f => new
                            {
                                f.Dscr,
                                f.FileNameNew
                            }).FirstOrDefault(),
                dm3Img = db.AttachFile.Where(x => x.HookID == t.Guid && x.IsRemove != "Y" && x.Dscr.Contains("DM3|"))
                            .OrderBy(x => x.UpdateTime)
                            .Select(f => new
                            {
                                f.Dscr,
                                f.FileNameNew
                            }).FirstOrDefault(),

            }).OrderByDescending(x => x.Sort).FirstOrDefault()
    }).FirstOrDefault();

                return Json(es);
            }
        }



        /// <summary>
        /// 讀取含有地理位置的商店資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ReadStoresMapXY()
        {
            using (db = new DB_ShiziEntities())
            {
                var es = db.Store.Select(g => new
                {
                    g.name,
                    g.codeName,
                    g.typ,
                    x = g.X,
                    y = g.Y,
                    g.id,
                    g.contentSN,
                })
                .OrderBy(x => x.typ)
                .ToList();

                return Json(es);
            }
        }


        [HttpPost]
        public JsonResult GetRightItemUnderSomeContent(int ContentSN)
        {

            using (db = new DB_ShiziEntities())
            {
                var es = (from a in db.Content
                          join b in db.Item on a.SN equals b.ContentSN
                          where a.ParentSN == ContentSN && b.IsRemove != "Y" && a.IsRemove != "Y"
                          select new vmItems
                          {
                              Guid = b.Guid,
                              BgColor = b.BgColor,
                              BgImageUrl = b.BgImageUrl,
                              ContentSN = b.ContentSN,
                              endDate = b.endDate,
                              startDate = b.startDate,
                              Name = b.Name,
                              Dscr = b.Dscr,
                              Sort = b.Sort,
                              Title = b.Title,
                              Subtitle = b.Subtitle,
                              Typ = b.Typ,
                              Cost = b.Cost,
                              Cost2 = b.Cost2,
                              UpdateTime = b.UpdateTime,
                              imgs = db.AttachFile.Where(x => x.HookID == b.Guid && x.IsRemove != "Y").ToList()
                          })
                          .ToList();

                return Json(es);
            }
        }

        [HttpPost]
        public JsonResult ReadMarketingDataAll(int shared, int individual, DateTime? dt, string keyword)
        {
            using (db = new DB_ShiziEntities())
            {
                return Json(new
                {
                    shared = this.ReadMarketingData(shared, dt, keyword),
                    individual = this.ReadMarketingData(individual, dt, keyword),
                });
            }
        }


        /// <summary>
        /// 抓取各家優惠
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ReadMarketingData(int RootContentSN, DateTime? dt, string keyword)
        {
            using(db=new DB_ShiziEntities())
            {
                
                string sql = @"select vc.ParentSN , a.*
                                from item as a
                                join (Select max(startDate) as maxStartDate, ContentSN, count(*) as cc
                                from item
                                where isRemove is null and endDate >= getdate()
                                group by ContentSN) as b
                                on a.startDate = b.maxStartDate and a.ContentSN = b.ContentSN
                                join vContentTreeNode as vc on a.ContentSN = vc.SN 
                                where vc.ParentSN={0}";

                sql = string.Format(sql, RootContentSN);
                
                var es1 = db.Database.SqlQuery<vmItems>(sql);
                var es = from b in es1
                         select new vmItems()
                         {
                             mainImage = db.AttachFile
                                            .Where(x => x.HookID == b.Guid && x.IsRemove!="Y")
                                            .Select(g=>new vmImage()
                                            {
                                                Guid = g.Guid,
                                                Dscr = g.Dscr,
                                                FileNameNew = g.FileNameNew,
                                                Title = g.Title
                                            }).FirstOrDefault(),
                             Title = b.Title,
                             Name = b.Name,
                             startDate = b.startDate,
                             endDate = b.endDate,
                             BgColor = b.BgColor,

                             store = db.Store.Where(x => x.contentSN == b.ContentSN)
                                    .Select(n => new vmStore1()
                                    {
                                        codeName = n.codeName,
                                        color = n.color,
                                        name = n.name
                                    }).FirstOrDefault(),
                             compoWording = b.Title + b.Name,
                         };

                es = es.Where(x => x.endDate.Value.CompareTo(DateTime.Now) >= 0);

                if(dt != null)
                {
                    es = es.Where(x => x.endDate.Value.CompareTo(dt) >= 0);
                }

                if (!string.IsNullOrWhiteSpace(keyword))
                {
                    es = es.Where(x=>x.compoWording.Contains(keyword) || x.store.name.Contains(keyword))
                                .OrderBy(x => x.startDate);
                }

                return Json(es.ToList());
            }
        }
    }
}