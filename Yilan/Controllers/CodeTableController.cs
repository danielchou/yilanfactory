﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Attr;
using CMWebApp.Models;
using NLog;
using System.IO;
using My.Common;

namespace Wressly2.Controllers
{
    [GetStoreNameAttribute]
    public class CodeTableController : Controller
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;
        
        // GET: CodeTable
        public ActionResult CTList(string TB, string Typ)
        {
            ViewData["TB"] = TB ?? "Dress";
            ViewData["Typ"] = Typ ?? "Color";
            return View();
        }

        [HttpPost]
        public JsonResult GetTBList() {
            string Store = ViewData["Store"].ToString();

            using(db=new DB_ShiziEntities()){
                
                var es = db.CodeTable.Where(x => x.Store == Store).AsQueryable();
                var tb = es.GroupBy(p => p.TB).Select(g => new { Name = g.Key, count = g.Count() }).ToList();

                return Json(tb);
            }
        }

        [HttpPost]
        public JsonResult GetTyp(string TB) {
            string Store = ViewData["Store"].ToString();

            using (db = new DB_ShiziEntities()) {
                var es = db.CodeTable.Where(x => x.Store == Store && x.TB==TB).AsQueryable();
                //var tb = es.GroupBy(p => p.TB).Select(g => new { Name = g.Key, count = g.Count() }).ToList();
                var typ = es.GroupBy(p => new { p.Typ })
                    .Select(g => new { 
                        g.Key.Typ,
                        count=g.Count()
                    }).ToList();

                return Json(typ);
            }
        }

        [HttpPost]
        public JsonResult GetCodeTableList(string TB, string Typ) {
            string Store = ViewData["Store"].ToString();

            using (db = new DB_ShiziEntities()) {

                var es = db.CodeTable.Where(x => x.Store == Store && x.TB == TB && x.Typ == Typ)
                    .Select(g => new { 
                        g.GID,
                        g.Icon,
                        g.Name,
                        g.IsEnable,
                        g.Sort,
                        g.Dscr,
                        g.UpdateTime
                    })
                    .OrderBy(x=>x.Sort)
                    .ToList();

                return Json(es);
            }
        }

        [HttpPost]
        public JsonResult GetDetails(string Guid) {
            string Store = ViewData["Store"].ToString();

            using (db = new DB_ShiziEntities()) {

                var e = db.CodeTable.Where(x => x.Store == Store && x.GID == Guid).SingleOrDefault();
                return Json(e);
            }
        }

        [HttpPost]
        public JsonResult SaveDetails(CodeTable CodeTable) {
            string Store = ViewData["Store"].ToString();
            string msg = "OK";
            CodeTable d = CodeTable;

            try {
                using (db = new DB_ShiziEntities()) {
                    if (d.GID == null) {

                        int MaxSortNum = db.CodeTable.Where(x => x.TB == d.TB && x.Typ == d.Typ && x.IsEnable == "Y" && x.Store == Store)
                                            .Select(g => new { g.GID, g.Sort }).Count() + 1;

                        d.GID = Guid.NewGuid().ToString("N").Substring(0, 9);
                        d.InsertDate = DateTime.Now;
                        d.UpdateTime = DateTime.Now;
                        d.IsEnable = "Y";
                        d.Sort = MaxSortNum;
                        d.Store = Store;
                        db.CodeTable.Add(d);
                        db.SaveChanges();
                    } else {
                        var e = db.CodeTable.Where(x => x.Store == Store && x.GID == d.GID).SingleOrDefault();
                        e.UpdateTime = DateTime.Now;
                        e.Name = d.Name;
                        e.Dscr = d.Dscr;
                        e.IsEnable = d.IsEnable;
                        db.SaveChanges();
                    }
                }
            } catch (Exception ex) {
                msg = Store + " \n " + d.Name + " \n " + ex.Message.ToString();
            }
            return Json(msg);
        }

        [HttpPost]
        public JsonResult DeleteCodeTable(string Guid) {
            string Store = ViewData["Store"].ToString();
            string msg = "";
            string TB = "", Typ = "";
            int i = 0;

            using (db = new DB_ShiziEntities()) {
                var e = db.CodeTable.Where(x => x.Store == Store && x.GID == Guid).SingleOrDefault();
                if (e != null) {
                    try{
                        TB = e.TB;
                        Typ = e.Typ;
                        //if (TB == "Dress" && Typ=="Vender") { i += db.Dress.Where(x => x.VenderID == e.SN && x.Company==Store).Count(); }
                        //if (TB == "Dress" && Typ == "Color") { i += db.Dress.Where(x => x.Color == e.SN.ToString() && x.Company == Store).Count(); }
                        //if (TB == "Dress" && Typ == "Typ") { i += db.Dress.Where(x => x.Typ == e.SN && x.Company == Store).Count(); }
                        //if (TB == "Dress" && Typ == "Hanger") { i += db.Dress.Where(x => x.Hanger == e.SN.ToString() && x.Company == Store).Count(); }
                        if (i > 0) {
                            return Json(new { 
                                code="CodeTable Used", 
                                cc=i});
                        } else {
                            db.CodeTable.Remove(e);
                            db.SaveChanges();
                            msg="Delete Success";
                        }
                    }
                    catch(Exception ex){
                        logger.Error(Guid.ToString()+","+ex.ToString());
                        msg=ex.ToString();
                    }
                }
                return Json(msg);
            }
        }



        [HttpPost]
        public JsonResult SaveSort(int oldIndex, int newIndex, string TB, string Typ)
        {
            string msg = "OK";
            int n1, n2;
            bool pullBackFlag = true;
            
            try
            {
                using (db = new DB_ShiziEntities())
                {
                    oldIndex += 1;
                    newIndex += 1;
                    pullBackFlag = (oldIndex < newIndex) ? true : false;

                    if (oldIndex < newIndex) {
                        n1 = oldIndex;
                        n2 = newIndex;
                    }
                    else
                    {
                        n1 = newIndex;
                        n2 = oldIndex;
                    }

                    var es = db.CodeTable.Where(x => x.TB == TB && x.Typ == Typ && x.IsEnable == "Y" 
                                        && x.Sort >= n1 && x.Sort <= n2).AsQueryable();

                    var e = es.Where(x => x.Sort == oldIndex).SingleOrDefault();
                    e.Sort = newIndex;
                    e.UpdateTime = DateTime.Now;

                    var es2 = es.Where(x => x.Sort != oldIndex).AsQueryable(); //取中間段
                    foreach (var f in es2)
                    {
                        f.Sort = (pullBackFlag) ? (f.Sort - 1) : (f.Sort + 1);
                        f.UpdateTime = DateTime.Now;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                msg = TB + " \n sorting problems." + Typ + " \n " + ex.Message.ToString();
            }
            return Json(msg);
        }

    }
}