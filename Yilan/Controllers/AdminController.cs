﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Attr;
using CMWebApp.Models;
using System.IO;
using NLog;
using System.Configuration;
using AutoMapper;

namespace CMWebApp.Controllers
{
    public partial class AdminController : Controller
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;

        // GET: Admin
        public ActionResult Index(string k)
        {
            ViewData["ContentGuid"] = (k == null) ? "noContentGuid" : k;

            var UserID = User.Identity.Name;
            string AdminUsers = ConfigurationManager.AppSettings["AdminUsers"].ToString();

            using (db = new DB_ShiziEntities())
            {

                if (AdminUsers.IndexOf(UserID) >= 0 && UserID != "")
                {
                    return View();
                }
                else
                {
                    var e = db.Store.Where(x => x.GoogleAccount == UserID).FirstOrDefault();
                    if (e == null || UserID == "")
                    {
                        return View("NotAdmin");
                    }
                    else
                    {
                        return View();
                    }
                }
            }

        }

        public class ContentTreeNode {
            public int SN { get; set; }
            public int? ParentSN { get; set; }
            public string GUID { get; set; }
            public string BigTitle { get; set; }
            public int Level { get; set; }
            public string Sort { get; set; }
        }

        [HttpPost]
        public JsonResult GetContentNodeList() {
            var UserID = User.Identity.Name;
            string AdminUsers = ConfigurationManager.AppSettings["AdminUsers"].ToString();

            using (db = new DB_ShiziEntities()) {


                if (AdminUsers.IndexOf(UserID)>=0 && UserID!="")
                {
                    //最大管理者
                    var es = db.Database.SqlQuery<ContentTreeNode>("select * from vContentTreeNode order by sort").ToList();
                    return Json(es);
                }
                else
                {
                    var e = db.Store.Where(x => x.GoogleAccount == UserID).FirstOrDefault();
                    if (e == null || UserID=="")
                    {
                        return Json(new { isAdminAccount = false });
                    }
                    else
                    {
                        int contentSN = e.contentSN.Value;
                        var es = db.Database.SqlQuery<ContentTreeNode>("select * from vContentTreeNode where SN = " + contentSN.ToString() + " order by sort").ToList();
                        return Json(es);
                    }
                }
            }
        }

        [HttpPost]
        public JsonResult SaveContent(Content NewContent) {
            using (db = new DB_ShiziEntities()) {
                int MaxSN = db.Content.Max(x => x.SN) +1;
                Guid guid = System.Guid.NewGuid();

                try {
                    if (NewContent.SN == 0) {
                        NewContent.SN = MaxSN;
                        NewContent.Guid = guid.ToString();
                        NewContent.InsertDate = DateTime.Now;
                        NewContent.Creator = User.Identity.Name;
                        db.Content.Add(NewContent);
                    } else {
                        var e = db.Content.Where(x => x.SN == NewContent.SN).SingleOrDefault();
                        e.RouteName = NewContent.RouteName;
                        e.Body = NewContent.Body;
                        e.BigTitle = NewContent.BigTitle;
                        e.SubTitle = NewContent.SubTitle;
                        e.UpdateTime = DateTime.Now;
                    }
                    db.SaveChanges();
                } 
                catch (Exception ex) 
                {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
                return Json("Success");
            }
        }


        [HttpPost]
        public JsonResult GetOneContent(string contentGUID) {
            using (db = new DB_ShiziEntities()) {
                try {
                    var e = db.Content.Where(x => x.Guid == contentGUID)
                        .Select(g => new
                        {
                            g.RouteName,
                            g.SN,
                            g.ParentSN,
                            g.BigTitle,
                            g.Body,
                            g.Creator,
                            g.Dscr,
                            g.EndDateTime,
                            g.Guid,
                            g.Head,
                            g.Note,
                            g.StartDateTime,
                            g.SubTitle,
                            imgs = db.AttachFile.Where(x => x.HookID == g.Guid && x.IsRemove!="Y").OrderByDescending(x=>x.Dscr).ToList(),
                            g.UpdateTime,
                            g.InsertDate
                        })
                        .SingleOrDefault();

                    return Json(new {
                        OneContent = e
                    });
                } catch (Exception ex) {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
            }
        }

        [HttpPost]
        public JsonResult GetStoreList()
        {
            using (db = new DB_ShiziEntities())
            {
                var es = db.Store.Select(g => new
                {
                    g.id,
                    g.name,
                    g.lat,
                    g.lng
                }).OrderBy(x => x.id).ToList();

                return Json(es);
            }
        }

        [HttpPost]
        public JsonResult GetItems(int contentSN)
        {
            using (db = new DB_ShiziEntities())
            {
                try
                {
                    var e = db.Item.Where(x => x.ContentSN == contentSN && x.IsRemove != "Y")
                        .Select(g => new
                        {
                            g.Title,
                            g.Dscr,
                            g.Guid,
                            g.Name,
                            g.SN,
                            g.Subtitle,
                            g.UpdateTime,
                            g.YoutubeUrl,
                            g.BgImageUrl,
                            g.ContentSN,
                            g.Cost,
                            g.Cost2,
                            g.startDate,
                            g.endDate,
                            g.BgColor,
                            g.Sort,
                            g.Typ,
                            g.Creator,
                            imgs = db.AttachFile
                                .Where(a => a.HookID == g.Guid && a.IsRemove!="Y")
                                .OrderBy(x=>x.Sort).ToList(),
                            g.InsertDate,
                            g.ActiveStores
                        })
                        .OrderBy(x => x.Sort).ToList();

                    return Json(new
                    {
                        Items = e
                    });
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
            }
        }

        [HttpPost]
        public JsonResult RemoveContent(string contentGUID) {
            using (db = new DB_ShiziEntities()) {
                try {
                    var e = db.Content.Where(x => x.Guid == contentGUID).SingleOrDefault();
                    e.IsRemove = "Y";
                    e.UpdateTime=DateTime.Now;
                    db.SaveChanges();
                    return Json(e);
                } catch (Exception ex) {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
            }
        }


        [HttpPost]
        public JsonResult SaveItem(Item NewItem) {
            using (db = new DB_ShiziEntities()) {


                try {
                    if (NewItem.SN == 0)
                    {
                        int MaxSN = db.Item.Max(x => x.SN) + 1;
                        MaxSN = (MaxSN == 1) ? 1 : MaxSN;
                        Guid guid = System.Guid.NewGuid();

                        NewItem.SN = MaxSN;
                        NewItem.Guid = guid.ToString().Replace("-", "").Substring(0, 20);
                        NewItem.InsertDate = DateTime.Now;
                        NewItem.Creator = User.Identity.Name;
                        db.Item.Add(NewItem);
                    } else {
                        var e = db.Item.Where(x => x.SN == NewItem.SN).SingleOrDefault();
                        Mapper.Initialize(cfg => cfg.CreateMap<Item, Item>()
                                               .ForMember(x => x.InsertDate, opt => opt.Ignore()));
                        Mapper.Map(NewItem, e);
                        e.UpdateTime = DateTime.Now;
                    }
                    db.SaveChanges();
                } catch (Exception ex) {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
                return Json("Success");
            }
        }

        [HttpPost]
        public JsonResult EditItem(int itemSN) {
            using (db = new DB_ShiziEntities())
            {
                try
                {
                    var e = db.Item.Where(x => x.SN == itemSN).SingleOrDefault();
                    return Json(e);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(ex.ToString()); 
                }
            }
        }

        [HttpPost]
        public JsonResult RemoveItem(int itemSN)
        {
            using (db = new DB_ShiziEntities())
            {
                try
                {
                    var e = db.Item.Where(x => x.SN == itemSN).SingleOrDefault();
                    e.IsRemove = "Y";
                    e.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                    return Json(e);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
            }
        }

        [HttpPost]
        public JsonResult GetItemsCC()
        {
            using (db = new DB_ShiziEntities())
            {
                try
                {
                    var es = db.Item.GroupBy(x => new { x.ContentSN })
                                .Select(g => new
                                {
                                    ContentSN = g.Key.ContentSN,
                                    CC = g.Count()
                                }).ToList();

                    return Json(es);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return Json(ex.ToString());
                }
            }

        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public void UploadFiles(IEnumerable<HttpPostedFileBase> files, int AttachFileSN, string HookID, string UploadTyp)
        {
            string Store = ConfigurationManager.AppSettings["StoreName"].ToString(), //ViewData["Store"].ToString(),
                    ServerPath = Server.MapPath("~/uploads/photo/" + Store),
                    fileName = "", extension = "", fileNameNew = "",
                    path = "", pathNew = "", guid = "";

            if (HookID == null)
            {
                logger.Warn("該圖片沒有綁定的主檔");
            }

            if (files != null)
            {
                foreach (var file in files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file.FileName); // extract only the fielname
                        extension = Path.GetExtension(file.FileName);
                        int fileSize = file.ContentLength / 1014;      //KByte
                        path = Path.Combine(ServerPath, fileName);  // TODO: need to define destination
                        Guid gid = System.Guid.NewGuid();
                        guid = gid.ToString();

                        if (!System.IO.Directory.Exists(ServerPath))
                        {
                            System.IO.Directory.CreateDirectory(ServerPath);
                        }
                        file.SaveAs(path);

                        //rename with appending guid.
                        //因為避免中文問題....全面改為hash code. 2015/7/31
                        //string fileNameNew =fileName.Replace(extName,"")+"-"+guid+extName;
                        fileNameNew = guid + extension;
                        pathNew = Path.Combine(ServerPath, fileNameNew);

                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Move(path, pathNew);   //Rename a file name.
                        }

                        this.EditAttachFile(AttachFileSN, Store, HookID, extension, UploadTyp, guid, fileName, fileNameNew, pathNew, fileSize);
                    }
                }
            }


        }


        public void EditAttachFile(int AttachFileSN, string Store, string HookID, string extName, string UploadTyp, string guid, string fileName, string fileNameNew, string pathNew, int fileSize)
        {
            try
            {
                using (db = new DB_ShiziEntities())
                {
                    if(AttachFileSN > 0) {
                        var e = db.AttachFile.Where(x => x.SN == AttachFileSN).SingleOrDefault();
                        e.FileExtend = extName;
                        e.Store = Store;
                        e.FileName = fileName;
                        e.FileNameNew = fileNameNew;
                        e.FileNameReal = guid + extName;
                        e.Path = pathNew;
                        e.FileSize = fileSize;
                        e.UpdateTime = DateTime.Now;
                        db.SaveChanges();
                    }
                    else {

                        AttachFile f = new AttachFile
                        {
                            HookID = HookID,
                            FileExtend = extName,
                            Store = Store,
                            // UploadTyp 通常是對應table, 
                            // e.g.: DressID,ItemID,ContentID, UserID...這樣可讓所有需要圖片的資料綁定
                            Typ = UploadTyp,
                            Guid = guid,
                            FileName = fileName,
                            FileNameNew = fileNameNew,          //rename file.
                            FileNameReal = guid + extName,     //using Guid File Name since 2015/7/31
                            Path = pathNew,
                            FileSize = fileSize,
                            UpdateTime = DateTime.Now,
                        };
                        db.AttachFile.Add(f);
                        db.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }


        [HttpPost]
        public void RemoveItemImage(string imageGuid) {
            using (db = new DB_ShiziEntities())
            {
                var e = db.AttachFile.Where(x => x.Guid == imageGuid).SingleOrDefault();
                if(e != null)
                {
                    e.IsRemove = "Y";
                    e.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }

        [HttpPost]
        public void EditDscrItemImage(string imageGuid, string imageDscr)
        {
            using (db = new DB_ShiziEntities())
            {
                var e = db.AttachFile.Where(x => x.Guid == imageGuid).SingleOrDefault();
                    if (e != null)
                {
                    e.Dscr = imageDscr;
                    e.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }


        [HttpPost]
        public void onMasterImg(string hookID, string imageGuid)
        {
            using (db = new DB_ShiziEntities())
            {
                var es = db.AttachFile.Where(x => x.HookID == hookID).ToList();
                foreach(var m in es)
                {
                    m.IsMaster = "";
                    m.UpdateTime = DateTime.Now;
                }
                db.SaveChanges();

                var e = db.AttachFile.Where(x => x.Guid == imageGuid).SingleOrDefault();
                if (e != null)
                {
                    e.IsMaster = "Y";
                    e.UpdateTime = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }


        [HttpPost]
        public JsonResult GetOrderM()
        {
            using(db=new DB_ShiziEntities())
            {
                var es = db.OrderM
                    .Select(g => new
                    {
                        g.id,
                        g.memberId,
                        g.mobile,
                        g.shipTyp,
                        g.sn,
                        g.staffMemo,
                        g.stName,
                        g.totalPrice,
                        g.updateTime,
                        g.createDate,
                        g.custName,
                        g.email,
                        g.stat,
                        details = db.vCart.Where(v => v.orderid == g.id)
                            .Select(s => new
                            {
                                s.prodID,
                                s.ProdTyp,
                                s.Cost2,
                                s.title,
                                s.subtitle,
                                s.count
                            }).ToList()
                    })
                    .OrderByDescending(x => x.id)
                    .ToList();

                return Json(es);
            }
        }
        



    }
}