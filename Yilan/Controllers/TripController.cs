﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMWebApp.Models;

using AutoMapper;
using NLog;

namespace CMWebApp.Controllers
{
    [AllowAnonymous]
    public class TripController : Controller
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DB_ShiziEntities db;


        /// <summary>
        /// 初始化: 1.聯合行銷 2.我的收藏店家
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult getInit()
        {
            using (db = new DB_ShiziEntities())
            {
                var es1 = db.Item.Where(x=>x.ContentSN ==100)
                            .Select(g=>new
                            {
                                value = g.SN,
                                g.Sort,
                                text =g.Title,
                            })
                         .OrderBy(g => g.Sort)
                         .ToList();

                var es2 = db.TripName.Where(x => x.userid == User.Identity.Name || x.id == 0)
                                .Select(g=>new
                                {
                                    g.id,
                                    g.name
                                })
                                .OrderBy(x => x.id).ToList();

                return Json(new {
                    unionMarketings = es1,
                    sharedTrips = es2,
                });
            }
        }


        [HttpPost]
        public JsonResult GetStoreMarkers(int? tripid)
        {
            using (db = new DB_ShiziEntities())
            {
                string user = User.Identity.Name.ToString();

                //所有人的我的收藏的店家都是0

                var es = (from a in db.TripRouting
                          join b in db.vMarker on a.storeID equals b.id
                          where a.userId == user && a.tripID == tripid
                          select new vmMarker
                          {
                              storeID = a.storeID,
                              Name = b.name,
                              Typ = b.typ,
                              isVisible = a.isVisible,
                              sortNo = a.sortNo,
                              sn = a.sn,
                          }).AsQueryable();

                var es1 = (from a in db.TripRouting
                           where a.userId == user && a.tripID == tripid && a.storeID == 0
                           select new vmMarker
                           {
                               storeID = a.storeID,
                               Name = a.startLocation,
                               Typ = "start",
                               isVisible = "Y",
                               sortNo = a.sortNo,
                               sn = a.sn,
                           }).ToList();

                
                var es2 = es.ToList().Union(es1);
                var es3 = es2.OrderBy(x => x.sortNo).ToList();

                return Json(es3);
            }
        }

        [HttpPost]
        public JsonResult ToggleMarkerVisible(int? sn, int? tripid)
        {
            using (db = new DB_ShiziEntities())
            {
                var e = db.TripRouting.Where(x => x.sn == sn).SingleOrDefault();
                e.isVisible = (e.isVisible == "Y") ? "" : "Y";
                e.updateTime = DateTime.Now;
                db.SaveChanges();

                return this.GetStoreMarkers(tripid);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldSortSN"></param>
        /// <param name="newSortSN"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SortingMarkers(int? tripid, int? oldSortSN, int? newSortSN)
        {
            string UserID = User.Identity.Name.ToString();

            using (db = new DB_ShiziEntities())
            {
                oldSortSN += 1;
                newSortSN += 1;

                if (oldSortSN > newSortSN)
                {
                    var es = db.TripRouting
                                .Where(x => x.sortNo >= newSortSN && x.sortNo < oldSortSN
                                     && x.userId == UserID
                                     && x.tripID == tripid)
                                     .OrderBy(x => x.sortNo).ToList();
                    foreach (var c in es)
                    {
                        c.updateTime = DateTime.Now;
                        c.sortNo = c.sortNo + 1;
                    }
                    var e = db.TripRouting.Where(x => x.sortNo == oldSortSN && x.tripID == tripid && x.userId == UserID).SingleOrDefault();
                    e.sortNo = newSortSN;
                    e.updateTime = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    var es = db.TripRouting.Where(x => x.sortNo > oldSortSN && x.sortNo <= newSortSN
                                        && x.userId == UserID
                                        && x.tripID == tripid)
                                        .OrderBy(x => x.sortNo).ToList();
                    foreach (var c in es)
                    {
                        c.updateTime = DateTime.Now;
                        c.sortNo = c.sortNo - 1;
                    }
                    var e = db.TripRouting.Where(x => x.sortNo == oldSortSN && x.userId == UserID && x.tripID == tripid).SingleOrDefault();
                    e.sortNo = newSortSN;
                    e.updateTime = DateTime.Now;
                    db.SaveChanges();
                }

                return this.GetStoreMarkers(tripid);
            }
        }

        [HttpPost]
        public JsonResult getOpenTypsMarker(string typ)
        {
            using(db=new DB_ShiziEntities())
            {
                var es = db.Marker.Where(x => x.Typ == typ)
                                .Select(g=>new
                                {
                                    g.id,
                                    g.Name,
                                    g.address
                                })
                                .OrderBy(x => x.Name).ToList();
                return Json(es);
            }
        }

        

        [HttpPost]
        public void appendTypedMarker(int? markerid, string markerTyp, int? tripId)
        {
            using (db = new DB_ShiziEntities())
            {
                string user = User.Identity.Name;
                int? maxFavorSortNo = db.TripRouting.Where(x => x.userId == user && x.tripID == tripId.Value).Max(x => x.sortNo);
                maxFavorSortNo = (maxFavorSortNo == null)? 1 : maxFavorSortNo + 1;

                TripRouting n = new TripRouting()
                {
                    createDate = DateTime.Now,
                    isVisible = "Y",
                    sortNo = maxFavorSortNo,
                    tripID = tripId.Value,
                    storeID = markerid.Value,
                    typ = markerTyp,
                    userId = User.Identity.Name,
                };
                db.TripRouting.Add(n);
                db.SaveChanges();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="markerid"></param>
        /// <param name="markerTyp"></param>
        /// <returns></returns>
        [HttpPost]
        //public JsonResult mySaveFavors(string routeName)
        public JsonResult newTrip(string routeName)
        {
            using (db = new DB_ShiziEntities())
            {
                var user = User.Identity.Name;
                var maxTripID = db.TripName.Where(x => x.userid == user).Count()+1;

                //maxTripID = (maxTripID == null) ? 0 : maxTripID + 1;

                TripName tn = new TripName()
                {
                    id = maxTripID,
                    userid = user, 
                    name = routeName,
                    createDate = DateTime.Now
                };

                db.TripName.Add(tn);

                //db.SaveChanges();
                //var t = db.TripName.Where(x => x.userid == user).OrderByDescending(x => x.id).FirstOrDefault();
                //int? maxTripID = t.id;
                //db.SaveChanges();

                //把我的收藏當作基底來儲存。
                var es = db.TripRouting.Where(x => x.userId == user && x.tripID == 0).OrderBy(x => x.sortNo).ToList();

                foreach(var c in es)
                {
                    TripRouting n = new TripRouting()
                    {
                        createDate = DateTime.Now,
                        isVisible = c.isVisible,
                        sortNo = c.sortNo,
                        storeID = c.storeID,
                        tripID = maxTripID,
                        typ = c.typ,
                        userId = user
                    };
                    db.TripRouting.Add(n);
                }
                db.SaveChanges();

                return Json(new
                {
                    maxFavorTripID = maxTripID,
                });
            }
        }

        [HttpPost]
        public JsonResult changeStartPoint(int tripid, string startLocation)
        {
            string UserID = User.Identity.Name;
            string errMsg = "";

            using (db = new DB_ShiziEntities()) {

                try
                {
                    var es = db.TripRouting.Where(x => x.userId == UserID && x.tripID == tripid && x.storeID == 0).AsQueryable();

                    if (es.Count() == 0)
                    {

                        TripRouting newTR = new TripRouting()
                        {
                            createDate = DateTime.Now,
                            isVisible = "Y",
                            sortNo = 0,
                            storeID = 0,
                            startLocation = startLocation,
                            tripID = tripid,
                            typ = "Start",
                            userId = UserID
                        };
                        db.TripRouting.Add(newTR);
                    }
                    else
                    {
                        var e = es.SingleOrDefault();
                        e.startLocation = startLocation;
                        e.updateTime = DateTime.Now;
                    }
                    db.SaveChanges();
                    return Json("更新完成");
                }
                catch (Exception ex)
                {
                    errMsg = ex.Message.ToString();
                    return Json(errMsg);
                }
                
            };

        }

        [HttpPost]
        public JsonResult getFacStores()
        {
            using(db =new DB_ShiziEntities())
            {
                var es = db.Store.OrderBy(x => x.typ)
                            .Select(g => new
                            {
                                g.id,
                                g.name,
                                g.lat,
                                g.lng,
                                g.gmapTripUrl
                            }).ToList();

                var es2 = db.Item.Where(x => x.ContentSN == 100 && x.IsRemove != "Y")
                            .Select(g => new
                            {
                                g.Title,
                                g.SN,
                                g.ActiveStores
                            }).ToList();

                return Json(new {
                    facStores = es,
                    unionMarketings = es2
                });
            }
        }
    }
}