﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using CMWebApp.Models;
using Microsoft.AspNet.Identity;
using System.Text;

namespace CMWebApp.Attr
{

    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }

    public class GetStoreNameAttribute : ActionFilterAttribute 
    {
        DB_ShiziEntities db;
        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            HttpContextBase hc = filterContext.HttpContext;
            string
                UserName = hc.User.Identity.GetUserName(),
                Store = "Trial",    //店名稱
                StoreRole = "";  //店管理角色

            using (db = new DB_ShiziEntities()) {
                //var e = db.StoreAdminUser.Where(x => x.UserName == UserName).SingleOrDefault();
                //Store = (e == null) ? "wressly" : e.Store;
                //StoreRole = (e == null) ? "" : e.Role;
                filterContext.Controller.ViewData["Store"] = "Store";
                filterContext.Controller.ViewData["StoreRole"] = "StoreRole";
            }

            /*if (hc.Session["UserID"] == null) {
                filterContext.Result = new RedirectResult(OLP_Url);
            } else {
                filterContext.Controller.ViewData["UserID"] = hc.Session["UserID"].ToString();
            }*/
        }
    }


    public class Big5CharsetAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext fc)
        {
            var f = fc.HttpContext;
            f.Response.AddHeader("Content-Type", "text/html; charset=big5");
            f.Response.ContentEncoding = Encoding.GetEncoding(950);
            
            //fc.RequestContext.HttpContext.Request.ContentType
        }
    }

    //public class CheckJWTTokenAttribute : ActionFilterAttribute
    //{
    //    public string TokenE {get; set;}
    //    public string DeCodeJWT(string token)
    //    {
    //        string SecretKey = ConfigurationManager.AppSettings["JWTScretKey"].ToString();
    //        string JWTToken = JWT.JsonWebToken.Decode(token, SecretKey, true);
    //        return JWTToken;
    //    }

    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        var fc = filterContext;
    //        string token = fc.Controller.ValueProvider.GetValue("Token").AttemptedValue;

    //        base.OnActionExecuting(filterContext);
    //    }
    //}


    //public class SetJWTJsonAttribute : ActionFilterAttribute
    //{
    //    EPCWebEntities db;

    //    public string TokenE { get; set; }
    //    public string UserCode { get; set; }
    //    public bool IsAuthorized { get; set; }

    //    public string IdParamName { get; set; }

    //    public string LoginName { get; set; }
    //    public string RoleCode { get; set; }

    //    public string GetJwtToken() {
    //        string SecretKey = ConfigurationManager.AppSettings["JWTScretKey"].ToString();

    //        using (db = new EPCWebEntities()) {
    //            var e = db.vAuthTokens.Where(x => x.token == this.UserCode).SingleOrDefault();
    //            this.RoleCode = e.RoleCode;
    //            this.LoginName = e.Name;
    //        }

    //        var payload = new Dictionary<string, object>()
    //        {
    //            { "UserCode", this.UserCode },
    //            { "RoleCode", this.RoleCode }
    //        };

    //        var token = JWT.JsonWebToken.Encode(payload, SecretKey, JWT.JwtHashAlgorithm.HS384);
    //        return token;
    //    }

    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        var fc = filterContext;
    //        this.UserCode = fc.Controller.ValueProvider.GetValue("UserCode").AttemptedValue;
    //        string token = this.GetJwtToken();

    //        fc.RouteData.Values.Add("Token", token);
    //        fc.RouteData.Values.Add("LoginName", this.LoginName);

    //        //fc.RequestContext.HttpContext.Response.AddHeader("Authorization", token);
    //        base.OnActionExecuting(filterContext);
    //    }
    //}

    ///// <summary>
    ///// 讀取HttpHeader Token來判斷是否有認證過
    ///// </summary>
    //public class ReadJwtHeaderAttribute : ActionFilterAttribute
    //{
    //    public string TokenE { get; set; }
    //    public string UserID { get; set; }
    //    public string RoleCode { get; set; }

    //    public string DeCodeJWT(string token)
    //    {
    //        string SecretKey = ConfigurationManager.AppSettings["JWTScretKey"].ToString();
    //        string JWTToken = "";
    //        try
    //        {
    //            JWTToken = JWT.JsonWebToken.Decode(token, SecretKey, true);
    //        }
    //        catch (Exception ex)
    //        {
    //            return "Invalid";
    //        }
    //        return JWTToken;
    //    }

    //    EPCWebEntities db;
    //    public string GetUserID(string UserCode)
    //    {
    //        using(db =new EPCWebEntities())
    //        {
    //            var e = db.vAuthTokens.Where(x => x.token == UserCode).SingleOrDefault();
    //            this.RoleCode = e.RoleCode;
    //            return (e == null)? "Empty UserID": e.Name;
    //        }
    //    }

    //    class PayLoad
    //    {
    //        public string UserCode { get; set; }
    //        public string RoleCode { get; set; }
    //    }

    //    T GetObject<T>(Dictionary<string, object> dict)
    //    {
    //        Type type = typeof(T);
    //        var obj = Activator.CreateInstance(type);

    //        foreach (var kv in dict)
    //        {
    //            type.GetProperty(kv.Key).SetValue(obj, kv.Value);
    //        }
    //        return (T)obj;
    //    }

    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        var fc = filterContext;
    //        var headers = fc.HttpContext.Request.Headers;
    //        string token = "";

    //        if (!String.IsNullOrEmpty(headers["token"]))
    //        {
    //            token = headers["token"];
    //            //string UserCode = this.DeCodeJWT(token);
    //            string SecretKey = System.Configuration.ConfigurationManager.AppSettings["JWTScretKey"].ToString();

    //            var payload = GetObject<PayLoad>(JWT.JsonWebToken.DecodeToObject(token, SecretKey) as Dictionary<string, object>);
    //            string UserCode = payload.UserCode;
    //            this.UserID = this.GetUserID(UserCode);
    //            string role = this.RoleCode;

    //            fc.RouteData.Values.Add("UserID", UserID);
    //            fc.RouteData.Values.Add("RoleCode", RoleCode);
    //            this.UserID = UserID;

    //            if (UserCode == "Invalid")
    //            {
    //                fc.RequestContext.HttpContext.Response.Redirect("Invalid");
    //            }
    //            else
    //            {
    //                fc.RequestContext.HttpContext.Response.AddHeader("check", UserCode);
    //            }
    //        }
    //        else
    //        {
    //            fc.RequestContext.HttpContext.Response.Redirect("Invalid");
    //        }

    //        base.OnActionExecuting(filterContext);
    //    }

    //}


}