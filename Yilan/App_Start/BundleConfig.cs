﻿using System.Web;
using System.Web.Optimization;

namespace CMWebApp
{
    public class BundleConfig
    {
        // 如需「搭配」的詳細資訊，請瀏覽 http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/").Include(
            //            "~/Scripts/jquery-{version}.js",
            //            "~/Scripts/src/patch.js"));


            bundles.Add(new StyleBundle("~/content/css").Include(
                     "~/Scripts/vendor/semantic-ui/dist/semantic.css",
                     "~/Content/bootstrap.css",
                     "~/Content/main2.css",
                     "~/Content/admin.css",
                     "~/Content/Site.css"));
            //-----------------------------------------------------------------
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // 準備好實際執行時，請使用 http://modernizr.com 上的建置工具，只選擇您需要的測試。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/Scripts/jquery-{version}.js",
                      "~/Scripts/src/patch.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/vendor/semantic-ui/dist/semantic.js",
                      "~/Scripts/respond.js"));

           
        }
    }
}
