﻿using System.Web;
using System.Web.Mvc;

namespace CMWebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //此程式碼會將 Authorize 篩選器和 RequireHttps 篩選器新增至應用程式。 Authorize 篩選器會防止匿名使用者存取應用程式中的任何方法。 您將使用 AllowAnonymous 屬性來選擇略過一些方法中的授權需求，讓匿名使用者可登入及檢視首頁。 使用 RequireHttps 時，所有對 Web 應用程式的存取都必須透過 HTTPS 進行。

            //替代的方法是將 Authorize 屬性和 RequireHttps 屬性新增至每個控制器，但將這些屬性套用至整個應用程式是最安全的做法。 藉由全面新增這些屬性，您所新增的每個新控制器和動作方法都會自動受到保護，而不需要您記得套用它們。 如需詳細資訊，請參閱保護您的 ASP.NET MVC 應用程式和新 AllowAnonymous 屬性。

            //將 AllowAnonymous 屬性新增至首頁控制器的 Index 方法。 AllowAnonymous 屬性能讓您將想要選擇略過授權的方法加到白名單。如果您全面搜尋 AllowAnonymous，您將會發現帳戶控制器的登入與註冊方法中都使用了它。
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
            //filters.Add(new RequireHttpsAttribute());
        }
    }
}
