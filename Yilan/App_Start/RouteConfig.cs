﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CMWebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var AdminAliasName = System.Configuration.ConfigurationManager.AppSettings["AdminAliasName"].ToString();

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.RouteExistingFiles = true;

            //routes.Add(new LegacyUrlRoute());

            //--====================================================
            routes.MapRoute(
               name: "landingpage",
               url: "",
               defaults: new
               {
                   controller ="home", action = "context", id = UrlParameter.Optional
               }
            );

            //routes.MapRoute(
            //   name: "odds",
            //   url: "h",
            //   defaults: new { controller = "Home", action = "mainland", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
               name: "admin",
               url: AdminAliasName+"/{action}/{id}",
               defaults: new { controller = 
               "Admin", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "Member",
              url: "Member/{action}/{id}",
              defaults: new { controller = "Member", action = "Cart", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "Account",
              url: "Account/{action}/{id}",
              defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "store",
               url: "store/{action}/{id}",
               defaults: new { controller = "Store", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "trip",
              url: "trip/{action}/{id}",
              defaults: new { controller = "Trip", action = "GetStoreMarkers", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "getdata",
             url: "getdata/{action}/{id}",
             defaults: new { controller = "GetData", action = "GetSingleContent", id = UrlParameter.Optional }
           );

            //routes.MapRoute(
            //  name: "singleContent",
            //  url: "tourism/{RouteName}/{id}",
            //  defaults: new { controller = "Home", action = "context", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
               name: "singleContent",
               url: "{RouteName}/{id}",
               defaults: new { controller = "Home", action = "context", id = UrlParameter.Optional }
           );

        }
    }   
        
    public class LegacyUrlRoute : RouteBase
    {
        // source: http://www.mikesdotnetting.com/Article/108/Handling-Legacy-URLs-with-ASP.NET-MVC
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            const string status = "301 Moved Permanently";
            var request = httpContext.Request;
            var response = httpContext.Response;
            var legacyUrl = request.Url.ToString();
            var newUrl = "";

            if (legacyUrl.Contains("/villas/") || legacyUrl.EndsWith("asp"))
            {
                if (legacyUrl.Contains("/villas/"))
                    newUrl = "/en/home/Destinations";
                else if (legacyUrl.EndsWith("php"))
                {
                    newUrl = "/readpen2/test";
                    //if (legacyUrl.Contains("about")) newUrl = "/en/home/AboutUs";
                    //if (legacyUrl.Contains("offers")) newUrl = "/en/home/Offers";
                    //if (legacyUrl.Contains("contact")) newUrl = "/en/home/ContactUs";
                    //if (legacyUrl.Contains("destinations")) newUrl = "/en/home/destinations";
                    //if (legacyUrl.Contains("faq")) newUrl = "/en/home/Faq";
                    //if (legacyUrl.Contains("terms")) newUrl = "/en/home/Terms";
                    //if (legacyUrl.Contains("privacy")) newUrl = "/en/home/Privacy";
                }

                response.Status = status;
                response.RedirectLocation = newUrl;
                response.End();
            }
            return null;
        }
        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            return null;
        }
    }
}
