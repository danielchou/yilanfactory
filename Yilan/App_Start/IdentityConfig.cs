﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using CMWebApp.Models;


using System.Net.Mail;
using System.Configuration;

namespace CMWebApp
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            //WebMail.SmtpPort = 25;
            //WebMail.SmtpServer = "mail.powermax.tw";
            //WebMail.UserName = "postmaster@powermax.tw";
            //WebMail.Password = "#power123";
            //WebMail.Send(
            //  message.Destination,
            //  message.Subject,
            //  message.Body
            //);

            MailMessage m = new MailMessage();
            SmtpClient sc = new SmtpClient();
            try
            {
                /* For SmarterASP 
                m.From = new MailAddress("postmaster@powermax.tw");
                m.To.Add(message.Destination);
                m.Subject = message.Subject;
                m.Body = message.Body;
                m.IsBodyHtml = true;
                sc.Host = "mail.powermax.tw";
                sc.Port = 25;
                sc.Credentials = new System.Net.NetworkCredential("postmaster@powermax.tw", "#power123");
                */

                /* For GMAIL */
                string GmailSender = ConfigurationManager.AppSettings["GmailSender"].ToString();
                string GmailSenderPassword = ConfigurationManager.AppSettings["GmailSenderPassword"].ToString();

                m.From = new MailAddress(GmailSender);
                m.To.Add(message.Destination);
                m.Subject = message.Subject;
                m.Body = message.Body;
                m.IsBodyHtml = true;
                
                sc.Host = "smtp.gmail.com";
                sc.Port = 587; 
                sc.Credentials = new System.Net.NetworkCredential(GmailSender, GmailSenderPassword);
                sc.EnableSsl = true;
                sc.Send(m);
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }




            // 將您的電子郵件服務外掛到這裡以傳送電子郵件。
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // 將您的 SMS 服務外掛到這裡以傳送簡訊。
            return Task.FromResult(0);
        }
    }

    // 設定此應用程式中使用的應用程式使用者管理員。UserManager 在 ASP.NET Identity 中定義且由應用程式中使用。
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // 設定使用者名稱的驗證邏輯
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // 設定密碼的驗證邏輯
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };

            // 設定使用者鎖定詳細資料
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // 註冊雙因素驗證提供者。此應用程式使用手機和電子郵件接收驗證碼以驗證使用者
            // 您可以撰寫專屬提供者，並將它外掛到這裡。
            manager.RegisterTwoFactorProvider("電話代碼", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "您的安全碼為 {0}"
            });
            manager.RegisterTwoFactorProvider("電子郵件代碼", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "安全碼",
                BodyFormat = "您的安全碼為 {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();

            /// Ref: https://docs.microsoft.com/en-us/aspnet/identity/overview/features-api/account-confirmation-and-password-recovery-with-aspnet-identity 尋找 Once a forgotten password token has been used, it's invalidated. The following code change in the Create method (in the App_Start\IdentityConfig.cs file) sets the tokens to expire in 3 hours. 
            /// 為了防止認證時間過久，產生安全疑慮，設定發出email三小時之後就銷毀。

            int TokenLifeSpan =  Int16.Parse( ConfigurationManager.AppSettings["TokenLifeSpanHour"].ToString());

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"))
                    {
                        TokenLifespan = TimeSpan.FromHours(TokenLifeSpan)                   
                    };
            }
            return manager;
        }
    }

    // 設定在此應用程式中使用的應用程式登入管理員。
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
