﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CMWebApp.Startup))]
namespace CMWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
